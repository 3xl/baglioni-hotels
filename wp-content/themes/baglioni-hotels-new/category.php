<?php 

$is_press_review = ( strtoupper( get_the_archive_title() ) == 'PRESS REVIEW' );

get_header(); ?>

<!--start nicdark_container-->
<div class="nicdark_container nicdark_clearfix">

    <div class="nicdark_section nicdark_height_40"></div>

    <div class="nicdark_grid_12">
        <h1 class="category-title"><?php echo get_the_archive_title(); ?></h1>
    </div>

    <div class="posts-grid">
        <?php while ( have_posts() ) : the_post(); ?>
        <div class="nicdark_grid_4">
            <div class="post">
                <?php if( has_post_thumbnail() ) : ?>
                <div class="post-thumbnail">
                    <?php the_post_thumbnail( 'landscape' ); ?>
                </div>
                <?php endif; ?>
                
                <div class="post-data">
                    <h2 class="post-title"><?php the_title(); ?></h2>

                    <p class="post-excerpt"><?php echo get_the_excerpt(); ?></p>

                    <div class="post-button">
                        <?php if( $is_press_review ) : ?>
                        <a href="<?php echo ( !empty( get_post_meta( get_the_ID(), 'post-document', true ) ) ? get_post_meta( get_the_ID(), 'post-document', true ) : '#' ); ?>"><?php echo __( 'DOWNLOAD', 'baglioni-hotels' ); ?></a>
                        <?php else : ?>
                        <a href="<?php the_permalink(); ?>"><?php echo __( 'READ MORE', 'baglioni-hotels' ); ?></a>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
        <?php endwhile; ?>
    </div>

    <div class="navigation nicdark_grid_12">
        <div class="previous"><?php next_posts_link( __ ( '&laquo; Previous Posts', 'baglioni-hotels' ) ); ?></div>
        <div class="next"><?php previous_posts_link( __ ( 'Next Posts &raquo;', 'baglioni-hotels' ) ); ?></div>
    </div>

    <div class="nicdark_section nicdark_height_40"></div>

</div>
<!--end container-->

<?php get_footer(); ?>