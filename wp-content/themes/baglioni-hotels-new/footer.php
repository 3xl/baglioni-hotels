<footer id="footer">
    <div class="footer-main">
        <div class="nd_options_container">
            <?php wp_nav_menu( array(
                'menu' => 'footer-menu-2',
                'menu_class' => 'footer-menu',
            ) ); ?>  
            
            <div class="footer-columns">
                <div class="footer-column">
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/images/Logo.png" id="footer-logo"/>
                    
                    <div class="footer-data">
                        <div><?php echo __("BAGLIONI HOTELS S.p.A. single-member company", "baglioni-hotels") ?></div>
                        <div style="margin-top:5px;"><?php echo __("VAT 12956850155", "baglioni-hotels") ?></div>
                        <div style="margin-top:5px;"><?php echo __("info@baglionihotels.com", "baglioni-hotels") ?></div>
                        <div style="margin-top:5px;">
                            <a href="<?php echo get_permalink( get_page_by_path('privacy-policy') ); ?>"><?php echo __("PRIVACY POLICY", "baglioni-hotels") ?></a> - 
                            <a href="<?php echo get_permalink( get_page_by_path('cookie-policy') ); ?>"><?php echo __("COOKIE POLICY", "baglioni-hotels") ?></a> - 
                            <a href="<?php echo bloginfo('rss2_url'); ?>"><?php echo __("RSS", "baglioni-hotels") ?></a>
                        </div>
                    </div>
                </div>
                <div class="footer-column">
                    <h2 class="footer-column-title"><?php echo __("INSTAGRAM", "baglioni-hotels") ?></h2>
                    <div><?php echo do_shortcode( '[instagram-feed]' ); ?></div>
                </div>
                <div class="footer-column">
                    <h2 class="footer-column-title" style="margin-bottom: 10px;"><?php echo __("DESTINATIONS", "baglioni-hotels") ?></h2>
                    
                    <?php wp_nav_menu( array(
                        'menu' => 'Footer Destinations Menù',
                        'menu_class' => 'footer-destinations-menu',
                    ) ); ?>
                    
                    <div style="margin-top: 50px;">
                        <h2 class="footer-column-title"><?php echo __("NEWSLETTER", "baglioni-hotels") ?></h2>

                        <?php echo do_shortcode( '[baglioni-hotels-newsletter-form]' ); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-social-bar">
        <div class="nd_options_container">
            <div class="footer-social-bar-left">
                <div class="footer-social-bar-label"><?php echo __( 'Follow us', 'baglioni-hotels' ); ?></div>

                <a href="https://www.facebook.com/baglionihotels" class="footer-social-bar-icon" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/facebook-icon.png"></a>
                <a href="https://twitter.com/baglioni_hotels" class="footer-social-bar-icon" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/twitter-icon.png"></a>
                <a href="https://www.instagram.com/baglionihotels/" class="footer-social-bar-icon" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/instagram-icon.png"></a>
                <a href="https://www.youtube.com/user/BaglioniHotels" class="footer-social-bar-icon" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/youtube-icon.png"></a>
            </div>
            <div class="footer-social-bar-right">
                <div class="footer-social-bar-label"><?php echo __( 'Baglioni Hotels is a member of' ); ?></div>

                <div class="footer-social-bar-image"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/leading-hotels-logo.png" width="100"></div>
                <div class="footer-social-bar-image"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/relais-chateaux-logo.png" width="50"></div>
            </div>
        </div>
    </div>
    <div class="footer-bottom-bar">
        <div class="nd_options_container">
            <div class="footer-bottom-bar-columns">
                <div class="footer-bottom-bar-column">
                    <h5 class="footer-bottom-bar-title"><?php echo __( 'Our Partner', 'baglioni-hotels' ); ?></h5>

                    <a href="https://www.constancehotels.com/it/" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/costance-logo.png" class="footer-bottom-bar-logo" /></a>
                </div>
                <div class="footer-bottom-bar-column">
                    <h5 class="footer-bottom-bar-title"><?php echo __( 'Mauritius', 'baglioni-hotels' ); ?></h5>

                    <ul class="footer-bottom-bar-menu">
                        <li><a href="https://www.constancehotels.com/it/hotels-resorts/mauritius/prince-maurice/" target="_blank"><?php echo __( 'COSTANCE Prince Maurice', 'baglioni-hotels' ); ?></a></li>
                        <li><a href="https://www.constancehotels.com/it/hotels-resorts/mauritius/belle-mare-plage/?utm_source=Baglioni-Hotels&utm_medium=referral&utm_campaign=Baglioni-Hotels" target="_blank"><?php echo __( 'COSTANCE Belle Mare Plage', 'baglioni-hotels' ); ?></a></li>
                    </ul>
                </div>
                <div class="footer-bottom-bar-column">
                    <h5 class="footer-bottom-bar-title"><?php echo __( 'Seychelles', 'baglioni-hotels' ); ?></h5>

                    <ul class="footer-bottom-bar-menu">
                        <li><a href="https://www.constancehotels.com/it/hotels-resorts/seychelles/ephelia/?utm_source=Baglioni-Hotels&utm_medium=referral&utm_campaign=Baglioni-Hotels" target="_blank"><?php echo __( 'COSTANCE Euphelia', 'baglioni-hotels' ); ?></a></li>
                        <li><a href="https://www.constancehotels.com/it/hotels-resorts/seychelles/lemuria/?utm_source=Baglioni-Hotels&utm_medium=referral&utm_campaign=Baglioni-Hotels" target="_blank"><?php echo __( 'COSTANCE Lemuria', 'baglioni-hotels' ); ?></a></li>
                    </ul>
                </div>
                <div class="footer-bottom-bar-column">
                    <h5 class="footer-bottom-bar-title"><?php echo __( 'Madagascar', 'baglioni-hotels' ); ?></h5>

                    <ul class="footer-bottom-bar-menu">
                        <li><a href="https://www.constancehotels.com/it/hotels-resorts/madagascar/tsarabanjina/?utm_source=Baglioni-Hotels&utm_medium=referral&utm_campaign=Baglioni-Hotels" target="_blank"><?php echo __( 'COSTANCE Tsarabanjina', 'baglioni-hotels' ); ?></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
</div>
<!--END theme-->

<?php wp_footer(); ?>

	
</body>  
</html>