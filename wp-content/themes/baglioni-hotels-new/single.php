<?php get_header(); ?>

<!--start nicdark_container-->
<div class="nicdark_container nicdark_clearfix">

    <div class="nicdark_section nicdark_height_20"></div>

    <div class="nicdark_grid_12">
        <h1 class="post-title"><?php the_title(); ?><h1>
    </div>

    <div class="nicdark_grid_8">
        <?php if(have_posts()) :
            while(have_posts()) : the_post(); ?>
                <!--start content-->
                <div class="post-content">
                    <?php the_content(); ?>
                </div>
                <!--end content-->
            <?php endwhile; ?>
        <?php endif; ?>
    </div>

    <div class="nicdark_grid_4">
        <div class="post-thumbnail">
            <?php the_post_thumbnail( 'landscape' ); ?>
        </div>
    </div>

    <div class="nicdark_section nicdark_height_20"></div>

</div>
<!--end container-->

<?php get_footer(); ?>