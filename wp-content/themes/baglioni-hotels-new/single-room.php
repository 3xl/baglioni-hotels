<?php

$image_url = get_the_post_thumbnail_url( get_the_ID(), 'full' );

$room_branch = get_post_meta( get_the_ID(), 'room-branch', true );

$room_suite = get_post_meta( get_the_ID(), 'room-suite', true );

$room_price = get_post_meta( get_the_ID(), 'room-price', true );

$room_bg_position = ( !empty( get_post_meta( get_the_ID(), 'room-bg-position', true ) ) ) ? get_post_meta( get_the_ID(), 'room-bg-position', true ) : 'center center';
$room_slider = get_post_meta( get_the_ID(), 'room-slider', true );

$room_guests = get_post_meta( get_the_ID(), 'room-guests', true );
$room_best_view = get_post_meta( get_the_ID(), 'room-best-view', true );
$room_sqm = get_post_meta( get_the_ID(), 'room-sqm', true );
$room_style = get_post_meta( get_the_ID(), 'room-style', true );
$room_pet_friendly = get_post_meta( get_the_ID(), 'room-pet-friendly', true );

$room_features = get_post_meta( get_the_ID(), 'room-features', true );
$room_benefits = get_post_meta( get_the_ID(), 'room-benefits', true );

if( !empty( $room_features ) ) :
    $room_features = explode( '|', $room_features );
endif;

if( !empty( $room_benefits ) ) :
    $room_benefits = explode( '|', $room_benefits );
endif;

$room_book_now = get_post_meta( get_the_ID(), 'room-book-now', true );

$room_first_box_title = get_post_meta( get_the_ID(), 'room-first-box-title', true );
$room_first_box_image = get_post_meta( get_the_ID(), 'room-first-box-image', true );
$room_first_box_description = get_post_meta( get_the_ID(), 'room-first-box-description', true );
$room_first_box_link = get_post_meta( get_the_ID(), 'room-first-box-link', true );

$room_second_box_title = get_post_meta( get_the_ID(), 'room-second-box-title', true );
$room_second_box_image = get_post_meta( get_the_ID(), 'room-second-box-image', true );
$room_second_box_description = get_post_meta( get_the_ID(), 'room-second-box-description', true );
$room_second_box_link = get_post_meta( get_the_ID(), 'room-second-box-link', true );

$room_third_box_title = get_post_meta( get_the_ID(), 'room-third-box-title', true );
$room_third_box_image = get_post_meta( get_the_ID(), 'room-third-box-image', true );
$room_third_box_description = get_post_meta( get_the_ID(), 'room-third-box-description', true );
$room_third_box_link = get_post_meta( get_the_ID(), 'room-third-box-link', true );

$boxes = array( 'room-first-box-link', 'room-second-box-link', 'room-third-box-link' );
$num_boxes = 0;
$box_classes = array(
    1 => 'nicdark_grid_12',
    2 => 'nicdark_grid_6',
    3 => 'nicdark_grid_4',
);

foreach( $boxes as $box ) :
    if( !empty( get_post_meta( get_the_ID(), $box, true ) ) ) :
        $num_boxes++;
    endif;
endforeach;

$box_class = $box_classes[$num_boxes];

$room_similar_suites = get_post_meta( get_the_ID(), 'room-similar-suites', true );

if( !empty( $room_similar_suites ) ) :
    $room_similar_suites = explode( ',', get_post_meta( get_the_ID(), 'room-similar-suites', true ) );
endif;

$num_items = 0;
$items = array( 'room-guests', 'room-best-view', 'room-sqm', 'room-style', 'room-pet-friendly' );
$item_classes = array(
    1 => 'item-100',
    2 => 'item-50',
    3 => 'item-33',
    4 => 'item-25',
    5 => 'item-20'
);

foreach ( $items as $item ) :
    if( !empty( get_post_meta( get_the_ID(), $item, true ) ) ) :
        $num_items++;
    endif;
endforeach;

$item_class = $item_classes[$num_items];

get_header(); ?>

<!--start section-->
<div class="room-title-bg nicdark_section"<?php echo ( !empty( $image_url ) ) ? ' style="background: linear-gradient(rgba(0, 0, 0, 0.45), rgba(0, 0, 0, 0.45)), url(' . $image_url .'); background-position: ' . $room_bg_position . '; background-size: cover; background-repeat: no-repeat;"' : ''; ?>>

    <!--start nicdark_container-->
    <div class="nicdark_container nicdark_clearfix">

        <div class="nicdark_grid_12">

            <div class="nicdark_section nicdark_height_200"></div>

            <div class="price-from"><?php echo __( 'Price From', 'baglioni-hotels' ); ?></div>
            <div class="room-price"><?php echo $room_price; ?></div>

            <div class="room-title-links">
                <a href="#content" class="room-title-link"><?php echo __( 'Description', 'baglioni-hotels' ); ?></a>
                <a href="#around-the-hotel" class="room-title-link"><?php echo __( 'Around the Hotel', 'baglioni-hotels' ); ?></a>
                <a href="#similar-suites" class="room-title-link"><?php echo __( 'Similar Suites in our collection', 'baglioni-hotels' ); ?></a>
                <?php if( !empty( $room_book_now ) ) : ?>
                <a href="<?php echo $room_book_now; ?>" class="room-title-link" target="_blank"><?php echo __( 'Check availability', 'baglioni-hotels' ); ?></a>
                <?php endif; ?>
            </div>

            <div class="nicdark_section nicdark_height_20"></div>

        </div>

    </div>
    <!--end container-->

</div>
<!--end section-->

<!--start nicdark_container-->
<div class="nicdark_container nicdark_clearfix">

    <div class="nicdark_section nicdark_height_20"></div>

    <div class="nicdark_grid_12">
        <h1 class="room-title"><?php the_title(); ?></h1>
        <h6 class="room-branch"><?php echo get_room_branch( get_the_ID() ); ?></h6>
    </div>

    <div class="nicdark_section nicdark_height_20"></div>

    <div class="nicdark_grid_8">

        <?php if( !empty( $room_slider ) ) :
            putRevSlider( $room_slider );
        endif; ?>

        <?php if( $num_items > 0 ) : ?>
        <div class="room-items">
            <?php if( !empty( $room_guests ) ) : ?>
            <div class="item <?php echo $item_class; ?>">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/guests-icon.png" alt="Guests Icon" />
                <div class="item-description"><?php echo $room_guests; ?></div> 
            </div>
            <?php endif; ?>

            <?php if( !empty( $room_best_view ) ) : ?>
            <div class="item <?php echo $item_class; ?>">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/best-view-icon.png" alt="Best view Icon" />
                <div class="item-description"><?php echo $room_best_view; ?></div> 
            </div>
            <?php endif; ?>
            
            <?php if( !empty( $room_sqm ) ) : ?>
            <div class="item <?php echo $item_class; ?>">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/sqm-icon.png" alt="Guests Icon" />
                <div class="item-description"><?php echo $room_sqm ?></div> 
            </div>
            <?php endif; ?>

            <?php if( !empty( $room_style ) ) : ?>
            <div class="item <?php echo $item_class; ?>">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/<?php echo strtolower( str_replace( ' ', '-', $room_style ) ) ?>-icon.png" alt="Best view Icon" />
                <div class="item-description"><?php echo $room_style; ?></div> 
            </div>
            <?php endif; ?>
            
            <?php if( $room_pet_friendly == 1 ) : ?>
            <div class="item <?php echo $item_class; ?>">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/pet-friendly-icon.png" alt="Guests Icon" />
                <div class="item-description"><?php echo __( 'PET FRIENDLY', 'baglioni-hotels' ); ?></div> 
            </div>
            <?php endif; ?>
        </div>
        <?php endif; ?>

        <div class="divisor-line"></div>

        <div class="room-content" id="content">
            <?php if(have_posts()) :
                while(have_posts()) : the_post(); ?>
                    <!--start content-->
                    <?php the_content(); ?>
                    <!--end content-->
                <?php endwhile; ?>
            <?php endif; ?>
        </div>

        <?php if( is_array( $room_features ) && count( $room_features ) > 0 ) : ?>
        <div class="divisor-line"></div>

        <h4 class="specifications-title"><?php echo __( 'Features', 'baglioni-hotels' ); ?></h4>

        <ul class="specifications">
            <?php foreach( $room_features as $room_feature ) : ?>
            <li><?php echo $room_feature; ?></li>
            <?php endforeach; ?>
        </ul>
        <?php endif; ?>

        <?php if( is_array( $room_benefits ) && count( $room_benefits ) > 0 ) : ?>
        <div class="divisor-line"></div>

        <h4 class="specifications-title"><?php echo __( 'Benefits and Amenities*', 'baglioni-hotels' ); ?></h4>

        <ul class="specifications">
            <?php foreach( $room_benefits as $room_benefit ) : ?>
            <li><?php echo $room_benefit; ?></li>
            <?php endforeach; ?>
        </ul>

        <div class="nicdark_section nicdark_height_60"></div>

        <p class="specifications-notes">
            <?php echo __( '* The Baglioni Suites benefits and amenities are not available in case of upgrades or other promotions.', 'baglioni-hotels' ); ?>
        </p>
        <?php endif; ?>

        <div class="nicdark_section nicdark_height_60"></div>

        <?php if( $room_pet_friendly ) : ?>
        <div class="specifications-notes-icon">
            <div class="note-icon">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/pet-friendly-icon.png" alt="Pet Friendly Icon" />
            </div>
            <div class="note-description">
                <?php echo __( 'We welcome our guests’ little friends up to a small/medium size (max of 15 kilos weight).<br>A supplement of 50€ (50£) per stay will be applied.', 'baglioni-hotels' ); ?>
            </div>
        </div>

        <div class="nicdark_section nicdark_height_60"></div>
        <?php endif; ?>
        
        <?php if( !empty( $room_book_now ) ) : ?>
        <a href="<?php echo $room_book_now; ?>" class="btn-blue" target="_blank"><?php echo __( 'BOOK THIS ROOM', 'baglioni-hotels' ); ?></a>
        <?php endif; ?>

        <div class="nicdark_section nicdark_height_40"></div>

    </div>

    <!--sidebar-->
    <div class="nicdark_grid_4">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/my-baglioni-logo.png" alt="My Baglioni Hotels" class="my-baglioni-logo" />
    
        <p class="my-baglioni-description">
            <?php echo __( 'More than a special offer. More than a series of promotions. More than a loyalty programme. My Baglioni Hotels has a world of advantages and benefits waiting to welcome you. This is our way of treating you and saying thank you for choosing Baglioni Hotels, hoping that you will choose us with fresh enthusiasm every day, for your every dream, desire, whim or need. That\'s why we say: “Check in. You\'ll never want to check out.”', 'baglioni-hotels' ); ?>
        </p>

        <a href="#" class="btn-blue"><?php echo __( 'SUBSCRIBE NOW', 'baglioni-hotels' ); ?></a>

        <div class="nicdark_section nicdark_height_60"></div>

        <?php
        $meta_query = array(
            array(
                'key' => 'room-branch',
                'value' => $room_branch,
                'compare' => '='
            ),
            array(
                'key' => 'room-suite',
                'value' => $room_suite,
                'compare' => '='
            ),
        );
        
        $args = array( 'post_type' => 'room', 'posts_per_page' => -1, 'meta_query' => $meta_query );
        $other_rooms = get_posts( $args );
        if( count( $other_rooms ) > 1 ) : ?>
        <h4 class="sidebar-others-title"><?php echo ( $room_suite ? __( 'Other Suites in', 'baglioni-hotels' ) : __( 'Other Rooms in', 'baglioni-hotels' ) ); ?> <?php echo get_room_city( get_the_ID() ); ?></h4>

        <?php foreach( $other_rooms as $other_room ) : ?>
        <?php if( $other_room->ID != get_the_ID() ) : ?>
        <div class="sidebar-other">
            <div class="sidebar-other-image">
                <img src="<?php echo get_the_post_thumbnail_url( $other_room->ID, 'thumbnail' ); ?>" alt="<?php echo $other_room->post_title; ?>" />
            </div>
            <div class="sidebar-other-data">
                <h5 class="sidebar-other-title"><?php echo $other_room->post_title; ?></h5>

                <?php /*if( !empty( get_post_meta( $other_room->ID, 'room-small-description', true ) ) ) :
                <p class="sidebar-other-description"><?php echo get_post_meta( $other_room->ID, 'room-small-description', true ); ?></p>
                endif; */ ?>

                <a href="<?php echo get_permalink( $other_room->ID ); ?>" class="siderbar-other-view-more"><?php echo __( 'View more', 'baglioni-hotels' ); ?></a>
            </div>
        </div>
        <?php endif; ?>
        <?php endforeach;
        wp_reset_postdata(); ?>
        <?php endif; ?>
    </div>
    <!--sidebar-->
</div>
<!--end container-->

<?php if( $num_boxes > 0 ) : ?>
<div class="nicdark_clearfix" id="around-the-hotel">

    <div class="nicdark_section nicdark_height_60"></div>

    <div class="nicdark_container nicdark_clearfix">

        <div class="nicdark_grid_12">
            <h2 class="room-section-title"><?php echo __( 'Around the Hotel', 'baglioni-hotels' ); ?></h2>
        </div>
        
        <?php if( !empty( $room_first_box_link ) ) : ?>
        <div class="<?php echo $box_class; ?>">
            <div class="custom-box">
                <div class="custom-box-image">
                    <img src="<?php echo $room_first_box_image; ?>" alt="<?php echo $room_first_box_title; ?>" />
                </div>
                <div class="custom-box-data">
                    <h3 class="custom-box-title"><?php echo $room_first_box_title; ?></h3>
                    <p class=""><?php echo $room_first_box_description; ?></p>
                    <div class="custom-box-bottom">
                        <a href="<?php echo $room_first_box_link; ?>" class="custom-box-button"><?php echo __( 'Find out more', 'baglioni-hotels' ); ?></a>
                    </div>
                </div>
            </div>
        </div>
        <?php endif; ?>

        <?php if( !empty( $room_second_box_link ) ) : ?>
        <div class="<?php echo $box_class; ?>">
            <div class="custom-box">
                <div class="custom-box-image">
                    <img src="<?php echo $room_second_box_image; ?>" alt="<?php echo $room_second_box_title; ?>" />
                </div>
                <div class="custom-box-data">
                    <h3 class="custom-box-title"><?php echo $room_second_box_title; ?></h3>
                    <p class=""><?php echo $room_second_box_description; ?></p>
                    <div class="custom-box-bottom">
                        <a href="<?php echo $room_second_box_link; ?>" class="custom-box-button"><?php echo __( 'Find out more', 'baglioni-hotels' ); ?></a>
                    </div>
                </div>
            </div>
        </div>
        <?php endif; ?>

        <?php if( !empty( $room_third_box_link ) ) : ?>
        <div class="<?php echo $box_class; ?>">
            <div class="custom-box">
                <div class="custom-box-image">
                    <img src="<?php echo $room_third_box_image; ?>" alt="<?php echo $room_third_box_title; ?>" />
                </div>
                <div class="custom-box-data">
                    <h3 class="custom-box-title"><?php echo $room_third_box_title; ?></h3>
                    <p class=""><?php echo $room_third_box_description; ?></p>
                    <div class="custom-box-bottom">
                        <a href="<?php echo $room_third_box_link; ?>" class="custom-box-button"><?php echo __( 'Find out more', 'baglioni-hotels' ); ?></a>
                    </div>
                </div>
            </div>
        </div>
        <?php endif; ?>

    </div>

    <div class="nicdark_section nicdark_height_60"></div>

</div>
<?php endif; ?>

<?php if( is_array( $room_similar_suites ) && count( $room_similar_suites ) > 0 ) : ?>
<div class="nicdark_clearfix" id="similar-suites">

    <div class="nicdark_section nicdark_height_60"></div>
    
    <div class="nicdark_container nicdark_clearfix">

        <div class="nicdark_grid_12">
            <h2 class="room-section-title"><?php echo __( 'Similar Suites in our collection', 'baglioni-hotels' ); ?></h2>

            <div class="nicdark_height_20"></div>

            <div class="carousel-suites 2-columns">

                <?php 
                $args = array( 'post_type' => 'room', 'posts_per_page' => -1, 'post__in' => $room_similar_suites );
                $similar_rooms = get_posts( $args );
                foreach( $similar_rooms as $similar_room ) : ?>
                <div class="suite-box">

                    <div class="suite-box-image">
                        <img src="<?php echo get_the_post_thumbnail_url( $similar_room, 'landscape' ); ?>" alt="<?php echo $similar_room->post_title; ?>" />
                        <div class="suite-box-image-title"><?php echo get_room_branch( $similar_room->ID ); ?></div>
                    </div>

                    <div class="suite-box-data">
                        <h3 class="suite-box-title"><?php echo $similar_room->post_title; ?></h3>
                    
                        <div class="suite-box-details">

                            <div class="suite-box-detail">
                                <img width="23" src="<?php echo BAGLIONI_HOTELS_THEME_URL; ?>/images/icon-user-grey.svg">
                                <p><?php echo get_post_meta( $similar_room->ID, 'room-guests', true ); ?></p>
                            </div>

                            <div class="suite-box-detail">
                                <img width="20" src="<?php echo BAGLIONI_HOTELS_THEME_URL; ?>/images/icon-plan-grey.svg">
                                <p><?php echo get_post_meta( $similar_room->ID, 'room-sqm', true ); ?></p>
                            </div>

                        </div>
                
                        <p class=""><?php echo get_the_excerpt_by_post_id( $similar_room->ID ); ?></p>
                        
                        <div class="suite-box-bottom">
                            <a href="<?php echo get_permalink( $similar_room->ID ); ?>" class="suite-box-button"><?php echo __( 'CHECK AVAILABILITY', 'baglioni-hotels' ); ?></a>
                        
                            <?php if( !empty( $room_price ) ) : ?>
                            <div class="suite-box-price"><?php echo __( 'Price from' ); ?> <span class="price"><?php echo $room_price; ?></span></div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <?php endforeach;
                wp_reset_postdata(); ?>

            </div>
        </div>

    </div>

    <div class="nicdark_height_60 nicdark_clearfix"></div>

</div>
<?php endif; ?>

<?php get_footer(); ?>