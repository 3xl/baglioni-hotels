<?php get_header(); ?>

<!--start nicdark_container-->
<div class="nicdark_container nicdark_clearfix">

    <?php if(have_posts()) :
        while(have_posts()) : the_post(); ?>
            <!--start content-->
            <?php the_content(); ?>
            <!--end content-->
        <?php endwhile; ?>
    <?php endif; ?>

</div>
<!--end container-->

<?php get_footer(); ?>