<?php

$image_url = get_the_post_thumbnail_url( get_the_ID(), 'landscape' );

$experience_branch = get_post_meta( get_the_ID(), 'experience-branch', true );

$experience_subtitle = get_post_meta( get_the_ID(), 'experience-subtitle', true );

$experience_category = get_experience_category( get_the_ID() );

$experience_duration = get_post_meta( get_the_ID(), 'experience-duration', true );

$experience_target = get_post_meta( get_the_ID(), 'experience-target', true );

$experience_rating = get_post_meta( get_the_ID(), 'experience-rating', true );

$experience_inclusions = get_post_meta( get_the_ID(), 'experience-inclusions', true );
$experience_conditions = get_post_meta( get_the_ID(), 'experience-conditions', true );

if( !empty( $experience_inclusions ) ) :
    $experience_inclusions = explode( '|', $experience_inclusions );
endif;

if( !empty( $experience_conditions ) ) :
    $experience_conditions = explode( '|', $experience_conditions );
endif;

$experience_book_now = get_post_meta( get_the_ID(), 'experience-book-now', true );

$num_items = 0;
$items = array( 'experience-duration', 'experience-target' );
$item_classes = array(
    1 => 'item-100',
    2 => 'item-50',
    3 => 'item-33',
    4 => 'item-25',
    5 => 'item-20'
);

foreach ( $items as $item ) :
    if( !empty( get_post_meta( get_the_ID(), $item, true ) ) ) :
        $num_items++;
    endif;
endforeach;

if( !empty( $experience_category->name ) ) :
    $num_items++;
endif;

$item_class = $item_classes[$num_items];

get_header(); ?>

<!--start nicdark_container-->
<div class="nicdark_container nicdark_clearfix">

    <div class="nicdark_section nicdark_height_20"></div>

    <div class="nicdark_grid_12">
        <div class="experience-city"><?php echo get_experience_city( get_the_ID() ); ?></div>
        <h1 class="experience-title"><?php the_title(); ?></h1>
        <p class="experience-subtitle"><?php echo $experience_subtitle ?></p>
    </div>

    <div class="nicdark_section nicdark_height_20"></div>

    <!--sidebar-->
    <div class="nicdark_grid_4">
        <?php $args = array( 'post_type' => 'experience', 'posts_per_page' => 5, 'meta_key' => 'experience-branch', 'meta_value' => $experience_branch, 'orderby' => 'rand' );
        $other_experiences = get_posts( $args );
        if( count( $other_experiences ) > 1 ) : ?>
        <h4 class="sidebar-others-title"><?php echo __( 'Other Experiences in', 'baglioni-hotels' ) ?> <?php echo get_experience_city( get_the_ID() ); ?></h4>

        <?php foreach( $other_experiences as $other_experience ) : ?>
        <?php if( $other_experience->ID != get_the_ID() ) : ?>
        <div class="sidebar-other">
            <div class="sidebar-other-image">
                <img src="<?php echo get_the_post_thumbnail_url( $other_experience->ID, 'thumbnail' ); ?>" alt="<?php echo $other_experience->post_title; ?>" />
            </div>
            <div class="sidebar-other-data">
                <h5 class="sidebar-other-title"><?php echo $other_experience->post_title; ?></h5>

                <?php /*if( !empty( get_post_meta( $other_experience->ID, 'experience-small-description', true ) ) ) :
                <p class="sidebar-other-description"><?php echo get_post_meta( $other_experience->ID, 'experience-small-description', true ); ?></p>
                endif;*/ ?>

                <a href="<?php echo get_permalink( $other_experience->ID ); ?>" class="siderbar-other-view-more"><?php echo __( 'View more', 'baglioni-hotels' ); ?></a>
            </div>
        </div>
        <?php endif; ?>
        <?php endforeach;
        wp_reset_postdata(); ?>
        <?php endif; ?>
    </div>
    <!--sidebar-->

    <div class="nicdark_grid_8">

        <img src="<?php echo get_the_post_thumbnail_url( get_the_ID(), 'landscape' ); ?>" alt="<?php the_title(); ?>" class="experience-image" />

        <?php if( $num_items > 0 ) : ?>
        <div class="experience-items">
            <?php if( !empty( $experience_category->name ) ) : ?>
            <div class="item <?php echo $item_class; ?>">
                <img src="<?php echo BAGLIONI_HOTELS_THEME_URL; ?>/images/<?php echo $experience_category->slug; ?>-icon.png" alt="Category Icon" />
                <div class="item-description"><?php echo $experience_category->name; ?></div> 
            </div>
            <?php endif; ?>
            
            <?php if( !empty( $experience_target ) ) : ?>
            <div class="item <?php echo $item_class; ?>">
                <img src="<?php echo BAGLIONI_HOTELS_THEME_URL; ?>/images/<?php echo strtolower( $experience_target ) ?>-icon.png" alt="<?php echo $experience_target ?>" />
                <div class="item-description"><?php echo $experience_target; ?></div> 
            </div>
            <?php endif; ?>

            <?php if( !empty( $experience_duration ) ) : ?>
            <div class="item <?php echo $item_class; ?>">
                <img src="<?php echo BAGLIONI_HOTELS_THEME_URL; ?>/images/clock-icon.png" alt="Clock icon" />
                <div class="item-description"><?php echo $experience_duration; ?></div> 
            </div>
            <?php endif; ?>
            
            <?php /*if( !empty( $experience_rating ) ) :
            <div class="item <?php echo $item_class; ?>">
                <img src="<?php echo BAGLIONI_HOTELS_THEME_URL; ?>/images/rating-icon.png" alt="Rating Icon" />
                <div class="item-description"><?php echo __( 'RATING: ', 'baglioni-hotels' ); ?><?php echo $experience_rating; ?></div> 
            </div>
            endif; */ ?>
        </div>
        <?php endif; ?>

        <div class="divisor-line"></div>

        <div class="experience-content">
            <?php if(have_posts()) :
                while(have_posts()) : the_post(); ?>
                    <!--start content-->
                    <?php the_content(); ?>
                    <!--end content-->
                <?php endwhile; ?>
            <?php endif; ?>
        </div>

        <?php if( is_array( $experience_inclusions ) && count( $experience_inclusions ) > 0 ) : ?>
        <div class="divisor-line"></div>

        <h4 class="specifications-title"><?php echo __( 'The experience includes', 'baglioni-hotels' ); ?></h4>

        <ul class="specifications">
            <?php foreach( $experience_inclusions as $experience_inclusion ) : ?>
            <li><?php echo $experience_inclusion; ?></li>
            <?php endforeach; ?>
        </ul>
        <?php endif; ?>

        <?php if( is_array( $experience_conditions ) && count( $experience_conditions ) > 0 ) : ?>
        <div class="divisor-line"></div>

        <h4 class="specifications-title"><?php echo __( 'Terms and Conditions', 'baglioni-hotels' ); ?></h4>

        <ul class="specifications">
            <?php foreach( $experience_conditions as $experience_condition ) : ?>
            <li><?php echo $experience_condition; ?></li>
            <?php endforeach; ?>
        </ul>
        <?php endif; ?>
        
        <div class="nicdark_section nicdark_height_60"></div>

        <?php if( !empty( $experience_book_now ) ) : ?>
        <a href="<?php echo $experience_book_now; ?>" class="btn-beige" target="_blank"><?php echo __( 'BOOK NOW', 'baglioni-hotels' ); ?></a>
        <?php endif; ?>

        <div class="nicdark_section nicdark_height_40"></div>

    </div>

</div>
<!--end container-->

<?php get_footer(); ?>