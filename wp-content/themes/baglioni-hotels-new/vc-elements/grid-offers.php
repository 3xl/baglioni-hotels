<?php 
/*
Element Description: Grid Offers
*/

class GridOffers extends WPBakeryShortCode {
     
    function __construct() {
        add_action( 'init', array( $this, 'vc_grid_offers_mapping' ) );
        add_shortcode( 'vc_grid_offers', array( $this, 'vc_grid_offers_html' ) );
    }

    public function vc_grid_offers_mapping() {

	    if ( !defined( 'WPB_VC_VERSION' ) ) {
	            return;
	    }

	    $args = array( 'post_type' => 'nd_booking_cpt_4', 'posts_per_page' => -1 );

        $branches_array = get_posts( $args );

        $branches = array( __( 'All', 'baglioni-hotels' ) => '' );

        foreach ( $branches_array as $branch ) :
            $branches[$branch->post_title] = $branch->ID;
        endforeach;

	    vc_map(
	        array(
	        	'name' => __('Grid Offers', 'baglioni-hotels'),
	            'base' => 'vc_grid_offers',
	            'description' => __('This element creates a dynamic offers grid', 'baglioni-hotels'),         
	            'params' => array(
	                array(
                        'type' => 'dropdown',
                        'heading' => __( 'Branch', 'baglioni-hotels' ),
                        'param_name' => 'branch',
                        'value' => $branches,
                        'description' => __( 'What is the branch?', 'baglioni-hotels' ),
                        'admin_label' => false,
                        'weight' => 0,
                    ),
                    array(
	                    'type' => 'textfield',
	                    'holder' => 'div',
	                    'heading' => __( 'Number Offers', 'baglioni-hotels' ),
	                    'param_name' => 'posts_per_page',
	                    'value' => '',
	                    'description' => __( 'Insert a number if you want to limit the offers printed.', 'baglioni-hotels' ),
	                    'admin_label' => false,
	                    'weight' => 0,
	                ),  
	            )
	        )
	    );
    }

    public function vc_grid_offers_html( $atts ) {
	    extract(
	        shortcode_atts(
	            array(
	            	'branch' => '',
	            	'posts_per_page' => -1,
	            ), 
	            $atts
	        )
	    );

	    $meta_query = array( 'relation' => 'AND' );

        if( !empty( $branch ) ) :
            $query = array(
                'key' => 'offer-branch',
                'value' => $branch,
                'compare' => '='
            );

            array_push( $meta_query, $query );
        endif;

	    $args = array( 'post_type' => 'offer', 'posts_per_page' => $posts_per_page, 'meta_query' => $meta_query );

	    $offers = get_posts( $args );

	    $html = '<div class="offers-grid">';

	    foreach( $offers as $offer ) :
	    	
	    	$html .='<div class="nicdark_grid_4">';
	           
	        $html .='<div class="offer">';
	         
	        if( has_post_thumbnail( $offer->ID ) ) :
                $html .= '<div class="offer-thumbnail">';
                $html .= '<img src="' . get_the_post_thumbnail_url( $offer->ID, 'landscape' ) . '" alt="' . $offer->post_title . '" />';
                $html .= '</div>';
	        endif;

	        $html .= '<div class="offer-data">';
            
            $html .= '<h2 class="offer-title">' . $offer->post_title . '</h2>';
            $html .= '<p class="offer-excerpt">' . $offer->post_content .'</p>';
            
            $html .= '<div class="offer-bottom">';
            $html .= '<a href="' . get_post_meta( $offer->ID, 'offer-book-now', true ) . '" class="offer-button" target="_blank">' . __( 'BOOK NOW', 'baglioni-hotels' ) . '</a>';
            $html .= '</div>';
            
            $html .='</div>'; //.offer-data
        	
        	$html .='</div>'; //.offer
	        
	        $html .='</div>'; //.nicdark_grid_4

	    endforeach;
	    wp_reset_postdata();

	    $html .= '</div>'; //.offers-grid
	     
	    return $html;
    } 
     
}

new GridOffers();
