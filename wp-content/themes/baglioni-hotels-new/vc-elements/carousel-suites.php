<?php 
/*
Element Description: VC Carousel Suites
*/

class VCCarouselSuites extends WPBakeryShortCode {
     
    function __construct() {
        add_action( 'init', array( $this, 'vc_carousel_suites' ) );
        add_shortcode( 'vc_carousel_suites', array( $this, 'vc_carousel_suites_html' ) );
    }

    public function vc_carousel_suites() {

	    if ( !defined( 'WPB_VC_VERSION' ) ) {
	            return;
	    }

        $args = array( 'post_type' => 'nd_booking_cpt_4', 'posts_per_page' => -1 );

        $branches_array = get_posts( $args );

        $branches = array( __( 'All', 'baglioni-hotels' ) => '' );

        foreach ( $branches_array as $branch ) :
            $branches[$branch->post_title] = $branch->ID;
        endforeach;

        $num_columns = array(
            __( 'Two columns', 'baglioni-hotels' ) => 2,
            __( 'Three columns', 'baglioni-hotels' ) => 3,
        );

	    vc_map(
	        array(
	        	'name' => __('VC Carousel Suites', 'baglioni-hotels'),
	            'base' => 'vc_carousel_suites',
	            'description' => __('This element creates a carousel of suites with dynamic columns', 'baglioni-hotels'),         
	            'params' => array(
                    array(
                        'type' => 'dropdown',
                        'heading' => __( 'Columns', 'baglioni-hotels' ),
                        'param_name' => 'columns',
                        'value' => $num_columns,
                        'description' => __( 'What is the number of the columns?', 'baglioni-hotels' ),
                        'admin_label' => false,
                        'weight' => 0,
                    ),
                    array(
                        'type' => 'dropdown',
                        'heading' => __( 'Branch', 'baglioni-hotels' ),
                        'param_name' => 'branch',
                        'value' => $branches,
                        'description' => __( 'What is the branch?', 'baglioni-hotels' ),
                        'admin_label' => false,
                        'weight' => 0,
                    ),
                    array(
                        'type' => 'checkbox',
                        'heading' => __( 'Suite', 'baglioni-hotels' ),
                        'param_name' => 'suite',
                        'value' => 1,
                        'description' => __( 'Select if you want to print only the suites.', 'baglioni-hotels' ),
                        'admin_label' => false,
                        'weight' => 0,
                    ),
                    array(
                        'type' => 'checkbox',
                        'heading' => __( 'Highlighted', 'baglioni-hotels' ),
                        'param_name' => 'highlighted',
                        'value' => 1,
                        'description' => __( 'Select if you want to print only the highlighted rooms.', 'baglioni-hotels' ),
                        'admin_label' => false,
                        'weight' => 0,
                    ),
                )
	        )
	    );
    }

    public function vc_carousel_suites_html( $atts ) {
        extract(
            shortcode_atts(
                array(
                    'columns' => 2,
                    'branch' => '',
                    'suite' => 0,
                    'highlighted' => 0,
                ), 
                $atts
            )
        );

        $html = '<div class="carousel-suites ' . $columns .'-columns">';

        $meta_query = array( 'relation' => 'AND' );

        if( !empty( $branch ) ) :
            $query = array(
                'key' => 'room-branch',
                'value' => $branch,
                'compare' => '='
            );

            array_push( $meta_query, $query );
        endif;

        if( $suite ) :
            $query = array(
                'key' => 'room-suite',
                'value' => 1,
                'compare' => '='
            );

            array_push( $meta_query, $query );
        endif;

        if( $highlighted ) :
            $query = array(
                'key' => 'room-highlighted',
                'value' => 1,
                'compare' => '='
            );

            array_push( $meta_query, $query );
        endif;

        $args = array( 'post_type' => 'room', 'posts_per_page' => -1, 'meta_query' => $meta_query, 'order' => 'ASC' );

        $rooms = get_posts( $args );

        foreach( $rooms as $room ) :

            $html .= '<div class="suite-box">';

            $html .= '<div class="suite-box-image">';
            $html .= '<img src="' . get_the_post_thumbnail_url( $room->ID, 'landscape' ) . '" alt="' . $room->post_title . '" />';
            $html .= '<div class="suite-box-image-title">' . get_room_branch( $room->ID ) . '</div>';
            $html .= "</div>"; // .suite-box-image

            $html .= '<div class="suite-box-data">';
            $html .= '<h3 class="suite-box-title">' . $room->post_title . '</h3>';
            $html .= '<div class="suite-box-details">';

            if( !empty( get_post_meta( $room->ID, 'room-guests', true ) ) ) :
                $html .= '<div class="suite-box-detail">';
                $html .= '<img width="23" src="' . get_stylesheet_directory_uri() . '/images/icon-user-grey.svg">';
                $html .= '<p>' . get_post_meta( $room->ID, 'room-guests', true ) . '</p>';
                $html .= '</div>'; // .suite-box-detail
            endif;

            if( !empty( get_post_meta( $room->ID, 'room-sqm', true ) ) ) :
                $html .= '<div class="suite-box-detail">';
                $html .= '<img width="20" src="' . get_stylesheet_directory_uri() . '/images/icon-plan-grey.svg">';
                $html .= '<p>' . get_post_meta( $room->ID, 'room-sqm', true ) . '</p>';
                $html .= '</div>'; // .suite-box-detail
            endif;

            $html .= '</div>'; // .suite-box-details
            $html .= '<p class="">' . get_the_excerpt_by_post_id( $room->ID ) . '</p>';
            
            $html .= '<div class="suite-box-bottom">';
            $html .= '<a href="' . get_permalink( $room->ID ) . '" class="suite-box-button">' . __( 'CHECK AVAILABILITY', 'baglioni-hotels' ) . '</a>';
            
            if( !empty( get_post_meta( $room->ID, 'room-price', true ) ) ) :
                $html .= '<div class="suite-box-price">' . __( 'Price from' ) . '<span class="price">' . get_post_meta( $room->ID, 'room-price', true ) . '</span></div>';
            endif;

            $html .= '</div>'; // .suite-box-bottom

            $html .= "</div>"; // .suite-box-data

            $html .= "</div>"; // .suite-box

        endforeach;
        wp_reset_postdata();

        $html .= "</div>"; // .carousel-suites

        return $html;
    } 
     
}

new VCCarouselSuites();
