<?php 
/*
Element Description: VC Slider Experiences
*/

class VCSliderExperiences extends WPBakeryShortCode {
     
    function __construct() {
        add_action( 'init', array( $this, 'vc_slider_experiences' ) );
        add_shortcode( 'vc_slider_experiences', array( $this, 'vc_slider_experiences_html' ) );
    }

    public function vc_slider_experiences() {

	    if ( !defined( 'WPB_VC_VERSION' ) ) {
	            return;
	    }

        $cities = array( __( 'All', 'baglioni-hotels' ) => '' );

        foreach ( get_cities() as $city ) :
            $cities[$city] = $city;
        endforeach;

	    vc_map(
	        array(
	        	'name' => __('VC Slider Experiences', 'baglioni-hotels'),
	            'base' => 'vc_slider_experiences',
	            'description' => __('This element creates a dynamic slider experiences', 'baglioni-hotels'),         
	            'params' => array(
                    array(
                        'type' => 'dropdown',
                        'heading' => __( 'City', 'baglioni-hotels' ),
                        'param_name' => 'city',
                        'value' => $cities,
                        'description' => __( 'What is the city?', 'baglioni-hotels' ),
                        'admin_label' => false,
                        'weight' => 0,
                    ),
                    array(
                        'type' => 'checkbox',
                        'heading' => __( 'Highlighted', 'baglioni-hotels' ),
                        'param_name' => 'highlighted',
                        'value' => 1,
                        'description' => __( 'Select if you want to print only the highlighted experiences.', 'baglioni-hotels' ),
                        'admin_label' => false,
                        'weight' => 0,
                    )
                )
	        )
	    );
    }

    public function vc_slider_experiences_html( $atts ) {
        extract(
            shortcode_atts(
                array(
                    'city' => '',
                    'highlighted' => 0,
                ), 
                $atts
            )
        );

        if( !empty( $city ) ) :
            $args = array( 'post_type' => 'nd_booking_cpt_4', 'posts_per_page' => -1, 'meta_key' => 'nd_booking_meta_box_cpt_4_city', 'meta_value' => $city, 'fields' => 'ids' );

            $branches = get_posts( $args );
        endif;

        $html = '<div class="slider-experiences">';

        $terms = get_terms( array(
            'taxonomy' => 'experience_category',
            'hide_empty' => false,
        ) );

        foreach( $terms as $term ) :

            $meta_query = array( 'relation' => 'AND' );

            if( !empty( $city ) ) :
                $query = array(
                    'key' => 'experience-branch',
                    'value' => $branches,
                    'compare' => 'IN'
                );

                array_push( $meta_query, $query );
            endif;

            if( $highlighted ) :
                $query = array(
                    'key' => 'experience-highlighted',
                    'value' => 1,
                    'compare' => '='
                );

                array_push( $meta_query, $query );
            endif;

            $args = array(
                'post_type' => 'experience',
                'posts_per_page' => 4,
                'meta_query' => $meta_query,
                'tax_query' => array(
                    array(
                        'taxonomy' => 'experience_category',
                        'field' => 'id',
                        'terms' => $term->term_id,
                        'include_children' => false
                    )
                )
            );

            $experiences = get_posts( $args );

            if( count( $experiences ) > 0 ) :

                $html .= '<div class="category-experiences">';

                $html .= '<h2 class="category-title">' . __( 'Exclusive Experiences', 'baglioni-hotels' ) . '</h2>';

                $html .= '<h3 class="exclusive-experiences-title">' . $term->name . '</h3>';
                $html .= '<div class="exclusive-experiences-subtitle">' . term_description( $term->term_id, 'experience-category' ) . '</div>';

                $html .= '<div class="experiences">';

                $html .= '<div class="row">';

                $html .= '<div class="column">';

                $html .= '<div class="main-experience">';

                foreach( $experiences as $experience ) :
                    $html .= '<div class="experience">';

                    $html .= '<div class="experience-image" style="background-image: url(' . get_the_post_thumbnail_url( $experience->ID, 'landscape-low' ) . ')">';

                    $html .= '</div>'; // .experience-image

                    $html .= '<div class="experience-data">';

                    $html .= '<div class="experience-city">' . get_experience_city( $experience->ID ) . '</div>';

                    $html .= '<h3 class="experience-title">' . $experience->post_title . '</h3>';

                    $html .= '<div class="experience-details">';

                    if( !empty( get_post_meta( $experience->ID, 'experience-target', true ) ) ) :
                        $html .= '<div class="experience-detail">';
                        $html .= '<img width="23" src="' . get_stylesheet_directory_uri() . '/images/' . strtolower( get_post_meta( $experience->ID, 'experience-target', true ) ) . '-icon.png">';
                        $html .= '<p>' . get_post_meta( $experience->ID, 'experience-target', true ) . '</p>';
                        $html .= '</div>'; // .experience-details
                    endif;

                    //if( !empty( get_post_meta( $experience->ID, 'experience-duration', true ) ) ) :
                        $html .= '<div class="experience-detail">';
                        $html .= '<img width="23" src="' . get_stylesheet_directory_uri() . '/images/clock-icon.png" alt="Clock icon">';
                        $html .= '<p>' . get_post_meta( $experience->ID, 'experience-duration', true ) . '</p>';
                        $html .= '</div>'; // .experience-details
                    //endif;

                    /*if( get_post_meta( $experience->ID, 'experience-couple', true ) == 1 ) :
                        $html .= '<div class="experience-detail">';
                        $html .= '<img width="23" src="' . get_stylesheet_directory_uri() . '/images/couple.png">';
                        $html .= '<p>' . __( 'COUPLE', 'baglioni-hotels' ) .'</p>';
                        $html .= '</div>'; // .experience-detail
                    endif;*/

                    $html .= '</div>'; // .experience-details

                    $html .= '<p class="experience-description">' . get_the_excerpt_by_post_id( $experience->ID ) . '</p>';

                    $html .= '<a href="' . get_permalink( $experience->ID ) . '" class="experience-button">' . __( 'CHECK AVAILABILITY', 'baglioni-hotels' ) . '</a>';

                    $html .= '</div>'; // .experience-data

                    $html .= '</div>'; // .experience
                endforeach;
                wp_reset_postdata();

                $html .= '<div class="squares-container">';
            
                foreach( $experiences as $index => $experience ) :
                    $html .= '<a rel="" href="#" class="nicdark_display_inline_block nd_options_font_weight_lighter nd_options_second_font ' . ( ( $index == 0 ) ? ' active' : '' ) . ' square-experience" data-index="' . $index . '"></a>'; // .test buttons
                
                endforeach;

                $html .= '</div>';

                $html .= '</div>'; // .main-experience

                $html .= '</div>'; // .column

                $html .= '<div class="column">';

                $html .= '<div class="other-experiences">';

                $html .= '<div class="row">';

                foreach( $experiences as $index => $experience ) :
                    $html .= '<div class="column">';

                    $html .= '<div class="experience' . ( ( $index == 0 ) ? ' active' : '' ) . '" data-index="' . $index . '">';
                    $html .= '<a href="#">';
                    $html .= '<div class="experience-image">';
                    $html .= '<img src="' . get_the_post_thumbnail_url( $experience->ID, 'semi-square' ) . '" alt="' . $experience->post_title . '">';
                    $html .= '<div class="experience-image-title">' . $experience->post_title .'<span class="experience-location">' . get_experience_city( $experience->ID ) . '</span></div>';
                    $html .= '</div>'; // .experience-image
                    $html .= '</a>';
                    $html .= '</div>'; // .experience

                    $html .= '</div>'; // .column
                endforeach;
                wp_reset_postdata();
                
                $html .= '</div>'; // .row

                $html .= '</div>'; // .other-experiences

                $html .= '</div>'; // .column

                $html .= '</div>'; // .row

                $html .= '</div>'; // .experiences

                $html .= "</div>"; // .category-experiences

            endif;

        endforeach;

        $html .= "</div>"; // .slider-experiences

        return $html;
    } 
     
}

new VCSliderExperiences();
