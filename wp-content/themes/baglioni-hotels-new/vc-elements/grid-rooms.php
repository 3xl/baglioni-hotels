<?php 
/*
Element Description: Grid Rooms
*/

class GridRooms extends WPBakeryShortCode {
     
    function __construct() {
        add_action( 'init', array( $this, 'vc_grid_rooms_mapping' ) );
        add_shortcode( 'vc_grid_rooms', array( $this, 'vc_grid_rooms_html' ) );
    }

    public function vc_grid_rooms_mapping() {

	    if ( !defined( 'WPB_VC_VERSION' ) ) {
	            return;
	    }

	    $args = array( 'post_type' => 'nd_booking_cpt_4', 'posts_per_page' => -1 );

        $branches_array = get_posts( $args );

        $branches = array( __( 'All', 'baglioni-hotels' ) => '' );

        foreach ( $branches_array as $branch ) :
            $branches[$branch->post_title] = $branch->ID;
        endforeach;

	    $room_types = array(
	    	__( 'All', 'baglioni-hotels' ) => 'all',
            __( 'Suite', 'baglioni-hotels' ) => 'suite',
            __( 'Room', 'baglioni-hotels' ) => 'room',
        );

	    vc_map(
	        array(
	        	'name' => __('Grid Rooms', 'baglioni-hotels'),
	            'base' => 'vc_grid_rooms',
	            'description' => __('This element creates a dynamic rooms grid', 'baglioni-hotels'),         
	            'params' => array(
	                array(
                        'type' => 'dropdown',
                        'heading' => __( 'Branch', 'baglioni-hotels' ),
                        'param_name' => 'branch',
                        'value' => $branches,
                        'description' => __( 'What is the branch?', 'baglioni-hotels' ),
                        'admin_label' => false,
                        'weight' => 0,
                    ),
	                array(
                        'type' => 'dropdown',
                        'heading' => __( 'Room type', 'baglioni-hotels' ),
                        'param_name' => 'room_type',
                        'value' => $room_types,
                        'description' => __( 'What is the type of room that do you want to print?', 'baglioni-hotels' ),
                        'admin_label' => false,
                        'weight' => 0,
                    ),
                    array(
	                    'type' => 'textfield',
	                    'holder' => 'div',
	                    'heading' => __( 'Number Rooms', 'baglioni-hotels' ),
	                    'param_name' => 'posts_per_page',
	                    'value' => '',
	                    'description' => __( 'Insert a number if you want to limit the rooms printed.', 'baglioni-hotels' ),
	                    'admin_label' => false,
	                    'weight' => 0,
	                ),  
	            )
	        )
	    );
    }

    public function vc_grid_rooms_html( $atts ) {
	    extract(
	        shortcode_atts(
	            array(
	            	'branch' => '',
	            	'room_type' => '',
	            	'posts_per_page' => -1,
	            ), 
	            $atts
	        )
	    );

	    $meta_query = array( 'relation' => 'AND' );

        if( !empty( $branch ) ) :
            $query = array(
                'key' => 'room-branch',
                'value' => $branch,
                'compare' => '='
            );

            array_push( $meta_query, $query );
        endif;

        if( $room_type == 'suite' ) :
            $query = array(
                'key' => 'room-suite',
                'value' => 1,
                'compare' => '='
            );

            array_push( $meta_query, $query );
        endif;

        if( $room_type == 'room' ) :
            $query = array(
                'key' => 'room-suite',
                'value' => 0,
                'compare' => '='
            );

            array_push( $meta_query, $query );
        endif;

	    $args = array( 'post_type' => 'room', 'posts_per_page' => $posts_per_page, 'meta_query' => $meta_query, 'order' => 'ASC' );

	    $rooms = get_posts( $args );

	    $html = '<div class="rooms-grid">';

	    foreach( $rooms as $room ) :
	    	
	    	$html .='<div class="nicdark_grid_4">';
	           
	        $html .='<div class="room">';
	         
	        if( has_post_thumbnail( $room->ID ) ) :
                $html .= '<div class="room-thumbnail">';
                $html .= '<img src="' . get_the_post_thumbnail_url( $room->ID, 'landscape' ) . '" alt="' . $room->post_title . '" />';
                $html .= '</div>';
	        endif;

	        $html .= '<div class="room-data">';
            
            $html .= '<h2 class="room-title">' . $room->post_title . '</h2>';
            $html .= '<p class="room-excerpt">' . get_the_excerpt_by_post_id( $room->ID ) .'</p>';
            
            $html .= '<div class="room-bottom">';
            $html .= '<a href="' . get_the_permalink( $room->ID ) . '" class="room-button">' . __( 'CHECK AVAILABILITY', 'baglioni-hotels' ) . '</a>';
            
            if( !empty( get_post_meta( $room->ID, 'room-price', true ) ) ) :
                $html .= '<div class="room-price">' . __( 'Price from' ) . '<span class="price">' . get_post_meta( $room->ID, 'room-price', true ) . '</span></div>';
            endif;

            $html .='</div>';
            
            $html .='</div>'; //.room-data
        	
        	$html .='</div>'; //.room
	        
	        $html .='</div>'; //.nicdark_grid_4

	    endforeach;
	    wp_reset_postdata();

	    $html .= '</div>'; //.rooms-grid
	     
	    return $html;
    } 
     
}

new GridRooms();
