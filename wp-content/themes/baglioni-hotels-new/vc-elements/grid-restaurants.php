<?php 
/*
Element Description: Grid Restaurants
*/

class GridRestaurants extends WPBakeryShortCode {
     
    function __construct() {
        add_action( 'init', array( $this, 'vc_grid_restaurants_mapping' ) );
        add_shortcode( 'vc_grid_restaurants', array( $this, 'vc_grid_restaurants_html' ) );
    }

    public function vc_grid_restaurants_mapping() {

	    if ( !defined( 'WPB_VC_VERSION' ) ) {
	            return;
	    }

	    $args = array( 'post_type' => 'nd_booking_cpt_4', 'posts_per_page' => -1 );

        $branches_array = get_posts( $args );

        $branches = array( __( 'All', 'baglioni-hotels' ) => '' );

        foreach ( $branches_array as $branch ) :
            $branches[$branch->post_title] = $branch->ID;
        endforeach;

        $num_columns = array(
        	__( 'Three Columns', 'baglioni-hotels' ) => 4,
        	__( 'Two Columns', 'baglioni-hotels' ) => 6,
        );

	    vc_map(
	        array(
	        	'name' => __('Grid Restaurants', 'baglioni-hotels'),
	            'base' => 'vc_grid_restaurants',
	            'description' => __('This element creates a dynamic restaurants grid', 'baglioni-hotels'),         
	            'params' => array(
	                array(
                        'type' => 'dropdown',
                        'heading' => __( 'Branch', 'baglioni-hotels' ),
                        'param_name' => 'branch',
                        'value' => $branches,
                        'description' => __( 'What is the branch?', 'baglioni-hotels' ),
                        'admin_label' => false,
                        'weight' => 0,
                    ),
                    array(
                        'type' => 'dropdown',
                        'heading' => __( 'Number Columns', 'baglioni-hotels' ),
                        'param_name' => 'columns',
                        'value' => $num_columns,
                        'description' => __( 'How many columns for the grid?', 'baglioni-hotels' ),
                        'admin_label' => false,
                        'weight' => 0,
                    ),
                    array(
                        'type' => 'dropdown',
                        'heading' => __( 'What are the type of restaurants that you want to print?', 'baglioni-hotels' ),
                        'param_name' => 'type',
                        'value' => array(
                        	__( 'All', 'baglioni-hotels' ) => '',
                        	__( 'Highlighted', 'baglioni-hotels' ) => 'highlighted',
                        	__( 'Not Highlighted', 'baglioni-hotels' ) => 'not-highlighted',
                        ),
                        'description' => __( 'How many columns for the grid?', 'baglioni-hotels' ),
                        'admin_label' => false,
                        'weight' => 0,
                    ),
                    array(
	                    'type' => 'textfield',
	                    'heading' => __( 'Number Restaurants', 'baglioni-hotels' ),
	                    'param_name' => 'posts_per_page',
	                    'value' => '',
	                    'description' => __( 'Insert a number if you want to limit the restaurants printed.', 'baglioni-hotels' ),
	                    'admin_label' => false,
	                    'weight' => 0,
	                ),  
	            )
	        )
	    );
    }

    public function vc_grid_restaurants_html( $atts ) {
	    extract(
	        shortcode_atts(
	            array(
	            	'branch' => '',
	            	'columns' => 4,
	            	'type' => '',
	            	'posts_per_page' => -1,
	            ), 
	            $atts
	        )
	    );

	    $meta_query = array( 'relation' => 'AND' );

        if( !empty( $branch ) ) :
            $query = array(
                'key' => 'restaurant-branch',
                'value' => $branch,
                'compare' => '='
            );

            array_push( $meta_query, $query );
        endif;

        if( !empty( $type ) && $type == 'highlighted' ) :
            $query = array(
                'key' => 'restaurant-highlighted',
                'value' => 1,
                'compare' => '='
            );

            array_push( $meta_query, $query );
        elseif( !empty( $type ) && $type == 'not-highlighted' ) :
        	$query = array(
                'key' => 'restaurant-highlighted',
                'value' => 1,
                'compare' => '!='
            );

            array_push( $meta_query, $query );
        endif;

	    $args = array( 'post_type' => 'restaurant', 'posts_per_page' => $posts_per_page, 'meta_query' => $meta_query, 'order' => 'ASC' );

	    $restaurants = get_posts( $args );

	    $html = '<div class="restaurants-grid">';

	    foreach( $restaurants as $restaurant ) :
	    	
	    	$html .='<div class="nicdark_grid_' . $columns . '">';
	           
	        $html .='<div class="restaurant">';
	         
	        if( has_post_thumbnail( $restaurant->ID ) ) :
                $html .= '<div class="restaurant-thumbnail">';
                $html .= '<img src="' . get_the_post_thumbnail_url( $restaurant->ID, 'landscape' ) . '" alt="' . $restaurant->post_title . '" />';
                $html .= '<div class="restaurant-thumbnail-title">' . get_restaurant_branch( $restaurant->ID ) . '</div>';
                $html .= '</div>';
	        endif;

	        $html .= '<div class="restaurant-data">';
            
            $html .= '<h2 class="restaurant-title">' . $restaurant->post_title . '</h2>';
            $html .= '<p class="restaurant-excerpt">' . get_the_excerpt_by_post_id( $restaurant->ID ) .'</p>';
            
            $html .= '<div class="restaurant-bottom">';
            $html .= '<a href="#" class="restaurant-button">' . __( 'BOOK NOW YOUR TABLE', 'baglioni-hotels' ) . '</a>';
            $html .= '<a href="' . get_the_permalink( $restaurant->ID ) . '" class="restaurant-full-info">' . __( 'Full info', 'baglioni-hotels' ) . '</a>';
            $html .= '</div>';
            
            $html .='</div>'; //.restaurant-data
        	
        	$html .='</div>'; //.restaurant
	        
	        $html .='</div>'; //.nicdark_grid_4

	    endforeach;
	    wp_reset_postdata();

	    $html .= '</div>'; //.restaurants-grid
	     
	    return $html;
    } 
     
}

new GridRestaurants();
