<?php 
/*
Element Description: Multiple Button
*/

class MultipleButton extends WPBakeryShortCode {
     
    function __construct() {
        add_action( 'init', array( $this, 'vc_multiple_button_mapping' ) );
        add_shortcode( 'vc_multiple_button', array( $this, 'vc_multiple_button_html' ) );
    }

    public function vc_multiple_button_mapping() {

	    if ( !defined( 'WPB_VC_VERSION' ) ) {
	            return;
	    }

	    vc_map(
	        array(
	        	'name' => __('Multiple Button', 'baglioni-hotels'),
	            'base' => 'vc_multiple_button',
	            'description' => __('This element creates a dynamic multiple button', 'baglioni-hotels'),         
	            'params' => array(
	            	array(
	                    'type' => 'textfield',
	                    'holder' => 'div',
	                    'heading' => __( 'Button text', 'baglioni-hotels' ),
	                    'param_name' => 'button_text',
	                    'value' => '',
	                    'description' => __( 'Text for the button', 'baglioni-hotels' ),
	                    'admin_label' => false,
	                    'weight' => 0,
	                ),

	                array(
	                    'type' => 'textarea',
	                    'holder' => 'div',
	                    'heading' => __( 'Options', 'baglioni-hotels' ),
	                    'param_name' => 'options',
	                    'value' => '',
	                    'description' => __( 'Options', 'baglioni-hotels' ),
	                    'admin_label' => false,
	                    'weight' => 0,
	                ),
	                  
	                array(
	                    'type' => 'textarea',
	                    'holder' => 'div',
	                    'heading' => __( 'Options links', 'baglioni-hotels' ),
	                    'param_name' => 'options_links',
	                    'value' => '',
	                    'description' => __( 'Options links', 'baglioni-hotels' ),
	                    'admin_label' => false,
	                    'weight' => 0,
	                ),

	                array(
	                    'type' => 'textfield',
	                    'holder' => 'div',
	                    'heading' => __( 'Custom Class', 'baglioni-hotels' ),
	                    'param_name' => 'custom_class',
	                    'value' => '',
	                    'description' => __( 'Custom Class', 'baglioni-hotels' ),
	                    'admin_label' => false,
	                    'weight' => 0,
	                ),
	                     
	            )
	        )
	    );
    }

    public function vc_multiple_button_html( $atts ) {
	    extract(
	        shortcode_atts(
	            array(
	            	'button_text' => __( 'Button', 'baglioni-hotels' ),
	                'options' => '',
	                'options_links' => '',
	                'custom_class' => '',
	            ), 
	            $atts
	        )
	    );

	    $arrayOptions = explode('; ', $options);
	    $arrayOptionsLinks = explode('; ', $options_links);

	    $html = '<div class="multiple-button' . ( !empty( $custom_class ) ? ' ' . $custom_class : '' ) .'">';

	    $html .= '<select name="multiple-button" class="nice-select" >';

	    $html .= '<option value="">' . $button_text . '</option>';

	    foreach ( $arrayOptions as $index => $option ) :
	    	
	    	$html .= '<option value="' . $arrayOptionsLinks[$index] . '">' . $option . '</option>';

	    endforeach;

	    $html .= '</select>';

	    $html .= '</div>';
	     
	    return $html;
    } 
     
}

new MultipleButton();
