<?php 
/*
Element Description: VC Slider Rooms
*/

class VCSliderRooms extends WPBakeryShortCode {
     
    function __construct() {
        add_action( 'init', array( $this, 'vc_slider_rooms' ) );
        add_shortcode( 'vc_slider_rooms', array( $this, 'vc_slider_rooms_html' ) );
    }

    public function vc_slider_rooms() {

	    if ( !defined( 'WPB_VC_VERSION' ) ) {
	            return;
	    }

	    vc_map(
	        array(
	        	'name' => __('VC Slider Rooms', 'baglioni-hotels'),
	            'base' => 'vc_slider_rooms',
	            'description' => __('This element creates rooms', 'baglioni-hotels'),         
	            'params' => array(
                    array(
                        'type' => 'checkbox',
                        'heading' => __( 'Suite', 'baglioni-hotels' ),
                        'param_name' => 'suite',
                        'value' => 1,
                        'description' => __( 'Select if you want to print only the suites.', 'baglioni-hotels' ),
                        'admin_label' => false,
                        'weight' => 0,
                    ),
                    array(
                        'type' => 'checkbox',
                        'heading' => __( 'Highlighted', 'baglioni-hotels' ),
                        'param_name' => 'highlighted',
                        'value' => 1,
                        'description' => __( 'Select if you want to print only the highlighted rooms.', 'baglioni-hotels' ),
                        'admin_label' => false,
                        'weight' => 0,
                    ),

                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => __( "Exclusions", "baglioni-hotels" ),
                        "param_name" => "exclusions",
                        "value" => '',
                        "description" => __( "Insert the ids of the branches that you want exclude divided by comma.", "baglioni-hotels" )
                    ),
                )
	        )
	    );
    }

    public function vc_slider_rooms_html( $atts ) {
        extract(
            shortcode_atts(
                array(
                    'suite' => 0,
                    'highlighted' => 0,
                    'exclusions' => '',
                ), 
                $atts
            )
        );

        if( !empty( $exclusions ) ) :
            $exclusions = explode( ',', $exclusions );
        else :
            $exclusions = array();
        endif;

        $html = '<div class="slider-rooms">';

        $args = array( 'post_type' => 'nd_booking_cpt_4', 'posts_per_page' => -1 );

        $branches = get_posts( $args );

        foreach( $branches as $branch ) :

            if( in_array( $branch->ID, $exclusions ) ) :
                continue;
            endif;
            
            $args = array(
                'post_type' => 'nd_booking_cpt_1',
                'posts_per_page' => 5,
                'order' => 'DESC',
                'orderby' => 'date',
                'meta_query' => array(
                    array(
                        'key' => 'nd_booking_meta_box_branches',
                        'type' => 'numeric',
                        'value' => $branch->ID,
                        'compare' => '=',
                    ),
                )
            );

            $html .= '<div class="branch-rooms">';

            $html .= '<img src="' . get_the_post_thumbnail_url( $branch->ID, 'full' ) . '" class="branch-image">';
            $html .= '<div class="branch-location">' . get_branch_city( $branch->ID ) . '</div>';
            $html .= '<h2 class="branch-title">' . get_branch_name( $branch->ID ) . '</h2>';

            $html .= '<h3 class="best-suites-title">' . __( 'OUR UNFORGETTABLE SUITES', 'baglioni-hotels' ) . '</h3>';
            $html .= '<p class="best-suites-subtitle" style="font-style: italic;">' . __( 'All our rooms and suites are as luxurious as the most elegant italian houses', 'baglioni-hotels' ) . '</p>';
            $html .= '<p class="best-suites-subtitle">' . __( 'Roberto Polito – Founder Baglioni Hotels', 'baglioni-hotels' ) . '</p>';

            $html .= '<div class="rooms">';

            $html .= '<div class="row">';

            $html .= '<div class="column">';

            $html .= '<div class="main-room">';

            $meta_query = array( 'relation' => 'AND' );

            $query = array(
                'key' => 'room-branch',
                'value' => $branch->ID,
                'compare' => '='
            );
            array_push( $meta_query, $query );

            if( $suite ) :
                $query = array(
                    'key' => 'room-suite',
                    'value' => 1,
                    'compare' => '='
                );

                array_push( $meta_query, $query );
            endif;

            if( $highlighted ) :
                $query = array(
                    'key' => 'room-highlighted',
                    'value' => 1,
                    'compare' => '='
                );

                array_push( $meta_query, $query );
            endif;

            $args = array( 'post_type' => 'room', 'posts_per_page' => 4, 'meta_query' => $meta_query, 'order' => 'ASC' );

            $rooms = get_posts( $args );

            foreach ( $rooms as $room ) :
                $html .= '<div class="room">';

                $html .= '<div class="room-image" style="background-image: url(' . get_the_post_thumbnail_url( $room->ID, 'landscape-low' ) . ');">';

                $html .= '</div>'; // .room-image

                $html .= '<div class="room-data">';

                $html .= '<div class="room-branch">' . $branch->post_title . '</div>';

                $html .= '<h3 class="room-title">' . $room->post_title .'</h3>';

                $html .= '<div class="room-details">';

                if( !empty( get_post_meta( $room->ID, 'room-guests', true ) ) ) :
                    $html .= '<div class="room-detail">';
                    $html .= '<img width="23" src="' . get_stylesheet_directory_uri() . '/images/icon-user-grey.svg">';
                    $html .= '<p class="room-guests">' . get_post_meta( $room->ID, 'room-guests', true ) . '</p>';
                    $html .= '</div>'; // .room-detail
                endif;

                if( !empty( get_post_meta( $room->ID, 'room-sqm', true ) ) ) :
                    $html .= '<div class="room-detail">';
                    $html .= '<img width="20" src="' . get_stylesheet_directory_uri() . '/images/icon-plan-grey.svg">';
                    $html .= '<p class="room-sqm">' . get_post_meta( $room->ID, 'room-sqm', true ) . '</p>';
                    $html .= '</div>'; // .room-detail
                endif;

                $html .= '</div>'; // .room-details

                $html .= '<p class="room-description">' . get_the_excerpt_by_post_id( $room->ID ) . '</p>';

                $html .= '<div class="room-data-bottom">';

                $html .= '<a href="' . get_permalink( $room->ID ) . '" class="room-button">' . __( 'CHECK AVAILABILITY', 'baglioni-hotels' ) . '</a>';

                if( !empty( get_post_meta( $room->ID, 'room-price', true ) ) ) :
                    $html .= '<div class="room-price">' . __( 'Price from' ) . '<span class="price">' . get_post_meta( $room->ID, 'room-price', true ) . '</span></div>';
                endif;

                $html .= '</div>'; // .room-data-bottom

                $html .= '</div>'; // .room-data

                $html .= '</div>'; // .room
            endforeach;
            wp_reset_postdata();

            $html .= '<div class="squares-container">';
            
            foreach ( $rooms as $index => $room ) :
                $html .= '<a rel="" href="#" class="nicdark_display_inline_block nd_options_font_weight_lighter nd_options_second_font ' . ( ( $index == 0 ) ? ' active' : '' ) . ' square-room" data-index="' . $index . '"></a>'; // .test buttons
            
            endforeach;

            $html .= '</div>';

            $html .= '</div>'; // .main-room

            $html .= '</div>'; // .column

            $html .= '<div class="column">';

            $html .= '<div class="other-rooms">';

            $html .= '<div class="row">';

            foreach ( $rooms as $index => $room ) :
                $html .= '<div class="column">';

                $html .= '<div class="room' . ( ( $index == 0 ) ? ' active' : '' ) . '" data-index="' . $index . '">';
                $html .= '<a href="#">';
                $html .= '<div class="room-image">';
                $html .= '<img src="' . get_the_post_thumbnail_url( $room->ID, 'semi-square' ) . '">';
                $html .= '<div class="room-image-title">' . $room->post_title . '<span class="room-location">' . get_room_city( $room->ID ) . '</span></div>';
                $html .= '</div>'; // .room-image
                $html .= '</a>';
                $html .= '</div>'; // .room

                $html .= '</div>'; // .column
            endforeach;

            wp_reset_postdata();

            $html .= '</div>'; // .row

            $html .= '</div>'; // .other-rooms

            $html .= '</div>'; // .column

            $html .= '</div>'; // .row

            $html .= '</div>'; // .rooms

            $html .= "</div>"; // .branch-rooms

        endforeach;
        wp_reset_postdata();

        $html .= "</div>"; // .slider-rooms

        return $html;
    } 
     
}

new VCSliderRooms();
