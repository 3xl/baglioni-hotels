<?php 
/*
Element Description: VC Custom Box
*/

class CustomBox extends WPBakeryShortCode {
     
    function __construct() {
        add_action( 'init', array( $this, 'vc_custom_box' ) );
        add_shortcode( 'vc_custom_box', array( $this, 'vc_custom_box_html' ) );
    }

    public function vc_custom_box() {

	    if ( !defined( 'WPB_VC_VERSION' ) ) {
	            return;
	    }

	    vc_map(
	        array(
	        	'name' => __('VC Custom Box', 'baglioni-hotels'),
	            'base' => 'vc_custom_box',
	            'description' => __('This element prints a custom box', 'baglioni-hotels'),         
	            'params' => array(
                    array(
                        "type" => "attach_image",
                        "class" => "",
                        "heading" => __( "Image", "baglioni-hotels" ),
                        "param_name" => "box_image",
                        "value" => '',
                        "description" => __( "Attach an image.", "baglioni-hotels" )
                    ),

                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => __( "Image caption", "baglioni-hotels" ),
                        "param_name" => "box_image_caption",
                        "value" => '',
                        "description" => __( "The caption of the image", "baglioni-hotels" )
                    ),

                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => __( "Title", "baglioni-hotels" ),
                        "param_name" => "box_title",
                        "value" => '',
                        "description" => __( "The title of the box", "baglioni-hotels" )
                    ),

                    array(
                        "type" => "textarea",
                        "class" => "",
                        "heading" => __( "Description", "baglioni-hotels" ),
                        "param_name" => "box_description",
                        "value" => '',
                        "description" => __( "Attach an image.", "baglioni-hotels" )
                    ),

                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => __( "Box button text", "baglioni-hotels" ),
                        "param_name" => "box_button_text",
                        "value" => '',
                        "description" => __( "The text inside the button.", "baglioni-hotels" )
                    ),

                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => __( "Box button URL", "baglioni-hotels" ),
                        "param_name" => "box_button_url",
                        "value" => '',
                        "description" => __( "The link of the button", "baglioni-hotels" )
                    ),

                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => __( "Box full info text", "baglioni-hotels" ),
                        "param_name" => "box_full_info_text",
                        "value" => '',
                        "description" => __( "The text for the full info link", "baglioni-hotels" )
                    ),

                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => __( "Box full info URL", "baglioni-hotels" ),
                        "param_name" => "box_full_info_url",
                        "value" => '',
                        "description" => __( "The link of the full info", "baglioni-hotels" )
                    ),

                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => __( "Custom Class", "baglioni-hotels" ),
                        "param_name" => "box_custom_class",
                        "value" => '',
                        "description" => __( "Custom class for the box", "baglioni-hotels" )
                    ),
                ),
	        )
	    );
    }

    public function vc_custom_box_html( $atts ) {

        extract(
            shortcode_atts(
                array(
                    'box_image' => '',
                    'box_image_caption',
                    'box_title' => '',
                    'box_description' => '',
                    'box_button_text' => '',
                    'box_button_url' => '',
                    'box_full_info_text' => '',
                    'box_full_info_url' => '',
                    'box_custom_class' => '',
                ), 
                $atts
            )
        );

        $image_url = wp_get_attachment_image_src( $box_image, 'landscape' )[0];

        $html = '<div class="custom-box' . ( !empty( $box_custom_class ) ? ' ' . $box_custom_class : '' ) . '">';

        $html .= '<div class="custom-box-image">';
        $html .= '<img src="' . $image_url .'" alt="' . $box_title . '" />';

        if( !empty( $box_image_caption ) ) :
            $html .= '<div class="custom-box-image-title">' . $box_image_caption . '</div>';
        endif;
        
        $html .= "</div>"; // .custom-box-image

        $html .= '<div class="custom-box-data">';
        $html .= '<h3 class="custom-box-title">' . $box_title . '</h3>';
        $html .= '<p class="">' . $box_description . '</p>';

        if( !empty( $box_button_text ) || !empty( $box_full_info_text ) ) :

            $html .= '<div class="custom-box-bottom">';

            if( !empty( $box_button_text ) ) :
                $html .= '<a href="' . $box_button_url . '" class="custom-box-button">' . $box_button_text . '</a>';
            endif;

            if( !empty( $box_full_info_text ) ) :
                $html .= '<a href="' . $box_full_info_url . '" class="custom-box-full-info">' . $box_full_info_text . '</a>';
            endif;

            $html .= '</div>'; // .custom-box-bottom

        endif;

        $html .= "</div>"; // .custom-box-data

        $html .= "</div>"; // .custom-box-box

        return $html;
    } 
     
}

new CustomBox();
