<?php 
/*
Element Description: VC Carousel Experiences
*/

class VCCarouselExperiences extends WPBakeryShortCode {
     
    function __construct() {
        add_action( 'init', array( $this, 'vc_carousel_experiences' ) );
        add_shortcode( 'vc_carousel_experiences', array( $this, 'vc_carousel_experiences_html' ) );
    }

    public function vc_carousel_experiences() {

	    if ( !defined( 'WPB_VC_VERSION' ) ) {
	            return;
	    }

        $cities = array();

        foreach ( get_cities() as $city ) :
            $cities[$city] = $city;
        endforeach;

        $terms = get_terms( array(
            'taxonomy' => 'experience_category',
            'hide_empty' => false,
        ) );

        $categories = array();

        foreach ( $terms as $term ) :
            $categories[$term->name] = $term->term_id;
        endforeach;

        $num_columns = array(
            __( 'Four columns', 'baglioni-hotels' ) => 4,
            __( 'Two columns', 'baglioni-hotels' ) => 2,
        );

	    vc_map(
	        array(
	        	'name' => __('VC Carousel Experiences', 'baglioni-hotels'),
	            'base' => 'vc_carousel_experiences',
	            'description' => __('This element creates a carousel of experiences with dynamic columns', 'baglioni-hotels'),         
	            'params' => array(
                    array(
                        'type' => 'dropdown',
                        'heading' => __( 'Columns', 'baglioni-hotels' ),
                        'param_name' => 'columns',
                        'value' => $num_columns,
                        'description' => __( 'What is the number of the columns?', 'baglioni-hotels' ),
                        'admin_label' => false,
                        'weight' => 0,
                    ),
                    array(
                        'type' => 'textfield',
                        'heading' => __( 'Title', 'baglioni-hotels' ),
                        'param_name' => 'title',
                        'value' => '',
                        'description' => __( 'Title for the Carousel', 'baglioni-hotels' ),
                        'admin_label' => false,
                        'weight' => 0,
                    ),
                    array(
                        'type' => 'dropdown',
                        'heading' => __( 'Order by', 'baglioni-hotels' ),
                        'param_name' => 'order_by',
                        'value' => array(
                            __( 'City', 'baglioni-hotels' ) => 'city',
                            __( 'Category', 'baglioni-hotels' ) => 'category',
                            __( 'All', 'baglioni-hotels' ) => 'all',
                            __( 'Nothing', 'baglioni-hotels' ) => 'nothing',
                        ),
                        'description' => __( 'Order by option', 'baglioni-hotels' ),
                        'admin_label' => false,
                        'weight' => 0,
                    ),
                    array(
                        'type' => 'dropdown',
                        'heading' => __( 'City', 'baglioni-hotels' ),
                        'param_name' => 'city',
                        'value' => $cities,
                        'description' => __( 'What is the city?', 'baglioni-hotels' ),
                        'admin_label' => false,
                        'weight' => 0,
                    ),
                    array(
                        'type' => 'dropdown',
                        'heading' => __( 'Category', 'baglioni-hotels' ),
                        'param_name' => 'category',
                        'value' => $categories,
                        'description' => __( 'What is the category?', 'baglioni-hotels' ),
                        'admin_label' => false,
                        'weight' => 0,
                    ),
                    array(
                        'type' => 'checkbox',
                        'heading' => __( 'Top Experience', 'baglioni-hotels' ),
                        'param_name' => 'top',
                        'value' => 1,
                        'description' => __( 'Select if you want to print only the top experiences.', 'baglioni-hotels' ),
                        'admin_label' => false,
                        'weight' => 0,
                    ),
                    array(
                        'type' => 'checkbox',
                        'heading' => __( 'Corporate Top Experience', 'baglioni-hotels' ),
                        'param_name' => 'corporate_top',
                        'value' => 1,
                        'description' => __( 'Select if you want to print only the corporate top experiences.', 'baglioni-hotels' ),
                        'admin_label' => false,
                        'weight' => 0,
                    ),
                    array(
                        'type' => 'checkbox',
                        'heading' => __( 'Show icons', 'baglioni-hotels' ),
                        'param_name' => 'show_icons',
                        'value' => 1,
                        'description' => __( 'Select if you want to show icons', 'baglioni-hotels' ),
                        'admin_label' => false,
                        'weight' => 0,
                    ),
                    array(
                        'type' => 'textfield',
                        'heading' => __( 'Number of posts', 'baglioni-hotels' ),
                        'param_name' => 'posts_per_page',
                        'value' => '',
                        'description' => __( 'How many posts', 'baglioni-hotels' ),
                        'admin_label' => false,
                        'weight' => 0,
                    ),
                    array(
                        'type' => 'textfield',
                        'heading' => __( 'Discover More Link', 'baglioni-hotels' ),
                        'param_name' => 'discover_more_link',
                        'value' => '',
                        'description' => __( 'Link for the button Discover more', 'baglioni-hotels' ),
                        'admin_label' => false,
                        'weight' => 0,
                    ),
                    array(
                        'type' => 'textfield',
                        'heading' => __( 'Margin bottom', 'baglioni-hotels' ),
                        'param_name' => 'margin_bottom',
                        'value' => '',
                        'admin_label' => false,
                        'weight' => 0,
                    ),
                )
	        )
	    );
    }

    public function vc_carousel_experiences_html( $atts ) {
        extract(
            shortcode_atts(
                array(
                    'columns' => 4,
                    'title' => '',
                    'order_by' => 'city',
                    'city' => 'Florence',
                    'category' => '58',
                    'top' => 0,
                    'corporate_top' => 0,
                    'show_icons' => 0,
                    'posts_per_page' => -1,
                    'discover_more_link' => '',
                    'margin_bottom' => 0
                ), 
                $atts
            )
        );

        if( $order_by == 'city' || $order_by == 'all' ) :

            $args = array( 'post_type' => 'nd_booking_cpt_4', 'posts_per_page' => -1, 'meta_key' => 'nd_booking_meta_box_cpt_4_city', 'meta_value' => $city, 'fields' => 'ids' );

            $branches = get_posts( $args );

            $args = array(
                'post_type' => 'experience',
                'posts_per_page' => $posts_per_page,
                'meta_query' => array(
                    array(
                        'key' => 'experience-branch',
                        'value' => $branches,
                        'compare' => 'IN'
                    )
                )
            );

            if( $top ) :
                $query = array(
                    'key' => 'experience-top',
                    'value' => 1,
                    'compare' => '='
                );

                array_push( $args['meta_query'], $query );
            endif;

            if( $corporate_top ) :
                $query = array(
                    'key' => 'experience-corporate-top',
                    'value' => 1,
                    'compare' => '='
                );

                array_push( $args['meta_query'], $query );
            endif;

            if( $order_by == 'all' ) :
                $args['tax_query'] = array(
                    array(
                        'taxonomy' => 'experience_category',
                        'field' => 'id',
                        'terms' => $category,
                        'include_children' => false
                    )
                );
            endif;

            $experiences = get_posts( $args );

        elseif( $order_by == 'category' ) :

            $args = array(
                'post_type' => 'experience',
                'posts_per_page' => $posts_per_page,
                'meta_query' => array(),
                'tax_query' => array(
                    array(
                        'taxonomy' => 'experience_category',
                        'field' => 'id',
                        'terms' => $category,
                        'include_children' => false
                    )
                )
            );

            if( $top ) :
                $query = array(
                    'key' => 'experience-top',
                    'value' => 1,
                    'compare' => '='
                );

                array_push( $args['meta_query'], $query );
            endif;

            if( $corporate_top ) :
                $query = array(
                    'key' => 'experience-corporate-top',
                    'value' => 1,
                    'compare' => '='
                );

                array_push( $args['meta_query'], $query );
            endif;

            $experiences = get_posts( $args );

        elseif( $order_by == 'nothing' ) :
            
            $args = array(
                'post_type' => 'experience',
                'posts_per_page' => $posts_per_page,
                'meta_query' => array(),
            );

            if( $top ) :
                $query = array(
                    'key' => 'experience-top',
                    'value' => 1,
                    'compare' => '='
                );

                array_push( $args['meta_query'], $query );
            endif;

            if( $corporate_top ) :
                $query = array(
                    'key' => 'experience-corporate-top',
                    'value' => 1,
                    'compare' => '='
                );

                array_push( $args['meta_query'], $query );
            endif;

            $experiences = get_posts( $args );

        endif;

        $html = '';

        if( !empty( $title ) && count( $experiences ) ) :
            $html .= '<h2 class="carousel-experiences-title">' . $title . '</h2>';
        endif;

        $html .= '<div class="carousel-experiences ' . $columns .'-columns">';

        foreach( $experiences as $experience ) :
            $html .= '<div class="experience-box">';

            $html .= '<a href="' . get_permalink( $experience->ID ) . '">';

            $html .= '<div class="experience-image">';
            $html .= '<img src="' . get_the_post_thumbnail_url( $experience->ID, 'landscape' ) .'" />';
            $html .= '<div class="experience-image-title">' . ( ( $order_by == 'city' ) ? get_experience_category( $experience->ID )->name : get_experience_city( $experience->ID ) ) . '</div>';
            $html .= "</div>"; // .experience-image

            $html .= '<div class="experience-data">';
            $html .= '<h3 class="experience-title">' . $experience->post_title . '</h3>';

            if( $show_icons ) :

                $html .= '<div class="experience-details">';

                if( !empty( get_experience_category( $experience->ID )->name ) ) :
                    $html .= '<div class="experience-detail">';
                    $html .= '<img src="' . BAGLIONI_HOTELS_THEME_URL . '/images/' . get_experience_category( $experience->ID )->slug . '-icon.png" alt="Category Icon" width="23" />';
                    $html .= '<p>' . get_experience_category( $experience->ID )->name . '</p>';
                    $html .= '</div>';
                endif;

                if( !empty( get_post_meta( $experience->ID, 'experience-target', true ) ) ) :
                    $html .= '<div class="experience-detail">';
                    $html .= '<img src=" ' . BAGLIONI_HOTELS_THEME_URL . '/images/' . strtolower( get_post_meta( $experience->ID, 'experience-target', true ) ) . '-icon.png" alt="Couple Icon" width="23" />';
                    $html .= '<p>' . get_post_meta( $experience->ID, 'experience-target', true ) . '</p>';
                    $html .= '</div>';
                endif;

                //if( !empty( get_post_meta( $experience->ID, 'experience-duration', true ) ) ) :
                    $html .= '<div class="experience-detail">';
                    $html .= '<img src="' . BAGLIONI_HOTELS_THEME_URL . '/images/clock-icon.png" alt="Clock Icon" width="23" />';
                    $html .= '<p>' . get_post_meta( $experience->ID, 'experience-duration', true ) . '</p>'; 
                    $html .= '</div>';
                //endif;
                
                /*if( !empty( get_post_meta( $experience->ID, 'experience-rating', true ) ) ) :
                    $html .= '<div class="experience-detail">';
                    $html .= '<img width="23" src="' . BAGLIONI_HOTELS_THEME_URL . '/images/limited-edition.png">';
                    $html .= '<p>' . __( 'RATING: ' ) . get_post_meta( $experience->ID, 'experience-rating', true ) . '</p>';
                    $html .= '</div>';
                endif;*/

                $html .= '</div>';
            endif;

            $html .= '<p class="">' . get_the_excerpt_by_post_id( $experience->ID ) . '</p>';
            $html .= '</div>'; // .experience-data

            $html .= '</a>';

            $html .= "</div>"; // .experience-box
        endforeach;
        wp_reset_postdata();

        $html .= "</div>"; // .carousel-experiences

        if( !empty( $discover_more_link ) && count( $experiences ) > 0 ) :
            $html .= '<div style="text-align: right">';
            $html .= '<a href="' . $discover_more_link . '" style="margin-right: 10px; padding:12px 40px; font-size: 12px; line-height: 12px; background-color: #a29c88; color: #ffffff; display: inline-block;">' . __( 'FIND OUT MORE' ) . '</a>';
            $html .= '</div>';
        endif;

        if( $margin_bottom > 0 && count( $experiences ) > 0 ) :
            $html .= '<div style="height: ' . $margin_bottom . 'px"></div>';
        endif;

        return $html;
    } 
     
}

new VCCarouselExperiences();
