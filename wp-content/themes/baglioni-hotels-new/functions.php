<?php

define( 'BAGLIONI_HOTELS_THEME_URL', get_stylesheet_directory_uri() );

define( 'SYNXIS_URL', 'https://gc.synxis.com/rez.aspx' );
define( 'SYNXIS_CHAIN', 12036 );
define( 'SYNXIS_SHELL', 'RBE' );
define( 'SYNXIS_TEMPLATE', 'RBE' );

// Dimensioni thumbnail personalizzate
add_image_size( 'square', 800, 800, true );
add_image_size( 'semi-square', 800, 730, true );
add_image_size( 'landscape', 800, 500, true );
add_image_size( 'landscape-low', 800, 340, true );

// Ridimensionamento delle immagini più piccole
if( !function_exists('mit_thumbnail_upscale') ) {
    function mit_thumbnail_upscale( $default, $orig_w, $orig_h, $new_w, $new_h, $crop ){

        if ( !$crop ) return null;

        $aspect_ratio = $orig_w / $orig_h;
        $size_ratio = max($new_w / $orig_w, $new_h / $orig_h);

        $crop_w = round($new_w / $size_ratio);
        $crop_h = round($new_h / $size_ratio);

        $s_x = floor( ($orig_w - $crop_w) / 2 );
        $s_y = floor( ($orig_h - $crop_h) / 2 );

        return array( 0, 0, (int) $s_x, (int) $s_y, (int) $new_w, (int) $new_h, (int) $crop_w, (int) $crop_h );
    }
}
add_filter( 'image_resize_dimensions', 'mit_thumbnail_upscale', 10, 6 );


// Including theme CSS
add_action( 'wp_enqueue_scripts', 'enqueue_parent_theme_style' );
function enqueue_parent_theme_style() {
	wp_enqueue_style( 'nice-select', get_stylesheet_directory_uri() . '/css/nice-select.css' );

	wp_enqueue_style( 'slick', get_stylesheet_directory_uri() . '/css/slick/slick.css' );

	wp_enqueue_style( 'slick-theme', get_stylesheet_directory_uri() . '/css/slick/slick-theme.css' );

    wp_enqueue_style( 'simple-line-icons', get_stylesheet_directory_uri() . '/css/simple-line-icons/css/simple-line-icons.css' );

    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
}


// Including theme scripts
function theme_enqueue_scripts() {
	wp_enqueue_script( 'nice-select', get_stylesheet_directory_uri() . '/js/jquery.nice-select.min.js', array('jquery'), '1.1.0' );

	wp_enqueue_script( 'slick', get_stylesheet_directory_uri() . '/js/slick.min.js', array('jquery'), '1.8.1' );

    wp_enqueue_script( 'fittext', get_stylesheet_directory_uri() . '/js/jquery.fittext.js', array('jquery'), '1.2' );

    wp_enqueue_script( 'match-height', get_stylesheet_directory_uri() . '/js/jquery.matchHeight-min.js', array('jquery'), '0.7.2' );

    wp_register_script( 'baglioni-hotels', get_stylesheet_directory_uri() . '/js/custom.js', array('jquery'), '1.0', true );
    $translation_array = array(
        'ajaxUrl' => admin_url('admin-ajax.php'),
        'SYNXIS_URL' => SYNXIS_URL,
        'SYNXIS_CHAIN' => SYNXIS_CHAIN,
        'SYNXIS_SHELL' => SYNXIS_SHELL,
        'SYNXIS_TEMPLATE' => SYNXIS_TEMPLATE
    );
    wp_localize_script( 'baglioni-hotels', 'baglioniHotelsLocalize', $translation_array );
    wp_enqueue_script( 'baglioni-hotels' );
}
add_action( 'wp_enqueue_scripts', 'theme_enqueue_scripts' );


// Custom menus
function theme_menus_init() {
    register_nav_menus( array(
        'mobile-menu'    => __( 'Mobile Menù', 'baglioni-hotels' ),
        'main-menu-carlton'    => __( 'Main Menù Baglioni Hotel Carlton', 'baglioni-hotels' ),
        'mobile-menu-carlton'    => __( 'Mobile Menù Baglioni Hotel Carlton', 'baglioni-hotels' ),
        'main-menu-luna' => __( 'Main Menù Baglioni Hotel Luna', 'baglioni-hotels' ),
        'mobile-menu-luna' => __( 'Mobile Menù Baglioni Hotel Luna', 'baglioni-hotels' ),
        'main-menu-regina' => __( 'Main Menù Baglioni Hotel Regina', 'baglioni-hotels' ),
        'mobile-menu-regina' => __( 'Mobile Menù Baglioni Hotel Regina', 'baglioni-hotels' ),
        'main-menu-santa-croce' => __( 'Main Menù Relais Santa Croce', 'baglioni-hotels' ),
        'mobile-menu-santa-croce' => __( 'Mobile Menù Relais Santa Croce', 'baglioni-hotels' ),
        'main-menu-cala-del-porto' => __( 'Main Menù Baglioni Resort Cala del Porto', 'baglioni-hotels' ),
        'mobile-menu-cala-del-porto' => __( 'Mobile Menù Baglioni Resort Cala del Porto', 'baglioni-hotels' ),
        'main-menu-alleluja' => __( 'Main Menù Baglioni Resort Alleluja', 'baglioni-hotels' ),
        'mobile-menu-alleluja' => __( 'Mobile Menù Baglioni Resort Alleluja', 'baglioni-hotels' ),
        'main-menu-la-vela' => __( 'Main Menù Baglioni La Vela', 'baglioni-hotels' ),
        'mobile-menu-la-vela' => __( 'Mobile Menù La Vela', 'baglioni-hotels' ),
        'main-menu-london' => __( 'Main Menù Baglioni Hotel London', 'baglioni-hotels' ),
        'mobile-menu-london' => __( 'Mobile Menù Baglioni Hotel London', 'baglioni-hotels' ),
        'main-menu-maldives' => __( 'Main Menù Baglioni Resort Maldives', 'baglioni-hotels' ),
        'mobile-menu-maldives' => __( 'Mobile Menù Baglioni Resort Maldives', 'baglioni-hotels' ),
        'main-menu-le-saint-paul' => __( 'Main Menù Hotel Le Saint Paul', 'baglioni-hotels' ),
        'mobile-menu-le-saint-paul' => __( 'Mobile Menù Hotel Le Saint Paul', 'baglioni-hotels' ),
        'main-menu-villa-gallici' => __( 'Main Menù Villa Gallici', 'baglioni-hotels' ),
        'mobile-menu-villa-gallici' => __( 'Mobile Menù Villa Gallici', 'baglioni-hotels' ),
    ) );
}
add_action( 'init', 'theme_menus_init' );


// Custom Post Types
function theme_post_type_init() {
    register_post_type( 'experience',
        array(
            'labels' => array(
                'name' => __( 'Experiences', 'baglioni-hotels' ),
                'singular_name' => __( 'Experience', 'baglioni-hotels' )
            ),
            'public' => true,
            'supports' => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
            'menu_icon' => 'dashicons-star-half',
            'rewrite' => array('slug' => 'experiences'),
        )
    );

    register_post_type( 'gallery',
        array(
            'labels' => array(
                'name' => __( 'Gallery', 'baglioni-hotels' ),
                'singular_name' => __( 'Gallery', 'baglioni-hotels' )
            ),
            'public' => true,
            'supports' => array( 'title', 'thumbnail', 'excerpt' ),
            'menu_icon' => 'dashicons-format-gallery',
            'rewrite' => array('slug' => 'gallery'),
        )
    );

    register_post_type( 'offer',
        array(
            'labels' => array(
                'name' => __( 'Special Offers', 'baglioni-hotels' ),
                'singular_name' => __( 'Special Offer', 'baglioni-hotels' )
            ),
            'public' => true,
            'supports' => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
            'menu_icon' => 'dashicons-awards',
            'rewrite' => array('slug' => 'offers'),
        )
    );

    register_post_type( 'restaurant',
        array(
            'labels' => array(
                'name' => __( 'Restaurants', 'baglioni-hotels' ),
                'singular_name' => __( 'Restaurant', 'baglioni-hotels' )
            ),
            'public' => true,
            'supports' => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
            'menu_icon' => 'dashicons-store',
            'rewrite' => array('slug' => 'restaurants'),
        )
    );

    register_post_type( 'room',
        array(
            'labels' => array(
                'name' => __( 'Rooms', 'baglioni-hotels' ),
                'singular_name' => __( 'Rooms', 'baglioni-hotels' )
            ),
            'public' => true,
            'supports' => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
            'menu_icon' => 'dashicons-admin-multisite',
            'rewrite' => array('slug' => 'rooms'),
        )
    );
}
add_action( 'init', 'theme_post_type_init' );


// Custom Taxonomies
function custom_taxonomies_init() {
    register_taxonomy(
        'gallery_category',
        'gallery',
        array(
            'hierarchical' => true, 
            'label' => __( 'Gallery Categories' ),
            'rewrite' => array( 'slug' => 'gallery-category' ),
        )
    );

    register_taxonomy(
        'experience_category',
        'experience',
        array(
            'hierarchical' => true, 
            'label' => __( 'Experience Categories' ),
            'rewrite' => array( 'slug' => 'experience-category' ),
        )
    );
}
add_action( 'init', 'custom_taxonomies_init' );


// Remove Parent Custom Post Type (Rooms)
function remove_parent_theme_custom_post_types() {
    unregister_post_type( 'nd_booking_cpt_1' );
}
add_action( 'init', 'remove_parent_theme_custom_post_types' );


// Custom Meta Boxes for Page Header
function adding_custom_meta_boxes( $post ) {
    $post_types = array( 'page', 'post', 'nd_booking_cpt_4' );

    // Header Customizing
    foreach ( $post_types as $post_type ) :
        add_meta_box(
            'post_meta_box',
            __( 'Header Customizing', 'baglioni-hotels' ),
            'render_custom_meta_box',
            $post_type
        );
    endforeach;

    // Experience Data
    add_meta_box(
        'post_meta_box',
        __( 'Experience Data', 'baglioni-hotels' ),
        'render_custom_meta_box_experiences',
        'experience'
    );

    // Experience Data
    add_meta_box(
        'post_meta_box',
        __( 'Special Offer Data', 'baglioni-hotels' ),
        'render_custom_meta_box_offers',
        'offer'
    );

    // Restaurant Data
    add_meta_box(
        'post_meta_box',
        __( 'Restaurant Data', 'baglioni-hotels' ),
        'render_custom_meta_box_restaurants',
        'restaurant'
    );

    // Room Data
    add_meta_box(
        'post_meta_box',
        __( 'Room Data', 'baglioni-hotels' ),
        'render_custom_meta_box_rooms',
        'room'
    );
}
add_action( 'add_meta_boxes', 'adding_custom_meta_boxes', 10, 2 );

function render_custom_meta_box( $post ) {

    wp_nonce_field( basename( __FILE__ ), 'custom_meta_box_nonce' );

    $logo_custom = get_post_meta( $post->ID, 'logo-custom', true );
    $logo_custom_width = get_post_meta( $post->ID, 'logo-custom-width', true );
    $menu_custom = get_post_meta( $post->ID, 'menu-custom', true );
    $mobile_menu_custom = get_post_meta( $post->ID, 'mobile-menu-custom', true );

    ?>
    <div>
        <h4 style="margin-bottom: 5px;"><?php echo __( 'Logo URL', 'baglioni-hotels' ) ?>:</h4>
        <input type="text" name="logo_custom" value="<?php echo $logo_custom; ?>" style="width: 300px" />
    </div>
    <div>
        <h4 style="margin-bottom: 5px;"><?php echo __( 'Logo Width', 'baglioni-hotels' ) ?>:</h4>
        <input type="text" name="logo_custom_width" value="<?php echo $logo_custom_width; ?>" style="width: 300px" />
    </div>
    <div>
        <h4 style="margin-bottom: 5px;"><?php echo __( 'Menù Code ID', 'baglioni-hotels' ) ?>:</h4>
        <input type="text" name="menu_custom" value="<?php echo $menu_custom; ?>" style="width: 300px" />
    </div>
    <div>
        <h4 style="margin-bottom: 5px;"><?php echo __( 'Mobile Menù ID', 'baglioni-hotels' ) ?>:</h4>
        <input type="text" name="mobile_menu_custom" value="<?php echo $mobile_menu_custom; ?>" style="width: 300px" />
    </div>
    <?php
}

function save_custom_meta_box( $post_id ) {

    if ( !isset( $_POST['custom_meta_box_nonce'] ) || !wp_verify_nonce( $_POST['custom_meta_box_nonce'], basename( __FILE__ ) ) ){
        return;
    }

    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
        return;
    }

    if ( !current_user_can( 'edit_post', $post_id ) ) {
        return;
    }

    $post = get_post( $post_id );

    $post_types = array( 'page', 'post', 'nd_booking_cpt_4', 'experience' ); 

    if ( !in_array( $post->post_type, $post_types ) ) {
        return;
    }

    update_post_meta( $post_id, 'logo-custom', sanitize_text_field( $_POST['logo_custom'] ) );
    update_post_meta( $post_id, 'logo-custom-width', sanitize_text_field( $_POST['logo_custom_width'] ) );
    update_post_meta( $post_id, 'menu-custom', sanitize_text_field( $_POST['menu_custom'] ) );
    update_post_meta( $post_id, 'mobile-menu-custom', sanitize_text_field( $_POST['mobile_menu_custom'] ) );
}
add_action( 'save_post', 'save_custom_meta_box', 10, 2 );


// Custom Meta Boxes for Offers
function render_custom_meta_box_offers( $post ) {

    wp_nonce_field( basename( __FILE__ ), 'custom_meta_box_nonce' );

    $offer_branch = get_post_meta( $post->ID, 'offer-branch', true );
    $offer_book_now = get_post_meta( $post->ID, 'offer-book-now', true );

    ?>
    <div>
        <h4 style="margin-bottom: 5px;"><?php echo __( 'Branch', 'baglioni-hotels' ) ?>:</h4>
        <select name="offer_branch" style="width: 300px">
            <option value=""><?php echo __( 'Nothing', 'baglioni-hotels' ); ?></option>
            <?php $args = array( 'post_type' => 'nd_booking_cpt_4', 'posts_per_page' => -1 );
            $branches = get_posts( $args );
            foreach ($branches as $branch) : ?>
            <option value="<?php echo $branch->ID; ?>"<?php echo ( $offer_branch == $branch->ID ) ? ' selected' : ''; ?>><?php echo $branch->post_title; ?></option>
            <?php endforeach; ?>
        </select>
    </div>
    <div>
        <h4 style="margin-bottom: 5px;"><?php echo __( 'Book Now (Link)', 'baglioni-hotels' ) ?>:</h4>
        <input type="text" name="offer_book_now" value="<?php echo $offer_book_now; ?>" style="width: 300px" />
    </div>
    <?php
}

function save_custom_meta_box_offers( $post_id ) {

    if ( !isset( $_POST['custom_meta_box_nonce'] ) || !wp_verify_nonce( $_POST['custom_meta_box_nonce'], basename( __FILE__ ) ) ){
        return;
    }

    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
        return;
    }

    if ( !current_user_can( 'edit_post', $post_id ) ) {
        return;
    }

    $post = get_post( $post_id );

    if ( $post->post_type != 'offer' ) {
        return;
    }

    update_post_meta( $post_id, 'offer-branch', sanitize_text_field( $_POST['offer_branch'] ) );
    update_post_meta( $post_id, 'offer-book-now', sanitize_text_field( $_POST['offer_book_now'] ) );
}
add_action( 'save_post', 'save_custom_meta_box_offers', 10, 2 );


// Custom Meta Boxes for Restaurants
function render_custom_meta_box_restaurants( $post ) {

    wp_nonce_field( basename( __FILE__ ), 'custom_meta_box_nonce' );

    $restaurant_branch = get_post_meta( $post->ID, 'restaurant-branch', true );
    $restaurant_highlighted = get_post_meta( $post->ID, 'restaurant-highlighted', true );

    ?>
    <div>
        <h4 style="margin-bottom: 5px;"><?php echo __( 'Branch', 'baglioni-hotels' ) ?>:</h4>
        <select name="restaurant_branch" style="width: 300px">
            <option value=""><?php echo __( 'Nothing', 'baglioni-hotels' ); ?></option>
            <?php $args = array( 'post_type' => 'nd_booking_cpt_4', 'posts_per_page' => -1 );
            $branches = get_posts( $args );
            foreach ($branches as $branch) : ?>
            <option value="<?php echo $branch->ID; ?>"<?php echo ( $restaurant_branch == $branch->ID ) ? ' selected' : ''; ?>><?php echo $branch->post_title; ?></option>
            <?php endforeach; ?>
        </select>
    </div>
    <div>
        <h4 style="margin-bottom: 5px;"><?php echo __( 'Is it a highlighted restaurant?', 'baglioni-hotels' ) ?>:</h4>
        <input type="checkbox" name="restaurant_highlighted" value="1" <?php echo ( $restaurant_highlighted == 1 ) ? ' checked' : ''; ?> />
    </div>
    <?php
}

function save_custom_meta_box_restaurants( $post_id ) {

    if ( !isset( $_POST['custom_meta_box_nonce'] ) || !wp_verify_nonce( $_POST['custom_meta_box_nonce'], basename( __FILE__ ) ) ){
        return;
    }

    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
        return;
    }

    if ( !current_user_can( 'edit_post', $post_id ) ) {
        return;
    }

    $post = get_post( $post_id );

    if ( $post->post_type != 'restaurant' ) {
        return;
    }

    update_post_meta( $post_id, 'restaurant-branch', sanitize_text_field( $_POST['restaurant_branch'] ) );
    update_post_meta( $post_id, 'restaurant-highlighted', sanitize_text_field( $_POST['restaurant_highlighted'] ) );
}
add_action( 'save_post', 'save_custom_meta_box_restaurants', 10, 2 );


// Custom Meta Boxes for Rooms
function render_custom_meta_box_rooms( $post ) {

    wp_nonce_field( basename( __FILE__ ), 'custom_meta_box_nonce' );

    $room_branch = get_post_meta( $post->ID, 'room-branch', true );
    $room_suite = get_post_meta( $post->ID, 'room-suite', true );
    $room_highlighted = get_post_meta( $post->ID, 'room-highlighted', true );
    $room_price = get_post_meta( $post->ID, 'room-price', true );
    $room_small_description = get_post_meta( $post->ID, 'room-small-description', true );
    $room_bg_position = get_post_meta( $post->ID, 'room-bg-position', true );
    $room_slider = get_post_meta( $post->ID, 'room-slider', true );
    $room_guests = get_post_meta( $post->ID, 'room-guests', true );
    $room_best_view = get_post_meta( $post->ID, 'room-best-view', true );
    $room_sqm = get_post_meta( $post->ID, 'room-sqm', true );
    $room_style = get_post_meta( $post->ID, 'room-style', true );
    $room_pet_friendly = get_post_meta( $post->ID, 'room-pet-friendly', true );
    $room_features = get_post_meta( $post->ID, 'room-features', true );
    $room_benefits = get_post_meta( $post->ID, 'room-benefits', true );
    $room_book_now = get_post_meta( $post->ID, 'room-book-now', true );
    $room_first_box_title = get_post_meta( get_the_ID(), 'room-first-box-title', true );
    $room_first_box_image = get_post_meta( get_the_ID(), 'room-first-box-image', true );
    $room_first_box_description = get_post_meta( get_the_ID(), 'room-first-box-description', true );
    $room_first_box_link = get_post_meta( get_the_ID(), 'room-first-box-link', true );
    $room_second_box_title = get_post_meta( get_the_ID(), 'room-second-box-title', true );
    $room_second_box_image = get_post_meta( get_the_ID(), 'room-second-box-image', true );
    $room_second_box_description = get_post_meta( get_the_ID(), 'room-second-box-description', true );
    $room_second_box_link = get_post_meta( get_the_ID(), 'room-second-box-link', true );
    $room_third_box_title = get_post_meta( get_the_ID(), 'room-third-box-title', true );
    $room_third_box_image = get_post_meta( get_the_ID(), 'room-third-box-image', true );
    $room_third_box_description = get_post_meta( get_the_ID(), 'room-third-box-description', true );
    $room_third_box_link = get_post_meta( get_the_ID(), 'room-third-box-link', true );
    $room_similar_suites = get_post_meta( $post->ID, 'room-similar-suites', true );

    ?>
    <div>
        <h4 style="margin-bottom: 5px;"><?php echo __( 'Branch', 'baglioni-hotels' ) ?>:</h4>
        <select name="room_branch" style="width: 300px">
            <option value=""><?php echo __( 'Nothing', 'baglioni-hotels' ); ?></option>
            <?php $args = array( 'post_type' => 'nd_booking_cpt_4', 'posts_per_page' => -1 );
            $branches = get_posts( $args );
            foreach ($branches as $branch) : ?>
            <option value="<?php echo $branch->ID; ?>"<?php echo ( $room_branch == $branch->ID ) ? ' selected' : ''; ?>><?php echo $branch->post_title; ?></option>
            <?php endforeach; ?>
        </select>
    </div>
    <div>
        <h4 style="margin-bottom: 5px;"><?php echo __( 'Is a suite?', 'baglioni-hotels' ) ?>:</h4>
        <input type="checkbox" name="room_suite" value="1" <?php echo ( $room_suite == 1 ) ? ' checked' : ''; ?> />
    </div>
    <div>
        <h4 style="margin-bottom: 5px;"><?php echo __( 'Is a highlighted room?', 'baglioni-hotels' ) ?>:</h4>
        <input type="checkbox" name="room_highlighted" value="1" <?php echo ( $room_highlighted == 1 ) ? ' checked' : ''; ?> />
    </div>
    <div>
        <h4 style="margin-bottom: 5px;"><?php echo __( 'Room Price', 'baglioni-hotels' ) ?>:</h4>
        <input type="text" name="room_price" value="<?php echo $room_price; ?>" style="width: 300px" />
    </div>
    <div>
        <h4 style="margin-bottom: 5px;"><?php echo __( 'Room Small Description', 'baglioni-hotels' ) ?>:</h4>
        <input type="text" name="room_small_description" value="<?php echo $room_small_description; ?>" style="width: 300px" />
    </div>
    <div>
        <h4 style="margin-bottom: 5px;"><?php echo __( 'Room Background Position (Header)', 'baglioni-hotels' ) ?>:</h4>
        <select name="room_bg_position" style="width: 300px">
            <option value=""><?php echo __( 'Nothing', 'baglioni-hotels' ); ?></option>
            <?php $bg_positions = array( 'left top', 'left center', 'left bottom', 'right top', 'right center', 'right bottom', 'center top', 'center center', 'center bottom' );
            foreach ($bg_positions as $bg_position) : ?>
            <option value="<?php echo $bg_position ?>"<?php echo ( $room_bg_position == $bg_position ) ? ' selected' : ''; ?>><?php echo $bg_position; ?></option>
            <?php endforeach;
            wp_reset_postdata(); ?>
        </select>
    </div>
    <div>
        <h4 style="margin-bottom: 5px;"><?php echo __( 'Room Revolution Slider ID', 'baglioni-hotels' ) ?>:</h4>
        <select name="room_slider" style="width: 300px">
            <option value=""><?php echo __( 'Nothing', 'baglioni-hotels' ); ?></option>
            <?php $rev_slider = new RevSlider();

            if( class_exists( 'RevSlider' ) ) :
                $sliders = $rev_slider->getAllSliderAliases();
            else :
                $sliders = array();
            endif;

            foreach ($sliders as $slider) : ?>
            <option value="<?php echo $slider ?>"<?php echo ( $room_slider == $slider ) ? ' selected' : ''; ?>><?php echo $slider; ?></option>
            <?php endforeach; ?>
        </select>
    </div>
    <div>
        <h4 style="margin-bottom: 5px;"><?php echo __( 'Room Guests', 'baglioni-hotels' ) ?>:</h4>
        <input type="text" name="room_guests" value="<?php echo $room_guests; ?>" style="width: 300px" />
    </div>
    <div>
        <h4 style="margin-bottom: 5px;"><?php echo __( 'Room Best View', 'baglioni-hotels' ) ?>:</h4>
        <input type="text" name="room_best_view" value="<?php echo $room_best_view; ?>" style="width: 300px" />
    </div>
    <div>
        <h4 style="margin-bottom: 5px;"><?php echo __( 'Room SQM', 'baglioni-hotels' ) ?>:</h4>
        <input type="text" name="room_sqm" value="<?php echo $room_sqm; ?>" style="width: 300px" />
    </div>
    <div>
        <h4 style="margin-bottom: 5px;"><?php echo __( 'Room Style', 'baglioni-hotels' ) ?>:</h4>
        <input type="text" name="room_style" value="<?php echo $room_style; ?>" style="width: 300px" />
    </div>
    <div>
        <h4 style="margin-bottom: 5px;"><?php echo __( 'Room Pet Friendly', 'baglioni-hotels' ) ?>:</h4>
        <input type="checkbox" name="room_pet_friendly" value="1" <?php echo ( $room_pet_friendly == 1 ) ? ' checked' : ''; ?> />
    </div>
    <div>
        <h4 style="margin-bottom: 5px;"><?php echo __( 'Room Features divided by "|" character', 'baglioni-hotels' ) ?>:</h4>
        <textarea name="room_features" style="width: 100%;"><?php echo $room_features; ?></textarea>
    </div>
    <div>
        <h4 style="margin-bottom: 5px;"><?php echo __( 'Room Benefits divided by "|" character', 'baglioni-hotels' ) ?>:</h4>
        <textarea name="room_benefits" style="width: 100%;"><?php echo $room_benefits; ?></textarea>
    </div>
    <div>
        <h4 style="margin-bottom: 5px;"><?php echo __( 'Room Book Now (Link)', 'baglioni-hotels' ) ?>:</h4>
        <input type="text" name="room_book_now" value="<?php echo $room_book_now; ?>" style="width: 300px" />
    </div>
    <div>
        <h4 style="margin-bottom: 5px;"><?php echo __( 'Room First Box Image (Link)', 'baglioni-hotels' ) ?>:</h4>
        <input type="text" name="room_first_box_image" value="<?php echo $room_first_box_image; ?>" style="width: 300px" />
    </div>
    <div>
        <h4 style="margin-bottom: 5px;"><?php echo __( 'Room First Box Title', 'baglioni-hotels' ) ?>:</h4>
        <input type="text" name="room_first_box_title" value="<?php echo $room_first_box_title; ?>" style="width: 300px" />
    </div>
    <div>
        <h4 style="margin-bottom: 5px;"><?php echo __( 'Room First Box Description', 'baglioni-hotels' ) ?>:</h4>
        <input type="text" name="room_first_box_description" value="<?php echo $room_first_box_description; ?>" style="width: 300px" />
    </div>
    <div>
        <h4 style="margin-bottom: 5px;"><?php echo __( 'Room First Box Link', 'baglioni-hotels' ) ?>:</h4>
        <input type="text" name="room_first_box_link" value="<?php echo $room_first_box_link; ?>" style="width: 300px" />
    </div>
    <div>
        <h4 style="margin-bottom: 5px;"><?php echo __( 'Room Second Box Image (Link)', 'baglioni-hotels' ) ?>:</h4>
        <input type="text" name="room_second_box_image" value="<?php echo $room_second_box_image; ?>" style="width: 300px" />
    </div>
    <div>
        <h4 style="margin-bottom: 5px;"><?php echo __( 'Room Second Box Title', 'baglioni-hotels' ) ?>:</h4>
        <input type="text" name="room_second_box_title" value="<?php echo $room_second_box_title; ?>" style="width: 300px" />
    </div>
    <div>
        <h4 style="margin-bottom: 5px;"><?php echo __( 'Room Second Box Description', 'baglioni-hotels' ) ?>:</h4>
        <input type="text" name="room_second_box_description" value="<?php echo $room_second_box_description; ?>" style="width: 300px" />
    </div>
    <div>
        <h4 style="margin-bottom: 5px;"><?php echo __( 'Room Second Box Link', 'baglioni-hotels' ) ?>:</h4>
        <input type="text" name="room_second_box_link" value="<?php echo $room_second_box_link; ?>" style="width: 300px" />
    </div>
    <div>
        <h4 style="margin-bottom: 5px;"><?php echo __( 'Room Third Box Image (Link)', 'baglioni-hotels' ) ?>:</h4>
        <input type="text" name="room_third_box_image" value="<?php echo $room_third_box_image; ?>" style="width: 300px" />
    </div>
    <div>
        <h4 style="margin-bottom: 5px;"><?php echo __( 'Room Third Box Title', 'baglioni-hotels' ) ?>:</h4>
        <input type="text" name="room_third_box_title" value="<?php echo $room_third_box_title; ?>" style="width: 300px" />
    </div>
    <div>
        <h4 style="margin-bottom: 5px;"><?php echo __( 'Room Third Box Description', 'baglioni-hotels' ) ?>:</h4>
        <input type="text" name="room_third_box_description" value="<?php echo $room_third_box_description; ?>" style="width: 300px" />
    </div>
    <div>
        <h4 style="margin-bottom: 5px;"><?php echo __( 'Room Third Box Link', 'baglioni-hotels' ) ?>:</h4>
        <input type="text" name="room_third_box_link" value="<?php echo $room_third_box_link; ?>" style="width: 300px" />
    </div>
    <div>
        <h4 style="margin-bottom: 5px;"><?php echo __( 'Room Similar Suites', 'baglioni-hotels' ) ?>:</h4>
        <input type="text" name="room_similar_suites" value="<?php echo $room_similar_suites; ?>" style="width: 300px" />
    </div>
    <?php
}

function save_custom_meta_box_rooms( $post_id ) {

    if ( !isset( $_POST['custom_meta_box_nonce'] ) || !wp_verify_nonce( $_POST['custom_meta_box_nonce'], basename( __FILE__ ) ) ){
        return;
    }

    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
        return;
    }

    if ( !current_user_can( 'edit_post', $post_id ) ) {
        return;
    }

    $post = get_post( $post_id );

    if ( $post->post_type != 'room' ) {
        return;
    }

    update_post_meta( $post_id, 'room-branch', sanitize_text_field( $_POST['room_branch'] ) );

    if( $_POST['room_suite'] == 1 ) :
        update_post_meta( $post_id, 'room-suite', 1 );
    else :
        update_post_meta( $post_id, 'room-suite', 0 );
    endif;

    if( $_POST['room_highlighted'] == 1 ) :
        update_post_meta( $post_id, 'room-highlighted', 1 );
    else :
        update_post_meta( $post_id, 'room-highlighted', 0 );
    endif;

    update_post_meta( $post_id, 'room-price', sanitize_text_field( $_POST['room_price'] ) );
    update_post_meta( $post_id, 'room-small-description', sanitize_text_field( $_POST['room_small_description'] ) );
    update_post_meta( $post_id, 'room-bg-position', sanitize_text_field( $_POST['room_bg_position'] ) );
    update_post_meta( $post_id, 'room-slider', sanitize_text_field( $_POST['room_slider'] ) );
    update_post_meta( $post_id, 'room-guests', sanitize_text_field( $_POST['room_guests'] ) );
    update_post_meta( $post_id, 'room-best-view', sanitize_text_field( $_POST['room_best_view'] ) );
    update_post_meta( $post_id, 'room-sqm', sanitize_text_field( $_POST['room_sqm'] ) );
    update_post_meta( $post_id, 'room-style', sanitize_text_field( $_POST['room_style'] ) );
    
    if( $_POST['room_pet_friendly'] == 1 ) :
        update_post_meta( $post_id, 'room-pet-friendly', 1 );
    else :
        update_post_meta( $post_id, 'room-pet-friendly', 0 );
    endif;

    update_post_meta( $post_id, 'room-features', sanitize_text_field( $_POST['room_features'] ) );
    update_post_meta( $post_id, 'room-benefits', sanitize_text_field( $_POST['room_benefits'] ) );
    update_post_meta( $post_id, 'room-book-now', sanitize_text_field( $_POST['room_book_now'] ) );
    update_post_meta( $post_id, 'room-first-box-image', sanitize_text_field( $_POST['room_first_box_image'] ) );
    update_post_meta( $post_id, 'room-first-box-title', sanitize_text_field( $_POST['room_first_box_title'] ) );
    update_post_meta( $post_id, 'room-first-box-description', sanitize_text_field( $_POST['room_first_box_description'] ) );
    update_post_meta( $post_id, 'room-first-box-link', sanitize_text_field( $_POST['room_first_box_link'] ) );
    update_post_meta( $post_id, 'room-second-box-image', sanitize_text_field( $_POST['room_second_box_image'] ) );
    update_post_meta( $post_id, 'room-second-box-title', sanitize_text_field( $_POST['room_second_box_title'] ) );
    update_post_meta( $post_id, 'room-second-box-description', sanitize_text_field( $_POST['room_second_box_description'] ) );
    update_post_meta( $post_id, 'room-second-box-link', sanitize_text_field( $_POST['room_second_box_link'] ) );
    update_post_meta( $post_id, 'room-third-box-image', sanitize_text_field( $_POST['room_third_box_image'] ) );
    update_post_meta( $post_id, 'room-third-box-title', sanitize_text_field( $_POST['room_third_box_title'] ) );
    update_post_meta( $post_id, 'room-third-box-description', sanitize_text_field( $_POST['room_third_box_description'] ) );
    update_post_meta( $post_id, 'room-third-box-link', sanitize_text_field( $_POST['room_third_box_link'] ) );
    update_post_meta( $post_id, 'room-similar-suites', sanitize_text_field( $_POST['room_similar_suites'] ) );
}
add_action( 'save_post', 'save_custom_meta_box_rooms', 10, 2 );


// Custom Meta Boxes for Experiences
function render_custom_meta_box_experiences( $post ) {

    wp_nonce_field( basename( __FILE__ ), 'custom_meta_box_nonce' );

    $experience_branch = get_post_meta( $post->ID, 'experience-branch', true );
    $experience_top = get_post_meta( $post->ID, 'experience-top', true );
    $experience_corporate_top = get_post_meta( $post->ID, 'experience-corporate-top', true );
    $experience_highlighted = get_post_meta( $post->ID, 'experience-highlighted', true );
    $experience_subtitle = get_post_meta( $post->ID, 'experience-subtitle', true );
    $experience_small_description = get_post_meta( $post->ID, 'experience-small-description', true );
    $experience_category = get_post_meta( $post->ID, 'experience-category', true );
    $experience_duration = get_post_meta( $post->ID, 'experience-duration', true );
    $experience_target = get_post_meta( $post->ID, 'experience-target', true );
    $experience_rating = get_post_meta( $post->ID, 'experience-rating', true );
    $experience_inclusions = get_post_meta( $post->ID, 'experience-inclusions', true );
    $experience_conditions = get_post_meta( $post->ID, 'experience-conditions', true );
    $experience_book_now = get_post_meta( $post->ID, 'experience-book-now', true );

    ?>
    <div>
        <h4 style="margin-bottom: 5px;"><?php echo __( 'Branch', 'baglioni-hotels' ) ?>:</h4>
        <select name="experience_branch" style="width: 300px">
            <option value=""><?php echo __( 'Nothing', 'baglioni-hotels' ); ?></option>
            <?php $args = array( 'post_type' => 'nd_booking_cpt_4', 'posts_per_page' => -1 );
            $branches = get_posts( $args );
            foreach ($branches as $branch) : ?>
            <option value="<?php echo $branch->ID; ?>"<?php echo ( $experience_branch == $branch->ID ) ? ' selected' : ''; ?>><?php echo $branch->post_title; ?></option>
            <?php endforeach;
            wp_reset_postdata(); ?>
        </select>
    </div>
    <div>
        <h4 style="margin-bottom: 5px;"><?php echo __( 'Is it a top experience?', 'baglioni-hotels' ) ?>:</h4>
        <input type="checkbox" name="experience_top" value="1" <?php echo ( $experience_top == 1 ) ? ' checked' : ''; ?> />
    </div>
    <div>
        <h4 style="margin-bottom: 5px;"><?php echo __( 'Is it a corporate top experience?', 'baglioni-hotels' ) ?>:</h4>
        <input type="checkbox" name="experience_corporate_top" value="1" <?php echo ( $experience_corporate_top == 1 ) ? ' checked' : ''; ?> />
    </div>
    <div>
        <h4 style="margin-bottom: 5px;"><?php echo __( 'Is it a highlighted experience?', 'baglioni-hotels' ) ?></h4>
        <input type="checkbox" name="experience_highlighted" value="1" <?php echo ( $experience_highlighted == 1 ) ? ' checked' : ''; ?> />
    </div>
    <div>
        <h4 style="margin-bottom: 5px;"><?php echo __( 'Experience subtitle', 'baglioni-hotels' ) ?></h4>
        <input type="text" name="experience_subtitle" value="<?php echo $experience_subtitle; ?>" style="width: 300px" />
    </div>
    <div>
        <h4 style="margin-bottom: 5px;"><?php echo __( 'Experience small description', 'baglioni-hotels' ) ?>:</h4>
        <input type="text" name="experience_small_description" value="<?php echo $experience_small_description; ?>" style="width: 300px" />
    </div>
    <div>
        <h4 style="margin-bottom: 5px;"><?php echo __( 'Experience Duration', 'baglioni-hotels' ) ?>:</h4>
        <input type="text" name="experience_duration" value="<?php echo $experience_duration; ?>" style="width: 300px" />
    </div>
    <div>
        <h4 style="margin-bottom: 5px;"><?php echo __( 'Experience Target', 'baglioni-hotels' ) ?>:</h4>
        <input type="text" name="experience_target" value="<?php echo $experience_target; ?>" style="width: 300px" />
    </div>
    <div>
        <h4 style="margin-bottom: 5px;"><?php echo __( 'Experience Rating', 'baglioni-hotels' ) ?>:</h4>
        <input type="text" name="experience_rating" value="<?php echo $experience_rating; ?>" style="width: 300px" />
    </div>
    <div>
        <h4 style="margin-bottom: 5px;"><?php echo __( 'Experience Inclusions divided by "|" character', 'baglioni-hotels' ) ?>:</h4>
        <textarea name="experience_inclusions" style="width: 100%;"><?php echo $experience_inclusions; ?></textarea>
    </div>
    <div>
        <h4 style="margin-bottom: 5px;"><?php echo __( 'Experience Conditions divided by "|" character', 'baglioni-hotels' ) ?>:</h4>
        <textarea name="experience_conditions" style="width: 100%;"><?php echo $experience_conditions; ?></textarea>
    </div>
    <div>
        <h4 style="margin-bottom: 5px;"><?php echo __( 'Experience Book Now (Link)', 'baglioni-hotels' ) ?>:</h4>
        <input type="text" name="experience_book_now" value="<?php echo $experience_book_now; ?>" style="width: 300px" />
    </div>
    <?php
}

function save_custom_meta_box_experiences( $post_id ) {

    if ( !isset( $_POST['custom_meta_box_nonce'] ) || !wp_verify_nonce( $_POST['custom_meta_box_nonce'], basename( __FILE__ ) ) ){
        return;
    }

    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
        return;
    }

    if ( !current_user_can( 'edit_post', $post_id ) ) {
        return;
    }

    $post = get_post( $post_id );

    if ( $post->post_type != 'experience' ) {
        return;
    }
    
    update_post_meta( $post_id, 'experience-branch', sanitize_text_field( $_POST['experience_branch'] ) );

    if( $_POST['experience_top'] == 1 ) :
        update_post_meta( $post_id, 'experience-top', 1 );
    else :
        update_post_meta( $post_id, 'experience-top', 0 );
    endif;

    if( $_POST['experience_corporate_top'] == 1 ) :
        update_post_meta( $post_id, 'experience-corporate-top', 1 );
    else :
        update_post_meta( $post_id, 'experience-corporate-top', 0 );
    endif;

    if( $_POST['experience_highlighted'] == 1 ) :
        update_post_meta( $post_id, 'experience-highlighted', 1 );
    else :
        update_post_meta( $post_id, 'experience-highlighted', 0 );
    endif;

    update_post_meta( $post_id, 'experience-subtitle', sanitize_text_field( $_POST['experience_subtitle'] ) );
    update_post_meta( $post_id, 'experience-small-description', sanitize_text_field( $_POST['experience_small_description'] ) );
    update_post_meta( $post_id, 'experience-duration', sanitize_text_field( $_POST['experience_duration'] ) );
    update_post_meta( $post_id, 'experience-target', sanitize_text_field( $_POST['experience_target'] ) );
    update_post_meta( $post_id, 'experience-rating', sanitize_text_field( $_POST['experience_rating'] ) );
    update_post_meta( $post_id, 'experience-inclusions', sanitize_text_field( $_POST['experience_inclusions'] ) );
    update_post_meta( $post_id, 'experience-conditions', sanitize_text_field( $_POST['experience_conditions'] ) );
    update_post_meta( $post_id, 'experience-book-now', sanitize_text_field( $_POST['experience_book_now'] ) );
}
add_action( 'save_post', 'save_custom_meta_box_experiences', 10, 2 );


add_filter( 'get_the_archive_title', function ($title) {
    if ( is_category() ) :
        $title = single_cat_title( '', false );
    elseif ( is_tag() ) :
        $title = single_tag_title( '', false );
    endif;

    return $title;
});


function vc_before_init_actions() {

    // Require new custom Element
    require_once( __DIR__ . '/vc-elements/slider-rooms.php' );
    require_once( __DIR__ . '/vc-elements/slider-experiences.php' );
    require_once( __DIR__ . '/vc-elements/carousel-experiences.php' );
    require_once( __DIR__ . '/vc-elements/carousel-suites.php' );
    require_once( __DIR__ . '/vc-elements/grid-offers.php' );
    require_once( __DIR__ . '/vc-elements/grid-restaurants.php' );
    require_once( __DIR__ . '/vc-elements/grid-rooms.php' );
    require_once( __DIR__ . '/vc-elements/custom-box.php' );
    require_once( __DIR__ . '/vc-elements/multiple-button.php' );
}
add_action( 'vc_before_init', 'vc_before_init_actions' );

function get_branch_city( $branch_id ) {
    return get_post_meta( $branch_id, 'nd_booking_meta_box_cpt_4_city', true );
}

function get_branch_name( $branch_id ) {
    $branch_title = get_the_title( $branch_id );

    $length = strpos( $branch_title, ',' );

    if( $length ) :
        $branch_name = substr( $branch_title, 0, strpos( $branch_title, ',' ) );
    else :
        $branch_name = $branch_title;
    endif;

    return $branch_name;
}

function get_restaurant_branch( $restaurant_id ) {
    $branch_id = get_post_meta( $restaurant_id, 'restaurant-branch', true );

    return get_the_title( $branch_id );
}

function get_room_branch( $room_id ) {
    $branch_id = get_post_meta( $room_id, 'room-branch', true );

    return get_the_title( $branch_id );
}

function get_room_city( $room_id ) {
    $branch_id = get_post_meta( $room_id, 'room-branch', true );

    return get_post_meta( $branch_id, 'nd_booking_meta_box_cpt_4_city', true );
}

function get_experience_city( $experience_id ) {
    $branch_id = get_post_meta( $experience_id, 'experience-branch', true );

    return get_post_meta( $branch_id, 'nd_booking_meta_box_cpt_4_city', true );
}

function get_experience_category( $experience_id ) {
    return wp_get_post_terms( $experience_id, 'experience_category', array() )[0];
}

function get_the_excerpt_by_post_id( $post_id ) {
    global $post;
    $save_post = $post;
    $post = get_post($post_id);
    setup_postdata($post);
    $output = get_the_excerpt($post);
    wp_reset_postdata();
    $post = $save_post;
    return $output;
}

function custom_excerpt_length( $length ) {
    return 28;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

function get_max_string($text, $maxchar, $end = '...') {
    if (strlen($text) > $maxchar || $text == '') {
        $words = preg_split('/\s/', $text);      
        $output = '';
        $i = 0;
        while (1) {
            $length = strlen($output)+strlen($words[$i]);
            if ($length > $maxchar) {
                break;
            } 
            else {
                $output .= " " . $words[$i];
                ++$i;
            }
        }
        $output .= $end;
    } 
    else {
        $output = $text;
    }
    return $output;
}

function get_cities( $key = 'nd_booking_meta_box_cpt_4_city', $type = 'nd_booking_cpt_4', $status = 'publish' ) {

    global $wpdb;

    if( empty( $key ) )
        return;

    $r = $wpdb->get_col( $wpdb->prepare( "
        SELECT pm.meta_value FROM {$wpdb->postmeta} pm
        LEFT JOIN {$wpdb->posts} p ON p.ID = pm.post_id
        WHERE pm.meta_key = '%s' 
        AND p.post_status = '%s' 
        AND p.post_type = '%s'
        ORDER BY pm.meta_value ASC
    ", $key, $status, $type ) );

    return $r;
}
