jQuery(document).ready(function() {

    jQuery('.background-book-now').click(function() {

        jQuery('#book-now-widget').toggleClass('active');

        return false;
    });

    jQuery(window).scroll(function() {
        if(jQuery(window).scrollTop() > 20) {
            jQuery('#book-now-widget').addClass('sticky');
        } else {
            jQuery('#book-now-widget').removeClass('sticky');
        }
    });

    jQuery('#book-now-widget .close-icon').click(function() {
        jQuery('#book-now-widget').removeClass('active');

        return false;
    });

    jQuery('#book-now-widget .submit-button').click(function() {
        var hotel = jQuery('#book-now-widget select[name=dest]').val();
        var nights = jQuery('#book-now-widget select[name=nights]').val();
        var adults = jQuery('#book-now-widget select[name=adults]').val();
        var arrivalDate = jQuery('#book-now-widget input[name=arrival_date]').val();
        var departureDate = jQuery('#book-now-widget input[name=departure_date]').val();
        var children = jQuery('#book-now-widget select[name=children]').val();
        var rooms = jQuery('#book-now-widget select[name=rooms]').val();


        var arrivalDateArray = arrivalDate.split('/');
        var departureDateArray = departureDate.split('/');

        var arrivalDate = arrivalDateArray[1] + '/' + arrivalDateArray[0] + '/' + arrivalDateArray[2];
        var departureDate = departureDateArray[1] + '/' + departureDateArray[0] + '/' + departureDateArray[2];

        if(hotel) {
            window.open('https://gc.synxis.com/rez.aspx?Chain=' + baglioniHotelsLocalize.SYNXIS_CHAIN + '&Hotel=' + hotel + '&Shell=' + baglioniHotelsLocalize.SYNXIS_SHELL + '&Template=' + baglioniHotelsLocalize.SYNXIS_TEMPLATE + '&arrive=' + arrivalDate + '&depart=' + departureDate + '&adult=' + adults + '&child=' + children + '&rooms=' + rooms + '&start=availresults&locale=en-US', '_blank');
        }

        return false;
    });

    jQuery("#book-now-widget input[name=arrival_date]").datepicker({
        defaultDate: +1,
        minDate: 0,
        altField: "#book-now-widget input[name=arrival_date]",
        altFormat: "dd/mm/yy",
        firstDay: 0,
        dateFormat: "mm/dd/yy",
        nextText: "Next",
        prevText: "Prev",
    });

    jQuery("#book-now-widget input[name=departure_date]").datepicker({
        defaultDate: +2,
        minDate: 0,
        altField: "#book-now-widget input[name=departure_date]",
        altFormat: "dd/mm/yy",
        firstDay: 0,
        dateFormat: "dd/mm/yy",
        nextText: "Next",
        prevText: "Prev",
    });

    /* END BOOKING WIDGET */

    jQuery('#italian-talks a').not('#italian-talks a:nth-child(5)').click(function() {
        if(jQuery(this).attr('href') == 'http://dev.anteria.eu:8080/baglioni-hotels/around-us/') {
            window.open('https://www.italiantalks.com/italian-experience/readers-share-unforgettableday-ideas-florence-2/', '_blank');
            return false;
        } else if(jQuery(this).attr('href') == 'http://dev.anteria.eu:8080/baglioni-hotels/daily-walk/') {
            window.open('https://www.italiantalks.com/special-events/italian-aperitivo-masterclass-unforgettable-luxury-stay-2/', '_blank');
            return false;
        } else if(jQuery(this).attr('href') == 'http://dev.anteria.eu:8080/baglioni-hotels/new-website-online/') {
            window.open('https://www.italiantalks.com/italian-experience/36-hours-naples/', '_blank');
            return false;
        } else if(jQuery(this).attr('href') == 'http://dev.anteria.eu:8080/baglioni-hotels/relax-zone/') {
            window.open('https://www.italiantalks.com/italian-experience/italy-loving-bloggers-share-best-photos/', '_blank');
            return false;
        } else if(jQuery(this).attr('href') == 'http://dev.anteria.eu:8080/baglioni-hotels/quote-post/') {
            window.open('https://www.italiantalks.com/', '_blank');
            return false;
        }
    });

    /* END ITALIAN TALKS */

    jQuery('.nd_options_navigation_2_sidebar div ul.menu li > a[href="#"]').click(function() {
        return false;
    });

    jQuery('.room-title-link').click(function() {
        var href = jQuery(this).attr('href');

        jQuery('html, body').animate({ scrollTop: jQuery(href).offset().top - 110 }, 1500);

        return false;
    });

    jQuery('.nice-select').niceSelect();

    jQuery('.posts-grid .post .post-data').matchHeight();
    jQuery('.offers-grid .offer .offer-data').matchHeight();
    jQuery('.restaurants-grid .restaurant .restaurant-data').matchHeight();
    jQuery('.rooms-grid .room .room-data').matchHeight();
    jQuery('.custom-box .custom-box-data').matchHeight();

    jQuery('select[name=destination]').change(function() {
        jQuery('.nd_booking_section .nice-select .current').css('line-height', '20px');
    });

    jQuery('select[name=destination]').children().each(function() {
        var title = jQuery(document).find('title').text().replace(' – Baglioni Hotels', '').toUpperCase();
        var option = jQuery(this).text().toUpperCase();

        if(title == option) {
            jQuery(this).attr('selected', 'selected');
            jQuery('.nice-select').niceSelect('update')
            jQuery('.nd_booking_section .nice-select .current').css('line-height', '20px');
        }
    });

    jQuery('select[name=dest]').children().each(function() {
        var title = jQuery(document).find('title').text().replace(' – Baglioni Hotels', '').toUpperCase();
        var option = jQuery(this).text().toUpperCase();

        if(title == option) {
            jQuery(this).attr('selected', 'selected');
            jQuery('.nice-select').niceSelect('update')
            jQuery('.nd_booking_section .nice-select .current').css('line-height', '20px');
        }
    });

    jQuery('.nd_booking_section button[type=submit]').click(function() {
        var hotel = jQuery('select[name=destination]').val();
        var arrivalDate = jQuery('#nd_booking_archive_form_date_range_from').val();
        var departureDate = jQuery('#nd_booking_archive_form_date_range_to').val();
        var guests = jQuery('#nd_booking_archive_form_guests').val();
        var rate = jQuery('input[name=special_code]').val();

        if( hotel ) {
            window.open('https://gc.synxis.com/rez.aspx?Chain=' + baglioniHotelsLocalize.SYNXIS_CHAIN + '&Hotel=' + hotel + '&Shell=' + baglioniHotelsLocalize.SYNXIS_SHELL + '&Template=' + baglioniHotelsLocalize.SYNXIS_TEMPLATE + '&arrive=' + arrivalDate + '&depart=' + departureDate + '&adult=' + guests + '&rate=' + rate + '&start=availresults&locale=en-US', '_blank');
        }

        return false;
    });

    jQuery('.multiple-button select').change(function() {
        
        if(jQuery(this).val()) {
            window.open(jQuery(this).val());
            jQuery(this).next().find('.option:first-of-type').click();
        }
    });

    jQuery('.slider-rooms').slick({
        arrows: true
    });

    jQuery('.slider-experiences').slick({
        arrows: true
    });

    jQuery('.carousel-experiences.2-columns').slick({
        arrows: true,
        dots: true,
        infinite: true,
        slidesToShow: 2,
        slidesToScroll: 2,
        responsive: [
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1
              }
            }
        ]
    });
    jQuery('.carousel-experiences.4-columns').slick({
        arrows: true,
        dots: true,
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 4,
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1
              }
            }
        ]
    });
    jQuery('.carousel-experiences .experience-data').matchHeight();

    jQuery('.vc_tta-tab a').click(function() {
        setTimeout(function() {
            jQuery('.carousel-experiences').slick('setPosition');
            jQuery.fn.matchHeight._update()
        }, 500);
    });

    jQuery('.carousel-suites.3-columns').slick({
        arrows: true,
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 3,
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });
    jQuery('.carousel-suites.2-columns').slick({
        arrows: true,
        infinite: true,
        slidesToShow: 2,
        slidesToScroll: 2,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });
    jQuery('.carousel-suites .suite-box-data').matchHeight();

    jQuery('.slider-rooms .other-rooms .room').click(function() {
        var index = jQuery(this).data('index');

        jQuery('.slider-rooms .slick-active .main-room .room').hide();
        jQuery('.slider-rooms .slick-active .main-room .room').eq(index).fadeIn();

        jQuery('.slider-rooms .slick-active .other-rooms .room').removeClass('active');
        jQuery(this).addClass('active');

        return false;
    });

    jQuery('.slider-experiences .other-experiences .experience').click(function() {
        var index = jQuery(this).data('index');

        jQuery('.slider-experiences .slick-active .main-experience .experience').hide();
        jQuery('.slider-experiences .slick-active .main-experience .experience').eq(index).fadeIn();

        jQuery('.slider-experiences .slick-active .other-experiences .experience').removeClass('active');
        jQuery(this).addClass('active');

        return false;
    });

    jQuery('.square-room').click(function (event) {
        event.preventDefault();

        var index = jQuery(this).data('index');

        jQuery('.slider-rooms .slick-active .main-room .room').hide();
        jQuery('.slider-rooms .slick-active .main-room .room').eq(index).fadeIn();

        jQuery(this).siblings().removeClass('active');
        jQuery(this).addClass('active');

        return false;
    });

    jQuery('.square-experience').click(function (event) {
        event.preventDefault();

        var index = jQuery(this).data('index');

        jQuery('.slider-experiences .slick-active .main-experience .experience').hide();
        jQuery('.slider-experiences .slick-active .main-experience .experience').eq(index).fadeIn();

        jQuery(this).siblings().removeClass('active');
        jQuery(this).addClass('active');

        return false;
    });

    jQuery('.hotel-tab').click(function() {
        jQuery('.hotel-tab').removeClass('active');
        jQuery(this).addClass('active');

        var index =  jQuery(this).data('index');

        jQuery('.hotel').hide();
        jQuery('.hotel:eq(' + index +')').fadeIn();

        jQuery('#hotel-tab-contents .hotel-name').hide();
        jQuery('#hotel-tab-contents .hotel-name:eq(' + index + ')').fadeIn();

        jQuery('#hotel-tab-contents .carousel-suites').hide();
        jQuery('#hotel-tab-contents .carousel-suites:eq(' + index + ')').fadeIn();

        setTimeout(function() {
            jQuery('.carousel-suites').slick('setPosition');
            jQuery.fn.matchHeight._update();
        }, 100);

        return false;
    });

    jQuery('.around-the-hotel-btn').click(function() {
        jQuery('#overview').hide();
        jQuery('#around-the-hotel-overview').css('display', 'flex');

        jQuery('html, body').animate({ scrollTop: jQuery('#around-the-hotel-overview').offset().top - 110 }, 1000);

        return false;
    });

    jQuery('.overview-btn').click(function() {
        jQuery('#around-the-hotel-overview').hide();
        jQuery('#overview').css('display', 'flex');

        jQuery('html, body').animate({ scrollTop: jQuery('#overview').offset().top - 110 }, 1000);

        return false;
    });

    jQuery(window).on('orientationchange', function() {
        setTimeout(function() {
            jQuery('.carousel-suites').slick('setPosition');
            jQuery.fn.matchHeight._update();
        }, 100);
    });
});