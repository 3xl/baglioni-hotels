/* Baglioni Hotels Newsletter - Javascript */

jQuery(document).ready(function() {

	jQuery('.recruitment-form input[name=agree_data]').change(function() {
		if(jQuery(this).is(':checked')) {
			jQuery('.recruitment-form .radio-options').fadeIn();
		} else {
			jQuery('.recruitment-form .radio-options').fadeOut();
		}
	});

	// Recruitment request
	jQuery('.recruitment-form').each(function() {
		jQuery(this).validate({
			rules: {
				email: {
			    	required: true,
			    	email: true
			    },

			    agree_data: 'required',

			    agree_marketing: 'required',
		  	},

		  	messages: {
			    email: {
			      required: baglioniHotelsNewsletter.fieldRequired,
			      email: baglioniHotelsNewsletter.validEmail
			    },

			    agree_data: baglioniHotelsNewsletter.fieldRequired,

			    agree_marketing: baglioniHotelsNewsletter.fieldRequired,
		  	},

		  	submitHandler: function(form) {

				jQuery.ajax({
			  		type: "POST",
			  		url: baglioniHotelsNewsletter.ajaxUrl,
			  		data: {
			  			email: jQuery('.recruitment-form input[name=email]').val(),
			  			agree_marketing: jQuery('.recruitment-form input[name=agree_marketing]:checked').val(),
			  			action: 'recruitment_request'
			  		},
			  		dataType: 'json',

			  		beforeSend: function() {

			  			// Loading
						swal({
						  title: baglioniHotelsNewsletter.loading,
						  allowOutsideClick: false,
						  onOpen: () => {
						    swal.showLoading()
						  }
						});
			  		},

			  		success: function(data) {
						if(data.result) {
							// Success
							swal(
							  baglioniHotelsNewsletter.success,
							  baglioniHotelsNewsletter.successRecruitment,
							  'success'
							);

							form.reset();
							jQuery('.recruitment-form .radio-options').hide();

							if(typeof gtag == 'function') { 
  								gtag('event', 'Send Newsletter form');
							}
						} else {
							var errors = '';

							data.errors.forEach(function(error) {
								errors += '<div>' + error + '</div>';
							});
							
							// Error
							swal(
							  baglioniHotelsNewsletter.errorMessage,
							  errors,
							  'error'
							);
						}
					},

					error: function(error) {
						alert(baglioniHotelsNewsletter.ajaxError);
					}
				});
		  	}
		});
	});
});