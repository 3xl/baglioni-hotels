<?php

/*
Plugin Name: Baglioni Hotels Newsletter
Version: 1.0
Description: Plugin che permette l'integrazione del form di richiesta iscrizione Newsletter per il sito Baglioni Hotels
Author: Anteria Srl
Author URI: https://www.anteria.eu
Text Domain: baglioni-hotels-newsletter
*/

if( !defined( 'ABSPATH' ) ) {
	exit;
}

require __DIR__ . '/includes/GUMP/gump.class.php';
require __DIR__ . '/includes/PHPExcel/PHPExcel.php';

if ( !session_id() ) {
	session_start();
}

// Viene definita una costante contenente l'URL del plugin
define( 'BAGLIONI_HOTELS_NEWSLETTER', plugin_dir_url( __FILE__ ) );
define( 'BAGLIONI_HOTELS_NEWSLETTER_EMAIL', 'roberto.bruno@anteria.eu' );

// Creazione dei Post personalizzati
function baglioni_hotels_newsletter_post_type_init() {
    register_post_type( 'newsletter',
        array(
            'labels' => array(
                'name' => __( 'Newsletter', 'baglioni-hotels-newsletter' ),
                'singular_name' => __( 'Newsletter', 'baglioni-hotels-newsletter' )
            ),
            'public' => false,
            'supports' => array( 'title', 'custom-fields' ),
            'menu_icon' => 'dashicons-email-alt',
            'show_ui' => true
        )
    );
}
add_action( 'init', 'baglioni_hotels_newsletter_post_type_init' );


// Titoli colonne personalizzate per la tabella delle richieste di newsletter
function newsletter_custom_columns_title( $columns ) {
    $columns = array(
        'cb' => '<input type="checkbox" />',
        'title' => __( 'Email', 'baglioni-hotels-newsletter' ),
        'newsletter-agree-marketing' => __( 'Marketing acceptance',  'baglioni-hotels-newsletter' ),
        'date' => __( 'Date', 'baglioni-hotels-newsletter' )
    );

    return $columns;
}
add_filter( 'manage_edit-newsletter_columns', 'newsletter_custom_columns_title' ) ;

// Riempimento colonne personalizzate per la tabella delle prenotazioni
function newsletter_custom_columns( $column, $post_id ) {
    global $post;

    switch( $column ) {

        case 'newsletter-agree-marketing' :

            $agree_marketing = get_post_meta( $post_id, 'newsletter-agree-marketing', true );

            echo $agree_marketing;

            break;

        default :
            break;
    }
}
add_action( 'manage_newsletter_posts_custom_column', 'newsletter_custom_columns', 10, 2 );


// Inclusione file
require plugin_dir_path( __FILE__ ) . '/shortcodes.php';


// Effettua la registrazione della cartella con le traduzioni
function BAGLIONI_HOTELS_NEWSLETTER_load_textdomain() {
	load_plugin_textdomain( 'baglioni-hotels-newsletter', false, plugin_basename( dirname( __FILE__ ) ) . '/languages/' );
}


// Aggiunge il multilingua
add_action( 'plugins_loaded', 'BAGLIONI_HOTELS_NEWSLETTER_load_textdomain' );


function export_newsletter_excel() {
    $args = array( 'post_type' => 'newsletter', 'posts_per_page' => -1, 'orderby' => 'date', 'order' => 'ASC' );
    $newsletters = get_posts( $args );

    $objPHPExcel = new PHPExcel();

    $objPHPExcel->setActiveSheetIndex(0);

    $objPHPExcel->getActiveSheet()->setTitle( __( 'Newsletter', 'baglioni-hotels-newsletter' ) );

    $objPHPExcel->getActiveSheet()->SetCellValueExplicit('A1', __( 'Email', 'baglioni-hotels-newsletter' ), PHPExcel_Cell_DataType::TYPE_STRING );
    $objPHPExcel->getActiveSheet()->SetCellValueExplicit('B1', __( 'Marketing acceptance', 'baglioni-hotels-newsletter' ), PHPExcel_Cell_DataType::TYPE_STRING );
    $objPHPExcel->getActiveSheet()->SetCellValueExplicit('C1', __( 'Date', 'baglioni-hotels-newsletter' ), PHPExcel_Cell_DataType::TYPE_STRING );
    
    foreach( $newsletters as $newsletter ) : setup_postdata( $newsletter );

        $row = $objPHPExcel->getActiveSheet()->getHighestRow() + 1;

        $objPHPExcel->getActiveSheet()->SetCellValueExplicit('A'.$row, $newsletter->post_title, PHPExcel_Cell_DataType::TYPE_STRING );
        $objPHPExcel->getActiveSheet()->SetCellValueExplicit('B'.$row, get_post_meta( $newsletter->ID, 'newsletter-agree-marketing', true ), PHPExcel_Cell_DataType::TYPE_STRING );
        $objPHPExcel->getActiveSheet()->SetCellValueExplicit('C'.$row, $newsletter->post_date, PHPExcel_Cell_DataType::TYPE_STRING );

    endforeach;
    wp_reset_postdata();

    // Auto size columns for each worksheet
    foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {

        $objPHPExcel->setActiveSheetIndex($objPHPExcel->getIndex($worksheet));

        $sheet = $objPHPExcel->getActiveSheet();
        $cellIterator = $sheet->getRowIterator()->current()->getCellIterator();
        $cellIterator->setIterateOnlyExistingCells(true);

        /** @var PHPExcel_Cell $cell */
        foreach ($cellIterator as $cell) {
            $sheet->getColumnDimension($cell->getColumn())->setAutoSize(true);
        }
    }

    $objPHPExcel->setActiveSheetIndex(0);

    // We'll be outputting an excel file
    header('Content-type: application/vnd.ms-excel');

    // It will be called file.xls
    header('Content-Disposition: attachment; filename="newsletter.xlsx"');

    $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
    $objWriter->save('php://output');
}
add_action( 'admin_post_export_newsletter_excel', 'export_newsletter_excel' );


// Inserimento nuova richiesta di iscrizione newsletter
function insert_newsletter_request( $data ) {
    $args = array(
        'post_title' => $data['email'],
        'post_status' => 'publish',
        'post_type' => 'newsletter',
    );

    $newsletter = wp_insert_post( $args );

    add_post_meta( $newsletter, 'newsletter-agree-marketing', $data['agree_marketing'], true );

    return $newsletter;
}


// Abilita l'invio di email HTML
function html_mail_content_type(){
    return "text/html";
}
add_filter( 'wp_mail_content_type','html_mail_content_type' );


// Funzione che effettua la richiesta di recruitment
function ajax_recruitment_request() {

    $_POST = array_map('stripslashes', $_POST);

    // Validazione dati
    $gump = new BaglioniHotelsNewsletter\GUMP();

    $rules = array(
        'email' => 'required|valid_email',
        'agree_marketing' => 'required',
    );

    $gump->validation_rules( $rules );

    $gump->filter_rules( array(
        'email' => 'trim',
        'agree_marketing' => 'trim',
    ) );

    $is_valid = $gump->run( $_POST );

    // Risposta negativa se la validazione non va a buon fine
    if( $is_valid === false ) :
        echo json_encode( array( 'result' => false, 'errors' => $gump->get_readable_errors() ) );
        die();
    endif;

    // Inserimento della nuova richiesta di iscrizione newsletter
    insert_newsletter_request( $_POST );

    // Invio email
    send_recruitment_email( $_POST );
    send_recruitment_admin_email( $_POST );

    echo json_encode( ['result' => true] );
    exit();
}

// Aggiunge l'azione di risposta a chiamata ajax recreuitment
add_action( 'wp_ajax_recruitment_request', 'ajax_recruitment_request' );
add_action( 'wp_ajax_nopriv_recruitment_request', 'ajax_recruitment_request' );


function send_recruitment_email( $data ) {
    $subject = __( 'Baglioni Hotels - Newsletter', 'baglioni-hotels-newsletter' );

    $message .= '<p>' . __( 'Thank you for your request.', 'baglioni-hotels-newsletter' ) . '</p>';

    $message .= '<p>' . __( 'Our staff will contact you as soon as possible.', 'baglioni-hotels-newsletter' );

    $message .= '<br>';

    $message .= '<p>' . __( 'Kind regards', 'baglioni-hotels-newsletter' ) . '</p>';
    $message .= '<p>' . __( 'Baglioni Hotels', 'baglioni-hotels-newsletter' ) . '</p>';
    $message .= '<p><a href="https://www.baglionihotels.com"><img src="' . BAGLIONI_HOTELS_NEWSLETTER . 'images/baglioni-hotels-logo.png" width="180" height="60" alt="Baglioni Hotels Logo" /></a></p>';

    return wp_mail( $data['email'], $subject, $message );
}


// Funzione che effettua l'invio dell'email di recruitment
function send_recruitment_admin_email( $data ) {
    $subject = __( 'Baglioni Hotels - Newsletter request', 'baglioni-hotels-newsletter' );

    $message = '<p>' . __( 'New request for Newsletter:', 'baglioni-hotels-newsletter' ) . '</p>';

    $message .= __( 'Email address', 'baglioni-hotels-newsletter' ) . ': <strong>' . $data['email'] . '</strong><br>';
    $message .= __( 'Marketing acceptance', 'baglioni-hotels-newsletter' ) . ': <strong>' . $data['agree_marketing'] . '</strong><br>';

    return wp_mail( BAGLIONI_HOTELS_NEWSLETTER_EMAIL, $subject, $message );
}


// Integra il pulsante per l'esportazione su Excel
function custom_js_to_head() {
    ?>
    <script>
    jQuery(function() {
        
        jQuery("body.post-type-newsletter .wrap h1").append('<form action="<?php echo admin_url('admin-post.php'); ?>" method="post" style="display: inline;"><button type="submit" class="page-title-action"><?php echo __( 'Export to Excel', 'baglioni-hotels-newsletter' ) ?></button><input type="hidden" name="action" value="export_newsletter_excel"></form>');
    });
    </script>
    <?php
}
add_action('admin_head', 'custom_js_to_head');
