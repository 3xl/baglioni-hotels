<?php

class BaglioniHotelsNewsletter {

	public static $add_script;
	
	public static function init() {
		add_shortcode( 'baglioni-hotels-newsletter-form', array( __CLASS__, 'handle_shortcode_baglioni_hotels_newsletter_form' ) );

		add_action( 'init', array( __CLASS__, 'register_scripts' ) );
		add_action( 'init', array( __CLASS__, 'register_styles' ) );
		add_action( 'wp_footer', array( __CLASS__, 'inline_scripts' ) );
		add_action( 'wp_footer', array( __CLASS__, 'print_scripts' ) );
	}

	public static function handle_shortcode_baglioni_hotels_newsletter_form( $atts ) {
		self::$add_script = true;

		$content = '<form method="post" class="recruitment-form" novalidate>';

		$content .= '<div class="email-field">';
		$content .= '<input type="email" name="email" placeholder="' . __( 'Enter your email address', 'baglioni-hotels-newsletter' ) . '" />';
		$content .= '<button type="submit" class="recruitment-btn">' . __( 'GO', 'baglioni-hotels-newsletter' ) . '</button>';
		$content .= '</div>'; // .email-field
		
		$content .= '<p><label><input type="checkbox" name="agree_data" style="width: auto;"> ' .  __( 'I have read', 'baglioni-hotels-newsletter' ) . ' <a href="' . __( get_permalink( get_page_by_path( 'privacy-policy' ) ) ) . '" class="privacy-policy-newsletter" target="blank">' . __( 'the privacy policy', 'baglioni-hotels-newsletter' ) .'</a>' .', ' . __( 'and I authorise Baglioni Hotels SpA to process my personal data in order to send me the newsletter requested.', 'baglioni-hotels-newsletter' ) . '</label></p>';
		
		$content .= '<div class="radio-options" style="display: none;">';

		$content .= '<div class="radio-inline">';
		$content .= '<label><input type="radio" name="agree_marketing" value="Si"> ' . __( 'I authorise', 'baglioni-hotels-newsletter' ) . '</label>';
		$content .= '</div>'; // .radio-inline

		$content .= '<div class="radio-inline">';
		$content .= '<label><input type="radio" name="agree_marketing" value="No"> ' . __( 'I do not authorise', 'baglioni-hotels-newsletter' ) . '</label>';
		$content .= '</div>'; // .radio-inline

		$content .= '<div class="policy-description">' . __( 'Baglioni Hotels SpA to process my personal data for marketing purposes (emailing me promotional material on products or services provided and/or promoted by the company).', 'baglioni-hotels-newsletter' ) .'</div>';

		$content .= '</div>'; // .radio-options

		$content .= '</form>'; // .recruitment-form

		return $content;
	}

	public static function register_scripts() {
		wp_register_script( 'jquery-validate', BAGLIONI_HOTELS_NEWSLETTER . 'js/jquery-validation/jquery.validate.min.js', 'jquery', '1.17.0', true );

		wp_register_script( 'sweetalert2', BAGLIONI_HOTELS_NEWSLETTER . 'js/sweetalert2.all.js', 'jquery', '7.1.1', true );

		wp_register_script( 'baglioni-hotels-newsletter', BAGLIONI_HOTELS_NEWSLETTER . 'js/custom.js', 'jquery', '1.0', true );
		$translation_array = array(
	        'BAGLIONI_HOTELS_NEWSLETTER' => BAGLIONI_HOTELS_NEWSLETTER,
	        'locale' => get_locale(),
	        'ajaxUrl' => admin_url('admin-ajax.php'),
	        'ajaxError' => __( 'An error has occurred. Please refresh the page or contact the webmaster.', 'baglioni-hotels-newsletter' ),
	        'loading' => __( 'Sending your request ...', 'baglioni-hotels-newsletter' ),
	        'success' => __( 'Request successfully completed', 'baglioni-hotels-newsletter' ),
	        'successMessage' => __( 'Our staff will contact you shortly.', 'baglioni-hotels-newsletter' ),
	        'successRecruitment' => __( 'You will receive a confirmation email shortly.', 'baglioni-hotels-newsletter' ),
	        'errorMessage' => __( 'We are sorry, an error has occured with your request:', 'baglioni-hotels-newsletter' ),
	        'fieldRequired' => __( 'This information is required.', 'baglioni-hotels-newsletter' ),
	        'validEmail' => __( 'The email address appears not to be valid.', 'baglioni-hotels-newsletter' ),
    	);
   		wp_localize_script( 'baglioni-hotels-newsletter', 'baglioniHotelsNewsletter', $translation_array );
	}

	public static function register_styles() {
		wp_register_style( 'baglioni-hotels-newsletter', BAGLIONI_HOTELS_NEWSLETTER . 'css/style.css', false, '1.0' );
	}

	public static function print_scripts() {
		if ( !self::$add_script )
			return;

		// print JS
		wp_print_scripts( 'jquery-validate' );
		wp_print_scripts( 'sweetalert2' );
		wp_print_scripts( 'baglioni-hotels-newsletter' );

		// print CSS
		wp_print_styles( 'baglioni-hotels-newsletter' );
	}
}
