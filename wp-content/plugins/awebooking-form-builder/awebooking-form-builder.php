<?php
/**
 * Plugin Name:     AweBooking Form Builder
 * Plugin URI:      http://awethemes.com/plugins/awebooking
 * Description:     Build and add your own custom fields in the booking check-out form to collect any information you need for each reservation.
 * Author:          awethemes
 * Author URI:      http://awethemes.com
 * Text Domain:     awebooking-form-builder
 * Domain Path:     /languages
 * Version:         0.2.0
 *
 * @package         Awebooking/Form_Builder
 */

use AweBooking\AweBooking;
use AweBooking\Form_Builder\Form_Builder_Addon;

/**
 * Init the WooCommerce support.
 *
 * @param  AweBooking $awebooking AweBooking instance.
 * @return void
 */

add_action( 'awebooking/init', function( AweBooking $awebooking ) {
	skeleton_psr4_autoloader( 'AweBooking\\Form_Builder\\', trailingslashit( __DIR__ ) . 'inc/' );

	$awebooking->register_addon( new Form_Builder_Addon( 'awebooking-form-builder', __FILE__ ) );
});

/**
 * Gets instance of Form_Builder.
 *
 * @return Form_Builder
 */
function awebooking_form_builder() {
	return awebooking()->get_addon( 'awethemes.awebooking-form-builder' );
}
