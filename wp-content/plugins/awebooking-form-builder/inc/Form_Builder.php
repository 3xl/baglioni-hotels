<?php
namespace AweBooking\Form_Builder;

use Skeleton\CMB2\CMB2;
use AweBooking\Admin\Fields\Field_Proxy;

class Form_Builder extends CMB2 {
	/**
	 * Form constructor.
	 */
	public function __construct( array $fields ) {
		parent::__construct([
			'id'         => 'awebooking-checkout-form',
			'hookup'     => false,
			'cmb_styles' => false,
		]);

		// Prevent CMB2 get field data from database.
		$this->object_id( '_' );
		$this->object_type( 'options-page' );

		// Loop througth fields and add cleaned field.
		foreach ( $fields as $field ) {
			$this->add_field( (new Field_Builder( $field ))->get_field_args() );
		}
	}

	/**
	 * Get sanitized values of the form.
	 *
	 * @param  array|null $data An array input data, if null $_POST will be use.
	 * @return array|mixed
	 */
	public function get_sanitized( array $data = null ) {
		$data  = is_null( $data ) ? $_POST : $data;

		// Get sanitized values from input data.
		$sanitized = $this->get_sanitized_values( $data );

		return $sanitized;
	}

	/**
	 * Get a field object.
	 *
	 * @param  mixed           $field The field id or field config array or CMB2_Field object.
	 * @param  CMB2_Field|null $group Optional, CMB2_Field object (group parent).
	 * @return Field_Proxy|null
	 */
	public function get_field( $field, $group = null, $reset_cached = false ) {
		$field = parent::get_field( $field, $group, $reset_cached );

		return $field ? new Field_Proxy( $this, $field ) : null;
	}

	/**
	 * Output the form.
	 *
	 * @return void
	 */
	public function output() {
		$this->show_form();
	}
}
