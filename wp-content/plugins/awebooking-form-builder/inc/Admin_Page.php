<?php
namespace AweBooking\Form_Builder;

use Illuminate\Support\Arr;
use AweBooking\Support\Collection;

class Admin_Page {

	public function __construct() {
	}

	/**
	 * Output the page.
	 *
	 * @return void
	 */
	public function output() {
		if ( ! empty( $_POST ) ) {
			$controls = json_decode(
				sanitize_text_field( wp_unslash( $_POST['_awebooking_controls'] ) ),
				true
			);

			$controls = array_map( function ( $_controls ) {
				foreach ( $_controls as &$control ) {
					$control = (new Field_Builder( $control ))->get_args_for_db();
				}

				return $_controls;
			}, Arr::only( $controls, [ 'col1', 'col2', 'col3' ] ) );

			// Validate controls...
			update_option( '_awebooking_checkout_controls', $controls );
		}

		require_once ABSPATH . 'wp-admin/admin-header.php';

		?><div class="wrap">
			<h1 class="wp-heading-inline"><?php echo esc_html__( 'Checkout Form', 'awebooking-form-builder' ); ?></h1>
			<hr class="wp-header-end">

			<form action="" method="POST">
				<div id="awebooking-checkout-form-builder"></div>

				<div style="">
					<ul>
						<li><code>required</code> - Field is required</li>
						<li><code>accepted</code> - Checkbox or Radio must be accepted (yes, on, 1, true)</li>
						<li><code>numeric</code> - Must be numeric</li>
						<li><code>integer</code> - Must be integer number</li>
						<li><code>boolean</code> - Must be boolean</li>
						<li><code>length</code> - String must be certain length</li>
						<li><code>lengthBetween</code> - String must be between given lengths</li>
						<li><code>lengthMin</code> - String must be greater than given length</li>
						<li><code>lengthMax</code> - String must be less than given length</li>
						<li><code>min</code> - Minimum</li>
						<li><code>max</code> - Maximum</li>
						<li><code>in</code> - Performs in_array check on given array values</li>
						<li><code>notIn</code> - Negation of <code>in</code> rule (not in array of values)</li>
						<li><code>ip</code> - Valid IP address</li>
						<li><code>email</code> - Valid email address</li>
						<li><code>url</code> - Valid URL</li>
						<li><code>urlActive</code> - Valid URL with active DNS record</li>
						<li><code>alpha</code> - Alphabetic characters only</li>
						<li><code>alphaNum</code> - Alphabetic and numeric characters only</li>
						<li><code>slug</code> - URL slug characters (a-z, 0-9, -, _)</li>
						<li><code>date</code> - Field is a valid date</li>
						<li><code>dateFormat</code> - Field is a valid date in the given format</li>
						<li><code>dateBefore</code> - Field is a valid date and is before the given date</li>
						<li><code>dateAfter</code> - Field is a valid date and is after the given date</li>
						<li><code>contains</code> - Field is a string and contains the given string</li>
						<li><code>creditCard</code> - Field is a valid credit card number</li>
						<li><code>optional</code> - Value does not need to be included in data array. If it is however, it must pass validation.</li>
					</ul>
				</div>
			</form>
		</div><?php
	}
}
