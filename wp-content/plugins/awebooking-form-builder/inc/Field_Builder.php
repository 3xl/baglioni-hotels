<?php
namespace AweBooking\Form_Builder;

class Field_Builder {

	protected $args;

	public function __construct( array $args ) {
		$this->args = $this->clean_args( $args );
	}

	/**
	 * Determines if this is a core field.
	 *
	 * @return boolean
	 */
	public function is_core_field() {
		$core_fields = awebooking_form_builder()
			->get_core_controls()
			->pluck( 'id' )
			->to_array();

		return in_array( $this->args['id'], $core_fields );
	}

	/**
	 * Returns a clean of CMB2 field args.
	 *
	 * @return array
	 */
	public function get_field_args() {
		$options = [];
		if ( ! empty( $this->args['options'] ) && is_string( $this->args['options'] ) ) {
			$options = explode( "\n", $this->args['options'] );
			$options = array_unique( array_filter( $options ) );
			$options = array_combine( $options, $options );
		}

		return [
			'id'         => $this->args['id'],
			'type'       => $this->args['_type'],
			'name'       => $this->args['name'],
			'desc'       => isset( $this->args['desc'] ) ? $this->args['desc'] : '',
			'validate'   => isset( $this->args['validate'] ) ? $this->args['validate'] : '',
			'default'    => '',
			'options'    => $options,
			'show_names' => true,
			'repeatable' => false, // Don't change this!
			'on_front'   => true,  // Must be alway true.
			'attributes' => [],
		];
	}

	/**
	 * Returns a clean of CMB2 field args.
	 *
	 * @return array
	 */
	public function get_args_for_db() {
		$args = [
			'id'         => sanitize_key( $this->args['id'] ),
			'type'       => $this->args['_type'],
			'name'       => sanitize_text_field( $this->args['name'] ),
			'desc'       => isset( $this->args['desc'] ) ? sanitize_text_field( $this->args['desc'] ) : '',
			'default'    => '',
			'attributes' => [],
		];

		$validate_rule = isset( $this->args['validate'] ) ? $this->args['validate'] : null;
		$args['validate'] = $this->sanitize_validate( $this->args['id'], $validate_rule );

		if ( isset( $this->args['options'] ) && is_string( $this->args['options'] ) ) {
			$args['options'] = sanitize_textarea_field( $this->args['options'] );
		}

		return $args;
	}

	protected function sanitize_validate( $field_id, $raw_validate ) {
		$raw_validate = trim( trim( $raw_validate ), '|' );

		if ( $this->is_core_field() ) {
			$core_field = awebooking_form_builder()
				->get_core_controls()
				->get( $field_id );

			$raw_validate .= isset( $core_field['validate'] ) ? '|' . $core_field['validate'] : null;
		}

		$validate_rule = implode( '|', array_unique(
			array_filter( explode( '|', $raw_validate ) )
		));

		if ( ! $validate_rule ) {
			return;
		}

		try {
			$builder = new Validator_Builder( $validate_rule );
			return $validate_rule;
		} catch ( \Exception $e ) {
			return isset( $core_field['validate'] ) ? $core_field['validate'] : null;
		}
	}

	/**
	 * Run clean raw field args.
	 *
	 * @param  array $args Raw field args.
	 * @return array
	 */
	protected function clean_args( array $args ) {
		$supported_types = awebooking_form_builder()->get_supported_types();

		if ( $supported_types->has( $args['type'] ) ) {
			$args['_type'] = $supported_types->get( $args['type'] )['type'];
		} else {
			// If not found the type, force to text field type.
			$args['_type'] = 'text';
		}

		return $args;
	}
}
