<?php
namespace AweBooking\Form_Builder;

use Skeleton\Support\Validator;

class Validator_Builder extends Validator {
	/**
	 * Create a validator builder.
	 *
	 * @param array $validate_rule Raw string validate rule.
	 */
	public function __construct( $validate_rule ) {
		parent::__construct( [], [ 'test_rule' => $validate_rule ] );
	}

	public function get_parsed_rule() {
		return $this->rules['test_rule'][0];
	}
}
