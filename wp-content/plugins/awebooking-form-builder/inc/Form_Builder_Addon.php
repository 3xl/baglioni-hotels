<?php
namespace AweBooking\Form_Builder;

use AweBooking\Support\Addon;
use AweBooking\Support\Collection;

class Form_Builder_Addon extends Addon {
	const VERSION = '0.2.0';

	protected $controls;
	protected $supported_types;

	/**
	 * Requires minimum AweBooking version.
	 *
	 * @return string
	 */
	public function requires() {
		return '3.0.0';
	}

	/**
	 * Registers services on the awebooking.
	 *
	 * @return void
	 */
	public function register() {
		$this->controls = $this->get_controls();

		load_plugin_textdomain( 'awebooking-form-builder', false, dirname( $this->get_basename() ) . '/languages' );
	}

	/**
	 * Init the addon.
	 */
	public function init() {
		add_action( 'awebooking/register_admin_scripts', [ $this, '_register_admin_scripts' ] );

		remove_action( 'awebooking/checkout/customer_form', 'awebooking_template_checkout_customer_form', 10 );
		add_action( 'awebooking/checkout/customer_form', [ $this, '_output_checkout_form' ] );

		add_action( 'awebooking/checkout/validator_rules', [ $this, '_checkout_validator_rules' ] );
		add_action( 'awebooking/checkout/validator_labels', [ $this, '_checkout_validator_labels' ] );

		add_action( 'awebooking/booking_created', [ $this, '_add_booking_meta' ] );

		add_action( 'awebooking/booking/register_metabox_fields', [ $this, '_register_metabox_fields' ] );
		add_filter( 'awebooking/after_add_email_notes', [ $this, '_add_email_notes' ] );
		add_filter( 'awebooking/notification/email_fields', [ $this, '_add_email_fields' ], 10, 3 );

		$this->awebooking['admin_menu']->add_submenu(
			'awebooking-form-builder', array(
				'page_title'  => esc_html__( 'Checkout Form', 'awebooking-form-builder' ),
				'menu_title'  => esc_html__( 'Checkout Form', 'awebooking-form-builder' ),
				'noheader'    => true,
				'function' => function() {
					(new Admin_Page())->output();
				},
			)
		);
	}

	public function get_controls() {
		$core_controls = $this->get_core_controls()->where( '_plugable', false );
		$_user_controls = (array) get_option( '_awebooking_checkout_controls', [] );

		// Valid user controls...
		$user_controls = [
			'col1' => [],
			'col2' => [],
			'col3' => [],
		];

		if ( isset( $_user_controls['col1'] ) ) {
			foreach ( $_user_controls as $col => $col_controls ) {
				foreach ( $col_controls as $_control ) {
					if ( empty( $_control['id'] ) || empty( $_control['type'] ) ) {
						continue;
					}

					$user_controls[ $col ][] = $_control;
				}
			}
		}

		// Add missing core control.
		$flatten_user_controls = Collection::make( $user_controls )->flatten( 1 );
		foreach ( $core_controls as $_core_control ) {
			$control_id = $_core_control['id'];

			if ( ! $flatten_user_controls->contains( 'id', $control_id ) ) {
				$user_controls['col1'][] = $_core_control;
			}
		}

		return new Collection( $user_controls );
	}

	public function get_core_controls() {
		return new Collection(
			[
				'customer_first_name' => [
					'id'               => 'customer_first_name',
					'type'             => 'text',
					'name'             => esc_html__( 'First Name', 'awebooking-form-builder' ),
					'validate'         => 'required',
					'_plugable'        => false,
				],
				'customer_last_name' => [
					'id'               => 'customer_last_name',
					'type'             => 'text',
					'name'             => esc_html__( 'Last Name', 'awebooking-form-builder' ),
					'validate'         => 'required',
					'_plugable'        => false,
				],
				'customer_phone' => [
					'id'               => 'customer_phone',
					'type'             => 'text',
					'name'             => esc_html__( 'Phone', 'awebooking-form-builder' ),
					'validate'         => 'required',
					'_plugable'        => false,
				],
				'customer_email' => [
					'id'               => 'customer_email',
					'type'             => 'text',
					'name'             => esc_html__( 'Email', 'awebooking-form-builder' ),
					'validate'         => 'required|email',
					'_plugable'        => false,
				],
				'customer_title' => [
					'id'               => 'customer_title',
					'type'             => 'select',
					'name'             => esc_html__( 'Customer Title', 'awebooking-form-builder' ),
					'options_cb'       => 'awebooking_get_common_titles',
					'show_option_none' => '---',
					'validate'         => '',
					'_plugable'        => true,
				],
				'customer_address' => [
					'id'               => 'customer_address',
					'type'             => 'text',
					'name'             => esc_html__( 'Address', 'awebooking-form-builder' ),
					'validate'         => '',
					'_plugable'        => true,
				],
				'customer_address_2' => [
					'id'               => 'customer_address_2',
					'type'             => 'text',
					'name'             => esc_html__( 'Address 2', 'awebooking-form-builder' ),
					'validate'         => '',
					'_plugable'        => true,
				],
				'customer_city' => [
					'id'               => 'customer_city',
					'type'             => 'text',
					'name'             => esc_html__( 'City', 'awebooking-form-builder' ),
					'validate'         => '',
					'_plugable'        => true,
				],
				'customer_state' => [
					'id'               => 'customer_state',
					'type'             => 'text',
					'name'             => esc_html__( 'State', 'awebooking-form-builder' ),
					'validate'         => '',
					'_plugable'        => true,
				],
				'customer_postal_code' => [
					'id'               => 'customer_postal_code',
					'type'             => 'text',
					'name'             => esc_html__( 'Postal Code', 'awebooking-form-builder' ),
					'validate'         => '',
					'_plugable'        => true,
				],
				'customer_country' => [
					'id'               => 'customer_country',
					'type'             => 'text',
					'name'             => esc_html__( 'Country', 'awebooking-form-builder' ),
					'validate'         => '',
					'_plugable'        => true,
				],
				'customer_company' => [
					'id'               => 'customer_company',
					'type'             => 'text',
					'name'             => esc_html__( 'Company', 'awebooking-form-builder' ),
					'validate'         => '',
					'_plugable'        => true,
				],
				'customer_note' => [
					'id'               => 'customer_note',
					'type'             => 'textarea',
					'name'             => esc_html__( 'Note', 'awebooking-form-builder' ),
					'validate'         => '',
					'_plugable'        => true,
				],
			]
		);
	}

	public function get_supported_types() {
		if ( is_null( $this->supported_types ) ) {
			$this->supported_types = new Collection(
				[
					'text' => [
						'type'     => 'text',
						'label'    => esc_html__( 'Text', 'awebooking-form-builder' ),
					],
					'textarea' => [
						'type'     => 'textarea',
						'label'    => esc_html__( 'Textarea', 'awebooking-form-builder' ),
					],
					'radio' => [
						'type'     => 'radio',
						'label'    => esc_html__( 'Radio', 'awebooking-form-builder' ),
					],
					'checkbox' => [
						'type'     => 'checkbox',
						'label'    => esc_html__( 'Checkbox', 'awebooking-form-builder' ),
					],
					'multicheck' => [
						'type'     => 'multicheck',
						'label'    => esc_html__( 'Multi Checkbox', 'awebooking-form-builder' ),
					],
					'select' => [
						'type'     => 'select',
						'label'    => esc_html__( 'Select', 'awebooking-form-builder' ),
					],
					'date' => [
						'type'     => 'text_date',
						'label'    => esc_html__( 'Date Picker', 'awebooking-form-builder' ),
						// date_format
					],
					'time' => [
						'type'     => 'text_time',
						'label'    => esc_html__( 'Time Picker', 'awebooking-form-builder' ),
						// time_format
					],
					'datetime' => [
						'type'     => 'text_datetime_timestamp',
						'label'    => esc_html__( 'DateTime Picker', 'awebooking-form-builder' ),
					],
					'timezone' => [
						'type'     => 'select_timezone',
						'label'    => esc_html__( 'Timezone', 'awebooking-form-builder' ),
					],
					'datetime_timezone' => [
						'type'     => 'text_datetime_timestamp_timezone',
						'label'    => esc_html__( 'DateTime and Timezone', 'awebooking-form-builder' ),
					],
				]
			);
		} // End if().

		return $this->supported_types;
	}

	public function get_default_control_props() {
		return [
			'id'         => '',
			'type'       => 'text',
			'name'       => '',
			'desc'       => '',
			'default'    => '',
			'options'    => [],
			'attributes' => [],
			'validate'   => '',
		];
	}

	public function _register_admin_scripts() {
		$current_screen = get_current_screen();
		$awebooking_screen_id = sanitize_title( esc_html__( 'AweBooking', 'awebooking' ) );

		wp_register_style( 'awebooking-form-builder', $this->get_dir_url() . 'assets/css/awebooking-checkout-form.css', [], $this->get_version() );
		wp_register_script( 'awebooking-form-builder', $this->get_dir_url() . 'assets/js/awebooking-form-builder.js', [ 'awebooking-admin' ], $this->get_version(), true );

		if ( $awebooking_screen_id . '_page_awebooking-form-builder' === $current_screen->id ) {
			wp_localize_script(
				'awebooking-form-builder', '_awebookingCheckoutControls', [
					'controls'            => $this->get_controls()->to_array(),
					'supportTypes'        => $this->get_supported_types()->to_array(),
					'coreFields'          => $this->get_core_controls()->to_array(),
					'defaultControlProps' => $this->get_default_control_props(),
				]
			);

			wp_enqueue_style( 'awebooking-form-builder' );
			wp_enqueue_script( 'awebooking-form-builder' );
		}
	}

	public function _output_checkout_form() {
		$controls = $this->controls;

		// Register form builder with flatten controls (without row and columns).
		$builder_form = new Form_Builder(
			$this->controls->flatten( 1 )->to_array()
		);

		?>

		<style type="text/css">
			.awebooking-checkout-row {
				display: flex;
				flex-wrap: wrap;

				margin-left: -15px;
				margin-right: -15px;
			}

			.awebooking-checkout-column {
				flex-basis: 0;
				flex-grow: 1;

				max-width: 100%;
				min-height: 1px;
				position: relative;

				padding-left: 15px;
				padding-right: 15px;
			}
		</style>

		<form id="awebooking-checkout-form" class="awebooking-checkout-form" method="POST">
			<?php wp_nonce_field( 'awebooking-checkout-nonce' ); ?>
			<input type="hidden" name="awebooking-action" value="checkout">

			<div class="awebooking-checkout-row">
			<?php foreach ( $controls as $row => $column ) :
				if ( empty( $column ) ) {
					continue;
				}
				?><div class="awebooking-checkout-column">
					<?php foreach ( $column as $control ) : ?>
						<?php $builder_form->get_field( $control['id'] )->display(); ?>
					<?php endforeach ?>
				</div>
			<?php endforeach ?>
			</div>

			<?php // echo $builder_form->output(); ?>

			<?php do_action( 'awebooking/checkout/before_submit_form' ); ?>

			<button type="submit" class="button" data-type="awebooking"><?php esc_html_e( 'Submit', 'awebooking-form-builder' ); ?></button>
		</form>

		<?php

		\CMB2_hookup::enqueue_cmb_js();
	}

	public function _checkout_validator_rules( $rules ) {
		$controls_rules = $this->controls->flatten( 1 )->pluck( 'validate', 'id' );

		$rules = array_merge( $rules, $controls_rules->toArray() );

		return array_filter( $rules );
	}

	public function _checkout_validator_labels( $labels ) {
		$controls_labels = $this->controls->flatten( 1 )->pluck( 'name', 'id' );
		return array_unique( array_merge( $labels, $controls_labels->toArray() ) );
	}

	public function _register_metabox_fields( $metabox ) {
		$section = $metabox->add_section(
			'custom_form', [
				'title' => esc_html__( 'Extra', 'awebooking-form-builder' ),
			]
		);

		foreach ( $this->controls->flatten( 1 ) as $control ) {
			$field = new Field_Builder( $control );

			if ( ! $field->is_core_field() ) {
				$args = $field->get_field_args();
				$args['id'] = '_booking_extra[' . $args['id'] . ']';

				$section->add_field( $args );
			}
		}
	}

	public function _add_booking_meta( $booking ) {
		$fields = $this->controls->flatten( 1 );

		$ignore_controls = [ 'customer_last_name', 'customer_first_name', 'customer_phone', 'customer_email' ];
		$form_builder = new Form_Builder( $fields->to_array() );

		$sanitized = $form_builder->get_sanitized();

		$insert_data = [];
		foreach ( $fields as $field ) {
			if ( in_array( $field['id'], $ignore_controls ) ) {
				continue;
			}

			if ( ! isset( $sanitized[ $field['id'] ] ) ) {
				continue;
			}

			$insert_data[ $field['id'] ] = $sanitized[ $field['id'] ];
		}

		$booking->update_meta( '_booking_extra', $insert_data );
	}

	public function _add_email_fields( $replacements, $booking, $notification ) {
		$meta = $booking->get_meta( '_booking_extra' );

		foreach ( $this->controls->flatten( 1 ) as $control ) {
			$field = new Field_Builder( $control );

			if ( ! $field->is_core_field() ) {
				$args = $field->get_field_args();
				$meta_key = '[' . $args['id'] . ']';
				$key = 'customer_field ' . esc_attr( $args['id'] );
				$value = ( isset( $meta[ $args['id'] ] ) && $meta[ $args['id'] ] ) ? $meta[ $args['id'] ] : '';
				$replacements[ $key ] = $value;
			}
		}

		return $replacements;
	}

	public function _add_email_notes( $contents ) {
		$shortcodes = [];
		foreach ( $this->controls->flatten( 1 ) as $control ) {
			$field = new Field_Builder( $control );

			if ( ! $field->is_core_field() ) {
				$args = $field->get_field_args();
				$code = 'customer_field ' . esc_attr( $args['id'] );

				$shortcodes[ $code ] = sprintf( esc_html__( 'The customer %s' ) , esc_html( $args['name'] ) );
			}
		}

		if ( $shortcodes ) {
			foreach ( $shortcodes as $key => $value ) {
				$contents .= '<tr><td><code>{' . esc_attr( $key ) . '}</code></td><td>' . esc_html( $value ) . '</td></tr>';
			}
		}

		return $contents;
	}
}
