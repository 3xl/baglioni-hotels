import App from './App'

new TheAweBooking.Vue({
  el: '#awebooking-checkout-form-builder',
  template: '<App/>',
  components: { App }
});
