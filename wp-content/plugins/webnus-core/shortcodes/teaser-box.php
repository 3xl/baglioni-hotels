<?php
function hotella_webnus_teaserbox ($atts, $content = null) {
 	extract(shortcode_atts(array(
		'type'			=> '1',
		'img'			=> '',
		'title'			=> '',
		'subtitle'		=> '',
		'link_url'		=> '#',
		'img_alt'		=> '',
		'text_content'	=> '',
		'border'		=> '',
		'featured'		=> '',
		'border_color'	=> '#00b1cd',
	), $atts));

	$border_color = ( $border_color ) ? $border_color : '#00b1cd';
	$style_border = ( $border == 'true' ) ? 'style = "background-color: '.$border_color.';"' : '' ;

	if(is_numeric($img))
		$img = wp_get_attachment_url( $img );
	$out = '';
	$out .= '<div class="teaser-box'.$type.'">';
	$has_image = $teaser_image = '';
	if ((($type==4)OR($type==5))&&(empty($subtitle))){
		$subtitle = $title;
	}
	if (($type==6)&&(empty($subtitle))){
		$subtitle = esc_html__('understand more','webnus-core');
	}
	if($img){
		$has_image = 'has-image';
		$teaser_image ='<img class="teaser-image " src="'. $img .'" alt="' . $img_alt . '">';
	}
	if ( !empty($link_url) )
		$out .= '<a href="'.$link_url.'">';
	$out .= $teaser_image;
	if ( !empty($featured) )
		$out .= '<h6 class="teaser-featured">'.$featured.'</h4>';
	$out .= '<h4 class="teaser-title '.$has_image.'">'.$title.'</h4>';
	if ( $border == 'true' )
		$out .= '<div class="border-bottom" '.$style_border.'></div>';
	if (!empty($subtitle))
		$out .= '<h5 class="teaser-subtitle">'.$subtitle.'</h5>';
	if ( !empty($link_url) )
		$out.= '</a>';
	if ( !empty($text_content) && $type == '8' )
		$out.= '<p>'.$text_content.'</p>';
	$out .= '</div>';
	return $out;
}
 add_shortcode('teaserbox','hotella_webnus_teaserbox');
?>