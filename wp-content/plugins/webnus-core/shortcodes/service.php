<?php
function hotella_webnus_service ( $atts, $content = null ) {
	extract(shortcode_atts(array(
		'service_single_title'		=> '',
		'bgcolor'					=> '',
		'service_single_content'	=> '',
		'icon_name'					=> '',
	), $atts ));

		$service_single_title = ( $service_single_title ) ? '<h3>' . $service_single_title . '</h3>' : '' ;
		$background = ( $bgcolor ) ? 'style="background-color:'.$bgcolor.'"' : '' ;
		$service_single_content = wpb_js_remove_wpautop($service_single_content, true);
		$service_main_content = ( $service_single_content ) ? '<div class="extra-content">' . $service_single_content . ' </div>' : '';
		$out = '
			<div class="suite-toggle" ' . $background . ' >
				<div class="main-content">
					'. $service_single_title . '
					<div class="service-icon">
						<i class="'.$icon_name.'"></i>
					</div>
				</div>
				<div class="toggle-content">
					' . $service_main_content . '
					<span><i class="ti-plus" style="color:'.$bgcolor.'"></i></span>
				</div>
			</div>
		';

	return $out;

}
	add_shortcode( 'service','hotella_webnus_service' );