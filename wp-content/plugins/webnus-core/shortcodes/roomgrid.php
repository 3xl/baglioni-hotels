<?php
function hotella_room_grid( $atts, $content = null ) {
	extract(shortcode_atts(array(
		'room_grid'		=> '',
		'type'			=> '1',
		'item_num'		=> '4',
		'post_count'	=> '6',
	), $atts));

	ob_start();

	if ( class_exists( 'AweBooking' ) ) :

		// Query
		$room_grid = $room_grid ? get_query_var( is_front_page() ? 'page' : 'paged' ) : '1';
		$room_grid_args = array(
			'post_type'			=> 'room_type',
			'posts_per_page'	=> $post_count,
			'paged'				=> $room_grid,
		);
		$room_grid_query = new WP_Query( $room_grid_args );

		if( $room_grid_query->have_posts() ) : ?>

			<div class="row">
				<div class="wrap-room-grid">

				<?php while( $room_grid_query->have_posts() ) : $room_grid_query->the_post();


					// variables
					$rating_out			= '';
					$currency			= get_post_meta( get_the_id(), 'base_price', true );
					$get_offer			= get_post_meta( get_the_id(), 'extra_sale', true );
					$discount_day		= isset( $get_offer[0]['type_duration'] ) ? $get_offer[0]['type_duration'] : '' ;
					$unit				= isset( $get_offer[0]['total'] ) ? $get_offer[0]['total'] : '' ;
					$discount_amount	= isset( $get_offer[0]['amount'] ) ? $get_offer[0]['amount'] : '' ;
					$discount_type		= isset( $get_offer[0]['sale_type'] ) ? $get_offer[0]['sale_type'] : '' ;
					$room_excerpt		= rwmb_meta( 'hotella_room_excerpt');
					
					$discount_day = $discount_day == 'Before-Day' ? esc_html__( 'Before', 'webnus-core' ) . ' ' . $unit . ' ' . esc_html__( 'Day(S)', 'webnus-core' ) : $unit . ' ' .$discount_day ;
					$discount_type = ( $discount_type == 'sub' ) ?  '-' . $discount_amount . esc_html__( '$', 'webnus-core' ) : '-' . $discount_amount.'%' ;
					// rating
					include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
					$wn_rating = 'wp-postratings/wp-postratings.php';
					if ( is_plugin_active( $wn_rating ) ) {
						if ( $type == 1 ) {
							$rating_out = function_exists( 'the_ratings' ) ? expand_ratings_template('<div class="modern-rating"><span class="rating">%RATINGS_IMAGES%</span></div>', get_the_ID()) : '';
						} elseif ( $type == 2 ) {
							$rating_out = function_exists( 'the_ratings' ) ? expand_ratings_template('<div class="modern-rating"><span class="rating">%RATINGS_IMAGES%</span><span class="rating-ave">(%RATINGS_USERS%' . __('votes', 'wp-postratings') .')</span></div>', get_the_ID()) : '';
						}
					}
					?>

					<article class="room-grid-item rg-<?php echo $type; ?> col-sm-<?php echo $item_num ;?>">
						<figure class="room-grid-item-figure">
							<?php get_the_image( array( 'meta_key' => array( 'thumbnail', 'thumbnail' ), 'size' => 'hotella_webnus_blog2_thumb' ) );
							if ( $discount_day && $discount_amount && $unit )
							echo '<span class="grid-offer"> ' . $discount_day . ' ' . $discount_type . ' </span>';
							?>
							<div class="entry-roomgrid-content">
								<?php
								the_title( sprintf( '<h2 class="entry-roomgrid-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' );

								if ( $type == '1' || $type == '2' )
									 echo $rating_out;

								if ( $type == '2' ) {
									awebooking_template_single_price();
								} ?>

								<p class="entry-roomgrid-ex"><?php echo $room_excerpt; ?></p>
								<a class="readmore" href="<?php the_permalink();?>"><?php esc_html_e( 'full details', 'webnus-core' )?></a>

								<?php if ( $type == '1' ) {
									awebooking_template_single_price();
								} ?>
							</div>
						</figure>
					</article>

				<?php endwhile; ?>

				</div> <!-- end .wrap-room-grid -->
			</div> <!-- end .row -->

			<?php if ( $room_grid != 1 ) : ?>
				<div class="room-grid-pagin pagination">
					<div class="row">
						<div class="col-sm-12">
							<?php if( function_exists( 'wp_pagenavi' ) )
								wp_pagenavi( array( 'query' => $room_grid_query ) );?>
						</div>
					</div>
				</div>
			<?php endif;

		endif;  // end $room_grid_query->have_posts()

		// Reset Query
		wp_reset_postdata();

		// Output
		$out = ob_get_contents();
		ob_end_clean();
		return $out;

	endif; // check install plugin

	if ( class_exists( 'APB_Booking' ) ) :

		// Query
		$room_grid = $room_grid ? get_query_var( is_front_page() ? 'page' : 'paged' ) : '1';
		$room_grid_args = array(
			'post_type'			=> 'apb_room_type',
			'posts_per_page'	=> $post_count,
			'paged'				=> $room_grid,
		);
		$room_grid_query = new WP_Query( $room_grid_args );

		if( $room_grid_query->have_posts() ) : ?>

			<div class="row">
				<div class="wrap-room-grid">

				<?php while( $room_grid_query->have_posts() ) : $room_grid_query->the_post();


					// variables
					$currency			= get_post_meta( get_the_id(), 'base_price', true );
					$get_offer			= get_post_meta( get_the_id(), 'extra_sale', true );
					$discount_day		= isset( $get_offer[0]['type_duration'] ) ? $get_offer[0]['type_duration'] : '' ;
					$unit				= isset( $get_offer[0]['total'] ) ? $get_offer[0]['total'] : '' ;
					$discount_amount	= isset( $get_offer[0]['amount'] ) ? $get_offer[0]['amount'] : '' ;
					$discount_type		= isset( $get_offer[0]['sale_type'] ) ? $get_offer[0]['sale_type'] : '' ;
					
					$discount_day = $discount_day == 'Before-Day' ? esc_html__( 'Before', 'webnus-core' ) . ' ' . $unit . ' ' . esc_html__( 'Day(S)', 'webnus-core' ) : $unit . ' ' .$discount_day ;
					$discount_type = ( $discount_type == 'sub' ) ?  '-' . $discount_amount . esc_html__( '$', 'webnus-core' ) : '-' . $discount_amount.'%' ;
					// rating
					include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
					$wn_rating = 'wp-postratings/wp-postratings.php';
					if ( is_plugin_active( $wn_rating ) ) {
						if ( $type == 1 ) {
							$rating_out = function_exists( 'the_ratings' ) ? expand_ratings_template('<div class="modern-rating"><span class="rating">%RATINGS_IMAGES%</span></div>', get_the_ID()) : '';
						} elseif ( $type == 2 ) {
							$rating_out = function_exists( 'the_ratings' ) ? expand_ratings_template('<div class="modern-rating"><span class="rating">%RATINGS_IMAGES%</span><span class="rating-ave">(%RATINGS_USERS%' . __('votes', 'wp-postratings') .')</span></div>', get_the_ID()) : '';
						}
					}
					?>

					<article class="room-grid-item rg-<?php echo $type; ?> col-sm-<?php echo $item_num ;?>">
						<figure class="room-grid-item-figure">
							<?php get_the_image( array( 'meta_key' => array( 'thumbnail', 'thumbnail' ), 'size' => 'hotella_webnus_blog2_thumb' ) );
							if ( $discount_day && $discount_amount && $unit )
							echo '<span class="grid-offer"> ' . $discount_day . ' ' . $discount_type . ' </span>';
							?>
							<div class="entry-roomgrid-content">
								<?php
								the_title( sprintf( '<h2 class="entry-roomgrid-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' );

								if ( $type == '1' || $type == '2' )
									 echo $rating_out;

								if ( $type == '2' ) {

									echo '<div class="room-price">
											<p class="from-night">' . esc_html__( 'from', 'webnus-core' ) . '</p>';
											loop_price_single( get_post_meta( get_the_ID(), 'base_price', true ) ) .
										'</div>';
								} ?>

								<p class="entry-roomgrid-ex"><?php echo hotella_webnus_excerpt( 20 ); ?></p>
								<a class="readmore" href="<?php the_permalink();?>"><?php esc_html_e( 'full details', 'webnus-core' )?></a>

								<?php if ( $type == '1' ) {
									echo '<div class="room-price">
											<p class="from-night">' . esc_html__( 'from', 'webnus-core' ) . '</p>';
											loop_price_single( get_post_meta( get_the_ID(), 'base_price', true ) ) .
										'</div>';
								} ?>
							</div>
						</figure>
					</article>

				<?php endwhile; ?>

				</div> <!-- end .wrap-room-grid -->
			</div> <!-- end .row -->

			<?php if ( $room_grid != 1 ) : ?>
				<div class="room-grid-pagin pagination">
					<div class="row">
						<div class="col-sm-12">
							<?php if( function_exists( 'wp_pagenavi' ) )
								wp_pagenavi( array( 'query' => $room_grid_query ) );?>
						</div>
					</div>
				</div>
			<?php endif;

		endif;  // end $room_grid_query->have_posts()

		// Reset Query
		wp_reset_postdata();

		// Output
		$out = ob_get_contents();
		ob_end_clean();
		return $out;

	endif; // check install plugin

}

add_shortcode('room-grid', 'hotella_room_grid');