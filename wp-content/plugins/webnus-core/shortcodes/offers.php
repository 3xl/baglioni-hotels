<?php
function hotella_webnus_offers ( $atts, $content = null ) {
	extract(shortcode_atts(array(
		'offers_subtitle'	=> '',
		'offers_title'		=> '',
		'background_image'	=> '',
		'bgcolor'			=> '',
		'open'				=> 'true',
		'offers_content'	=> '',
		'icon_name'			=> '',
		'min_height'		=> '',
	), $atts ));

		$min_height				= ( $min_height ) ? ' min-height:'. $min_height .'px;' : '' ;
		$open					= ( $open ) ? 'true' : 'false';
		$hide_content			= ( $open == "true" ) ? '' : 'w-hide' ;
		$plus_minus				= ( $open == "true" ) ? 'minus' : 'plus' ;
		$icon					= ( $icon_name ) ? '<div class="offer-icon"><i class="'. $icon_name .'"></i></div>' : '' ;
		$offers_subtitle 		= ( $offers_subtitle ) ? '<h4>' . $offers_subtitle . '</h4>' : '' ;
		$offers_title 	 		= ( $offers_title ) ? '<h3>' . $offers_title . '</h3>' : '' ;
		$background_color 		= ( $bgcolor ) ? ' background-color:' . $bgcolor . ';' : '' ;
		$background_image_url 	= ( $background_image ) ? wp_get_attachment_url( $background_image ) : '' ;
		$background_image 	 	= ( $background_image_url ) ? "background: url('{$background_image_url}') no-repeat center center; background-size: cover;" : '' ;
		$background 			= ( $background_image_url || $background_color ) ? 'style="' . $min_height . $background_color . $background_image . '"'  : '' ;
		$plus_icon 				= ( $offers_content ) ? '<span class="toogle-plus"><i class="ti-'. $plus_minus .'"></i></span>' : '' ;
		$offers_content 		= ( $offers_content ) ? wpb_js_remove_wpautop($offers_content, true) : '' ;
		$offers_main_content	= ( $offers_content ) ? '<div class="extra-content">' . $offers_content . '</div>' : '';
		$out = '
			<div class="offer-toggle" ' . $background . ' >
				<figure>
					<div class="main-content">
						' . $icon . '
						' . $offers_subtitle . '
						' . $offers_title . '
						' . $plus_icon . '
						<div class="toggle-content  '. $hide_content . '">
							' . $offers_main_content . '
						</div>
					</div>
				</figure>
			</div>
		';

	return $out;

}
add_shortcode( 'offers','hotella_webnus_offers' );