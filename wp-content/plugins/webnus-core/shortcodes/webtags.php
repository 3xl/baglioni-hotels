<?php

 // Paragraph
 function hotella_webnus_paragraph ($atts, $content = null) {
	extract(shortcode_atts(array(
		'class'      => ''
	), $atts));
 	return '<p class="'. $class .'">' . do_shortcode($content) . '</p>';
 }
 add_shortcode('p','hotella_webnus_paragraph');
 
 
function hotella_webnus_maxone_paragraph ($atts, $content = null) {
	extract(shortcode_atts(array(
		'class'      => ''
	), $atts));
 	return '<p class="max-p">' . do_shortcode($content) . '</p>';
 }
 add_shortcode('max-p','hotella_webnus_maxone_paragraph');


 // Link (magicmore)
function  hotella_webnus_magiclink_shortcode($attributes, $content = null)
{

	extract(shortcode_atts(array(
	"url" => '#',
		), $attributes));

	return '<a class="magicmore" href="'. esc_url($url) .'">'. do_shortcode($content) . '</a>';
}
add_shortcode("link", 'hotella_webnus_magiclink_shortcode');

 // BoxLink (magiclink)
function  hotella_webnus_boxlink_shortcode($attributes, $content = null)
{

	extract(shortcode_atts(array(
	"url" => '#',
	"boxlink_content" => '',
		), $attributes));

	return '<div class="magic-link"><a href="'. esc_url($url) .'">'. $boxlink_content . '</a></div>';
}
add_shortcode("boxlink", 'hotella_webnus_boxlink_shortcode');



 // Lists (ul li)
 function hotella_webnus_ul( $atts, $content = null ) {
 	extract(shortcode_atts(array(
 	'type'      => '',

 	), $atts));
 	return '<ul class="'. $type . '" >' . do_shortcode($content) . '</ul>';
 }
 add_shortcode('list-ul', 'hotella_webnus_ul');

 function hotella_webnus_li( $atts, $content = null ) {
 	extract(shortcode_atts(array(
 	'type'      => '',

 	), $atts));
	return '<li class="'. $type .'">' . do_shortcode($content) . '</li>';
 }
 add_shortcode('li-row', 'hotella_webnus_li');

 

  // Center
 function hotella_webnus_center( $atts, $content = null ) {
 	
	return '<div class="aligncenter">' . do_shortcode($content) . '</div>';
 }
 add_shortcode('center', 'hotella_webnus_center');


  // Span
 function hotella_webnus_span( $atts, $content = null ) {
 	
	return '<span>' . do_shortcode($content) . '</span>';
 }
 add_shortcode('span', 'hotella_webnus_span');


  // Row
 function hotella_webnus_row( $atts, $content = null ) {
 	
	return '<div class="row">' . do_shortcode($content) . '</div>';
 }
 add_shortcode('row', 'hotella_webnus_row');

 // Row
 function hotella_webnus_container( $atts, $content = null ) {
 	
	
	return '<section class="container">' . do_shortcode($content) . '</section>';
	
 }
 add_shortcode('container', 'hotella_webnus_container');

// Horizonal line1
 function hotella_webnus_hr1( $atts, $content = null ) {
 	return '<hr class="vertical-space1">';
 }
 add_shortcode('line1', 'hotella_webnus_hr1');
 
// Horizonal line2
 function hotella_webnus_hr2( $atts, $content = null ) {
 	return '<hr class="vertical-space2">';
 }
 add_shortcode('line2', 'hotella_webnus_hr2');
 // Clear
 function hotella_webnus_clear( $atts, $content = null ) {
 	return '<div class="clear"></div>';
 }
 add_shortcode('clear', 'hotella_webnus_clear');


 
  // Horizonal line
 function hotella_webnus_hr( $atts, $content = null ) {
 	
	extract(shortcode_atts(array(
 	'type'      => '1'
						), $atts));
	return ( $type == '1')?  '<hr>' : '<hr class="boldbx">';
	
	
 }
 add_shortcode('line', 'hotella_webnus_hr');

 
 // Horizonal line
 function hotella_webnus_thickline( $atts, $content = null ) {
 	return '<hr class="boldbx">';
 }
 add_shortcode('tline', 'hotella_webnus_thickline');


 // Maxone line
 function hotella_webnus_maxline( $atts, $content = null ) {
 	return '<span class="max-line"></span>';
 }
 add_shortcode('max-line', 'hotella_webnus_maxline');
 
 
 

?>