<?php
	function hotella_single_room( $atts, $content = null ) {
		extract(shortcode_atts(array(
			'room_id'				=> '',
			'room_extra_content'	=> '',
			'bgcolor'				=> '',
		), $atts));
	ob_start();


	// Query
	if ( class_exists( 'APB_Booking' ) ) {

		$args = array(
			'post_type' => 'apb_room_type',
			'posts_per_page' => 1,
			'p'	=> $room_id,
		);

	} elseif ( class_exists( 'AweBooking' ) ) {
		$args = array(
			'post_type' 		=> 'room_type',
			'posts_per_page' 	=> 1,
			'p'					=> $room_id,
		);
	} else {
		$args = '';
	}
	
	$query = new WP_Query($args);

	while ($query -> have_posts()) : $query -> the_post();
	$post_id = get_the_ID();
	$title = get_the_title();
	$rating_out = function_exists( 'the_ratings' ) ? expand_ratings_template('<div class="modern-rating"><span class="rating">%RATINGS_IMAGES%</span></div>', get_the_ID()) : '';
	$background = ( $bgcolor ) ? 'style="background-color:'.$bgcolor.'"' : '' ;
	$content = wpb_js_remove_wpautop($room_extra_content, true);
	$room_content = ( $content ) ? '<div class="extra-content">' . $content . ' </div>' : '';
	$from = esc_html__( 'From' , 'webnus-core' );
	echo '
		<div class="suite-toggle" ' . $background . ' >
			<div class="main-content">
				<h3>' . $title . '</h3>
				' . $rating_out . '
				<div class="price">
					<span>' . $from . '</span>';
					if ( class_exists( 'APB_Booking' ) ) {
						loop_price_single( get_post_meta( get_the_ID(), 'base_price', true ) );
					} elseif ( class_exists( 'AweBooking' ) ) {
						awebooking_template_single_price();
					}
	echo '		</div>
			</div>
			<div class="toggle-content">
				' . $room_content . '
				<span><i class="ti-plus" style="color:'.$bgcolor.'"></i></span>
			</div>
		</div>';
	endwhile;
	wp_reset_postdata();

	// Output
	$out = ob_get_contents();
	ob_end_clean();
	$out = str_replace('<p></p>','',$out);
	return $out;

}
	add_shortcode('single_room', 'hotella_single_room');