<?php
namespace AweBooking\Payment;

use AweBooking\AweBooking;
use InvalidArgumentException;

class Gateway_Manager {
	/**
	 * AweBooking instance.
	 *
	 * @var AweBooking\AweBooking;
	 */
	protected $awebooking;

	/**
	 * List support gateways.
	 *
	 * @var array
	 */
	protected $support_gateways = [];

	/**
	 * Resolved gateways.
	 *
	 * @var array
	 */
	protected $resolved = [];

	/**
	 * Gateway manager.
	 *
	 * @param AweBooking $awebooking AweBooking instance.
	 */
	public function __construct( AweBooking $awebooking ) {
		$this->awebooking = $awebooking;

		$this->support_gateways = apply_filters( 'awebooking/payment/support_gateways', [
			'paypal' => Gateways\PayPal_Gateway::class,
		]);
	}

	/**
	 * Add support gateway.
	 *
	 * @param string $gateway       Gateway name.
	 * @param string $gateway_class Gateway class.
	 */
	public function add_gateway( $gateway, $gateway_class ) {
		if ( isset( $this->support_gateways[ $gateway ] ) ) {
			return false;
		}

		$this->support_gateways[ $gateway ] = $gateway_class;

		return true;
	}

	/**
	 * Resolve the gateway.
	 *
	 * @param  string $gateway The supported gateway.
	 * @return AweBooking\Payment\Gateways\Gateway|null
	 *
	 * @throws InvalidArgumentException
	 */
	public function resolve( $gateway ) {
		if ( ! isset( $this->support_gateways[ $gateway ] ) ) {
			throw new InvalidArgumentException( "The {$gateway} gateway is not supported" );
		}

		// Resolve the gateway if necessary.
		if ( ! isset( $this->resolved[ $gateway ] ) ) {
			$resolve_class = $this->support_gateways[ $gateway ];
			$this->resolved[ $gateway ] = new $resolve_class( $this->awebooking['setting'] );
		}

		return $this->resolved[ $gateway ];
	}

	/**
	 * Get list gateway supported.
	 *
	 * @return array
	 */
	public function get_supported() {
		return array_keys( $this->support_gateways );
	}

	/**
	 * Get list gateway enabled.
	 *
	 * @return array
	 */
	public function get_enabled() {
		$supported = $this->get_supported();
		$payment_methods = (array) $this->awebooking['setting']->get( 'payment_methods' );

		$return = [];
		foreach ( $supported as $method ) {
			if ( in_array( $method, $payment_methods ) ) {
				$return[] = $method;
			}
		}

		return $return;
	}
}
