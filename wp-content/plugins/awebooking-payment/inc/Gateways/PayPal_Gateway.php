<?php
namespace AweBooking\Payment\Gateways;

use Omnipay\Omnipay;

class PayPal_Gateway extends Gateway {
	/**
	 * Setup the gateway.
	 */
	public function setup() {
		$this->name = esc_html__( 'Paypal', 'awebooking-payment' );
	}

	/**
	 * Create and setup the gateway.
	 *
	 * @return Omnipay\Common\GatewayInterface
	 */
	public function create_gateway() {
		$gateway = Omnipay::create( 'PayPal_Express' );

		$gateway->setUsername( $this->setting->get( 'paypal_api_username' ) );
		$gateway->setPassword( $this->setting->get( 'paypal_api_password' ) );
		$gateway->setSignature( $this->setting->get( 'paypal_api_signature' ) );
		$gateway->setTestMode( (bool) $this->setting->get( 'paypal_sandbox', false ) );

		return $gateway;
	}

	/**
	 * Get gateway settings.
	 *
	 * @return array
	 */
	public function get_settings() {
		return [
			[
				'id'          => 'paypal_email',
				'type'        => 'text',
				'name'        => esc_html__( 'PayPal Email', 'awebooking-payment' ),
				'description' => esc_html__( 'Please enter your PayPal email address. This is needed in order to take payment.', 'awebooking-payment' ),
				'default'     => get_option( 'admin_email' ),
				'validate'    => 'email',
				'attributes'  => [
					'type'        => 'email',
					'placeholder' => 'you@youremail.com',
				],
			],
			[
				'id'          => 'paypal_logo_url',
				'type'        => 'text',
				'name'        => esc_html__( 'Logo Image URL', 'awebooking-payment' ),
				'validate'    => 'url',
			],
			[
				'id'      => 'paypal_sandbox',
				'type'    => 'toggle',
				'default' => false,
				'name'    => esc_html__( 'PayPal Sandbox', 'awebooking-payment' ),
			],
			[
				'id'     => 'paypal_api_username',
				'type'   => 'text',
				'name'   => esc_html__( 'API Username', 'awebooking-payment' ),
				// 'before_row' => 'Enter your PayPal API credentials to process refunds via PayPal. Learn how to access your PayPal API Credentials.',
			],
			[
				'id'     => 'paypal_api_password',
				'type'   => 'text',
				'name'   => esc_html__( 'API Password', 'awebooking-payment' ),
			],
			[
				'id'     => 'paypal_api_signature',
				'type'   => 'text',
				'name'   => esc_html__( 'API Signature', 'awebooking-payment' ),
			],
		];
	}
}
