<?php
namespace AweBooking\Payment\Gateways;

use AweBooking\Setting;
use Omnipay\Common\GatewayInterface;
use AweBooking\Payment\Gateways\Exceptions\GatewayException;

abstract class Gateway {
	/**
	 * Omnipay gateway instance.
	 *
	 * @var Omnipay\Common\GatewayInterface
	 */
	protected $gateway;

	/**
	 * The AweBooking config instance.
	 *
	 * @var AweBooking\Setting
	 */
	protected $setting;

	/**
	 * Name of gateway.
	 *
	 * @var string
	 */
	protected $name;

	/**
	 * Gateway description.
	 *
	 * @var string
	 */
	protected $description = '';

	/**
	 * Create new gateway.
	 *
	 * @param  Setting $setting AweBooking setting instance.
	 * @throws GatewayException
	 */
	public function __construct( Setting $setting ) {
		$this->setting  = $setting;
		$this->gateway = $this->create_gateway();

		if ( is_null( $this->gateway ) || ! $this->gateway instanceof GatewayInterface ) {
			throw new GatewayException( 'The gateway must be a instanceof Omnipay\Common\GatewayInterface' );
		}

		$this->setup();
	}

	/**
	 * Setup the gateway.
	 */
	abstract public function setup();

	/**
	 * Create and setup the gateway.
	 *
	 * @return Omnipay\Common\GatewayInterface
	 */
	abstract public function create_gateway();

	/**
	 * Get gateway settings.
	 *
	 * @return array
	 */
	abstract public function get_settings();

	/**
	 * Get gateway settings.
	 *
	 * @return Omnipay\Common\GatewayInterface
	 */
	public function get_gateway() {
		return $this->gateway;
	}

	/**
	 * Get the gatway name.
	 *
	 * @return string
	 */
	public function get_name() {
		return $this->name;
	}

	/**
	 * Get the gatway description.
	 *
	 * @return string
	 */
	public function get_description() {
		return $this->description;
	}
}
