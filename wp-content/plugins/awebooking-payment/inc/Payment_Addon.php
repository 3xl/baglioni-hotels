<?php
namespace AweBooking\Payment;

use AweBooking\Support\Addon;
use AweBooking\Booking\Booking;
use AweBooking\Admin\Admin_Settings;

class Payment_Addon extends Addon {
	/* Constants */
	const VERSION = '0.2.0';

	/**
	 * Requires minimum AweBooking version.
	 *
	 * @return string
	 */
	public function requires() {
		return '3.0.0-beta8';
	}

	/**
	 * Registers services on the awebooking.
	 *
	 * @return void
	 */
	public function register() {
		$this->awebooking->singleton( 'gateway_manager', function( $awebooking ) {
			return new Gateway_Manager( $awebooking );
		});

		load_plugin_textdomain( 'awebooking-payment', false, dirname( $this->get_basename() ) . '/languages' );
	}

	/**
	 * Init the addon.
	 *
	 * @return void
	 */
	public function init() {
		add_action( 'template_redirect', [ $this, 'handler_complete_booking' ] );
		add_action( 'template_redirect', [ $this, 'cancel_booking' ] );

		add_action( 'awebooking/checkout_completed', [ $this, 'handler_booking_payment' ] );
		add_action( 'awebooking/checkout/before_submit_form', [ $this, 'show_payment_html' ] );
		add_action( 'awebooking/register_admin_settings', [ $this, 'register_admin_settings' ] );
	}

	public function handler_booking_payment( $booking ) {
		if ( isset( $_POST['payment_method'] ) && 'paypal' === $_POST['payment_method'] ) {
			$gateway = awebooking( 'gateway_manager' )->resolve( 'paypal' );

			$page_booking = get_home_url();
			$cancel_url = add_query_arg( array(
				'cancel_booking' => 'true',
				'booking_id'     => $booking->get_id(),
				'_wpnonce'       => wp_create_nonce( 'cancel-booking' ),
			), $page_booking );

			$booking['payment_method'] = 'paypal';
			$booking->calculate_totals();

			try {
				$request = $gateway->get_gateway()->purchase([
					'amount'      => $booking['total'],
					'currency'    => $booking->get_currency()->get_code(),
					'description' => 'Booking Room',
					'returnUrl'   => get_home_url() . '?complete-purchased=true&booking_id=' . $booking->get_id(),
					'cancelUrl'   => $cancel_url,
					'logoImageUrl' => awebooking_option( 'paypal_logo_url' ),
				]);

				$response = $request->send();

				if ( $response->isRedirect() ) {
					return $response->redirect();
				}
			} catch ( \Exception $e ) {
				wp_die( 'An error occurred while processing your request.' );
			}

			wp_die( 'Some errors was happend!' );
		}
	}

	public function handler_complete_booking() {
		if ( ! empty( $_REQUEST['complete-purchased'] ) && ! empty( $_REQUEST['booking_id'] ) ) {
			$booking = new Booking( absint( $_REQUEST['booking_id'] ) );
			if ( ! $booking->exists() ) {
				return;
			}

			$gateway = awebooking( 'gateway_manager' )->resolve( $booking->get_payment_method() );
			if ( ! $gateway ) {
				return;
			}

			try {
				$request = $gateway->get_gateway()->completePurchase([
					'amount'   => $booking->get_total()->get_amount(),
					'currency' => $booking->get_currency(),
				]);

				$response = $request->send();

			} catch ( \Exception $e ) {
				wp_die( 'An error occurred while processing your request.' );
			}

			if ( $response->isSuccessful() ) {
				$transaction_id = $response->getTransactionReference();

				$booking['payment_method_title'] = $gateway->get_name();
				$booking['transaction_id'] = $transaction_id;
				$booking['status'] = Booking::COMPLETED;
				$booking->save();

				// Clear booking request and set booking ID.
				awebooking_setcookie( 'awebooking-booking-id', $booking->get_id(), time() + 60 * 60 * 24 );

				$checkout_url  = get_the_permalink( absint( awebooking_option( 'page_checkout' ) ) );

				return wp_redirect( add_query_arg( [ 'step' => 'complete' ], $checkout_url ) );
			} else {
				// Update un-success...
			}
		} // End if().
	}

	/**
	 * Cancel a pending order.
	 */
	public static function cancel_booking() {
		if ( isset( $_GET['cancel_booking'] ) && isset( $_GET['booking_id'] ) ) {
			$booking = new Booking( absint( $_REQUEST['booking_id'] ) );
			if ( ! $booking->exists() ) {
				return;
			}

			// Already cancelled - take no action.
			if ( $booking->has_status( 'cancelled' ) ) {
				return;
			}

			$booking_can_cancel = $booking->has_status( array( Booking::PENDING, Booking::FAILED ) );
			if ( $booking_can_cancel ) {
				$booking['status'] = Booking::CANCELLED;
				$booking->save();

				awebooking( 'flash_message' )->warning(
					esc_html__( 'Your booking was cancelled.', 'awebooking-payment' )
				);

				do_action( 'awebooking_cancelled_booking', $booking );

				$checkout_url  = get_the_permalink( absint( awebooking_option( 'page_checkout' ) ) );
				return wp_redirect( add_query_arg( [ 'step' => 'cancelled' ], $checkout_url ) );
			}
		}
	}

	public function show_payment_html( $availability ) {
		$gateway_manager = awebooking( 'gateway_manager' );

		$payment_methods = (array) awebooking( 'config' )->get( 'payment_methods', [ 'offline' ] );

		?>
		<div style="margin-bottom: 15px;">
			<?php if ( in_array( 'offline', $payment_methods ) ) : ?>
				<label for="payment_method_offline">
					<input type="radio" name="payment_method" value="offline" id="payment_method_offline" checked="">
					<span><?php echo esc_html__( 'Offline payment', 'awebooking-payment' ); ?></span>
				</label>
			<?php endif ?>

			<?php foreach ( $gateway_manager->get_enabled() as $gateway_id ) :
				$gateway = $gateway_manager->resolve( $gateway_id ); ?>

				<label for="payment_method_paypal" style="display: block;">
					<input type="radio" name="payment_method" value="paypal" id="payment_method_paypal" checked="">
					<span><?php echo $gateway->get_name(); ?></span>
				</label>
			<?php endforeach ?>
		</div>
		<?php
	}

	/**
	 * Loop each gateways and call the admin settings method.
	 *
	 * @param  Admin_Settings $admin_settings Admin Setting instance.
	 * @return void
	 */
	public function register_admin_settings( Admin_Settings $admin_settings ) {
		$manager = $this->awebooking['gateway_manager'];

		$section = $admin_settings->add_section( 'payment', [
			'title' => esc_html__( 'Payment', 'awebooking-payment' ),
		]);

		$general = $section->add_field( array(
			'id'   => '__payment_title__',
			'type' => 'title',
			'name' => esc_html__( 'General', 'awebooking-payment' ),
		) );

		$general = $section->add_field( array(
			'id'   => 'payment_methods',
			'type' => 'multicheck',
			'name' => esc_html__( 'Payment methods', 'awebooking-payment' ),
			'select_all_button' => false,
			'default' => [ 'offline' ],
			'options' => [
				'offline' => esc_html__( 'Offline Payment', 'awebooking-payment' ),
				'paypal'  => esc_html__( 'PayPal', 'awebooking-payment' ),
			],
		) );

		// Loop through 'support gateways' and register each settings.
		foreach ( $manager->get_supported() as $name ) {
			$gateway = $manager->resolve( $name );

			$section->add_field( array(
				'id'   => "__payment_{$name}_gateway__",
				'type' => 'title',
				'name' => $gateway->get_name(),
			) );

			foreach ( $gateway->get_settings() as $field ) {
				$section->add_field( $field );
			}
		}
	}
}
