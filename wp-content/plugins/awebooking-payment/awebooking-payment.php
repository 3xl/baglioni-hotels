<?php
/**
 * Plugin Name:     AweBooking Payment
 * Plugin URI:      http://awethemes.com/plugins/awebooking
 * Description:     Allows you to enable payments online via gateways in guest checkout.
 * Author:          awethemes
 * Author URI:      http://awethemes.com
 * Text Domain:     awebooking-payment
 * Domain Path:     /languages
 * Version:         0.2.0
 *
 * @package         AweBooking/Payment
 */

use AweBooking\Payment\Payment_Addon;

/**
 * Register the payment addon into AweBooking.
 */
add_action( 'awebooking/init', function ( AweBooking $awebooking ) {
	require trailingslashit( __DIR__ ) . 'vendor/autoload.php';

	$awebooking->register_addon( new Payment_Addon( 'awebooking-payment', __FILE__ ) );
});
