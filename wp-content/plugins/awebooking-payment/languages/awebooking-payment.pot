# Copyright (C) 2017 awethemes
# This file is distributed under the same license as the AweBooking Payment package.
msgid ""
msgstr ""
"Project-Id-Version: AweBooking Payment 0.2.0\n"
"Report-Msgid-Bugs-To: "
"https://wordpress.org/support/plugin/awebooking-payment\n"
"POT-Creation-Date: 2017-10-04 08:47:30+00:00\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"PO-Revision-Date: 2017-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: en\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Poedit-Country: United States\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-KeywordsList: "
"__;_e;_x:1,2c;_ex:1,2c;_n:1,2;_nx:1,2,4c;_n_noop:1,2;_nx_noop:1,2,3c;esc_"
"attr__;esc_html__;esc_attr_e;esc_html_e;esc_attr_x:1,2c;esc_html_x:1,2c;\n"
"X-Poedit-Basepath: ../\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-Bookmarks: \n"
"X-Textdomain-Support: yes\n"

#: inc/Gateways/PayPal_Gateway.php:11
msgid "Paypal"
msgstr ""

#: inc/Gateways/PayPal_Gateway.php:40 inc/Gateways/Stripe_Gateway.php:40
msgid "PayPal Email"
msgstr ""

#: inc/Gateways/PayPal_Gateway.php:41 inc/Gateways/Stripe_Gateway.php:41
msgid ""
"Please enter your PayPal email address. This is needed in order to take "
"payment."
msgstr ""

#: inc/Gateways/PayPal_Gateway.php:52 inc/Gateways/Stripe_Gateway.php:52
msgid "Logo Image URL"
msgstr ""

#: inc/Gateways/PayPal_Gateway.php:59 inc/Gateways/Stripe_Gateway.php:59
msgid "PayPal Sandbox"
msgstr ""

#: inc/Gateways/PayPal_Gateway.php:64 inc/Gateways/Stripe_Gateway.php:64
msgid "API Username"
msgstr ""

#: inc/Gateways/PayPal_Gateway.php:70 inc/Gateways/Stripe_Gateway.php:70
msgid "API Password"
msgstr ""

#: inc/Gateways/PayPal_Gateway.php:75 inc/Gateways/Stripe_Gateway.php:75
msgid "API Signature"
msgstr ""

#: inc/Gateways/Stripe_Gateway.php:11
msgid "Stripe"
msgstr ""

#: inc/Payment_Addon.php:150
msgid "Your booking was cancelled."
msgstr ""

#: inc/Payment_Addon.php:171
msgid "Offline payment"
msgstr ""

#: inc/Payment_Addon.php:197
msgid "Payment"
msgstr ""

#: inc/Payment_Addon.php:203
msgid "General"
msgstr ""

#: inc/Payment_Addon.php:209
msgid "Payment methods"
msgstr ""

#: inc/Payment_Addon.php:213
msgid "Offline Payment"
msgstr ""

#: inc/Payment_Addon.php:214
msgid "PayPal"
msgstr ""

#. Plugin Name of the plugin/theme
msgid "AweBooking Payment"
msgstr ""

#. Plugin URI of the plugin/theme
msgid "http://awethemes.com/plugins/awebooking"
msgstr ""

#. Description of the plugin/theme
msgid "Allows you to enable payments online via gateways in guest checkout."
msgstr ""

#. Author of the plugin/theme
msgid "awethemes"
msgstr ""

#. Author URI of the plugin/theme
msgid "http://awethemes.com"
msgstr ""