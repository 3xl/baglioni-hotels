<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit21769f745386c09205fa610860994f0f
{
    public static $prefixLengthsPsr4 = array (
        'A' => 
        array (
            'Awethemes\\Support\\' => 18,
            'AweBooking\\WooCommerce\\' => 23,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Awethemes\\Support\\' => 
        array (
            0 => __DIR__ . '/..' . '/awethemes/support/inc',
        ),
        'AweBooking\\WooCommerce\\' => 
        array (
            0 => __DIR__ . '/../..' . '/inc',
        ),
    );

    public static $classMap = array (
        'AweBooking\\WooCommerce\\Cart_Hooks' => __DIR__ . '/../..' . '/inc/Cart_Hooks.php',
        'AweBooking\\WooCommerce\\Checkout_Hooks' => __DIR__ . '/../..' . '/inc/Checkout_Hooks.php',
        'AweBooking\\WooCommerce\\Order_Hooks' => __DIR__ . '/../..' . '/inc/Order_Hooks.php',
        'AweBooking\\WooCommerce\\Product_Booking_Room' => __DIR__ . '/../..' . '/inc/Product_Booking_Room.php',
        'AweBooking\\WooCommerce\\WooCommerce_Addon' => __DIR__ . '/../..' . '/inc/WooCommerce_Addon.php',
        'Awethemes\\Support\\Plugin_Updater' => __DIR__ . '/..' . '/awethemes/support/inc/Plugin_Updater.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit21769f745386c09205fa610860994f0f::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit21769f745386c09205fa610860994f0f::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInit21769f745386c09205fa610860994f0f::$classMap;

        }, null, ClassLoader::class);
    }
}
