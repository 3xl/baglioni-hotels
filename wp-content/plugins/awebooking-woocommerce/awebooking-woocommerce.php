<?php
/**
 * Plugin Name:     AweBooking WooCommerce
 * Plugin URI:      http://awethemes.com/plugins/awebooking
 * Description:     Seamlessly connects with Woocommerce to benefit from all variety of Woocommerce extensions and payment gateways.
 * Author:          awethemes
 * Author URI:      http://awethemes.com
 * Text Domain:     awebooking-woocommerce
 * Domain Path:     /languages
 * Version:         0.3.5
 *
 * @package         AweBooking/WooCommerce
 */

use AweBooking\AweBooking;
use AweBooking\WooCommerce\WooCommerce_Addon;

/**
 * Init the WooCommerce support.
 *
 * @param  AweBooking $awebooking AweBooking instance.
 * @return void
 */
function awebooking_register_woocommerce_support( AweBooking $awebooking ) {
	require trailingslashit( __DIR__ ) . 'vendor/autoload.php';

	$awebooking->register_addon( new WooCommerce_Addon( 'awebooking-woocommerce', __FILE__ ) );
}
add_action( 'awebooking/init', 'awebooking_register_woocommerce_support' );
