<?php
namespace AweBooking\WooCommerce;

use AweBooking\Factory;
use AweBooking\Concierge;
use AweBooking\Booking\Booking;
use AweBooking\Booking\Items\Line_Item;
use AweBooking\Booking\Request;
use AweBooking\Support\Period;
use AweBooking\Support\Collection;
use AweBooking\Support\Service_Hooks;

class Order_Hooks extends Service_Hooks {
	/**
	 * Init service provider.
	 *
	 * @param AweBooking $awebooking AweBooking instance.
	 */
	public function init( $awebooking ) {
		// Complete booking orders if virtual.
		add_action( 'woocommerce_checkout_update_order_meta', [ $this, 'sync_booking' ], 20, 2 );

		// When an order is processed or completed, we can mark publish the pending bookings.
		add_action( 'woocommerce_order_status_processing', [ $this, 'publish_bookings' ], 10, 1 );
		add_action( 'woocommerce_order_status_completed', [ $this, 'publish_bookings' ], 10, 1 );
		add_action( 'woocommerce_order_status_cancelled', [ $this, 'cancel_bookings' ], 10, 1 );
		add_action( 'woocommerce_order_status_refunded', [ $this, 'cancel_bookings' ], 10, 1 );

		add_action( 'awebooking/booking/html_after_general_form', [ $this, 'display_binding_order' ] );
		add_action( 'woocommerce_admin_order_data_after_order_details', [ $this, 'display_binding_booking' ] );

		// TODO: After a booking or order deleted, remove binding key ID.
	}

	/**
	 * Called when an order is paid
	 *
	 * @param  int $order_id The order ID.
	 * @return void
	 */
	public function publish_bookings( $order_id ) {
		$order = wc_get_order( $order_id );

		$booking = $this->get_booking_from_order( $order_id );
		if ( ! $booking ) {
			return;
		}

		$payment_method = method_exists( $order, 'get_payment_method' ) ?
			$order->get_payment_method() : $order->payment_method;

		// Don't publish bookings for COD orders.
		if ( $order->has_status( 'processing' ) && 'cod' === $payment_method ) {
			return;
		}

		// Update booking status.
		$booking['status'] = $this->transform_booking_status(
			$order->get_status()
		);

		$booking->save();
	}

	/**
	 * Cancel bookings with order.
	 *
	 * @param  int $order_id The order ID.
	 * @return void
	 */
	public function cancel_bookings( $order_id ) {
		$order = wc_get_order( $order_id );

		$booking = $this->get_booking_from_order( $order_id );
		if ( ! $booking ) {
			return;
		}

		// Update booking status.
		$booking['status'] = $this->transform_booking_status(
			$order->get_status()
		);

		$booking->save();
	}

	/**
	 * Triggered after an order is updated and sync booking items.
	 *
	 * @param  WC_Order $order_id //.
	 * @param  array    $data     //.
	 * @return void
	 */
	public function sync_booking( $order_id, $data ) {
		if ( ! $this->has_booking_rooms_in_cart() ) {
			return;
		}

		$cart  = WC()->cart;
		$order = wc_get_order( $order_id );

		// Create new booking.
		$booking = (new Booking)->fill(apply_filters( 'awebooking/store_booking_args', [
			'status'                  => Booking::PENDING,
			'created_via'             => 'WooCommerce',
			'customer_id'             => $order->get_customer_id(),
			'customer_note'           => $order->get_customer_note(),
			'customer_first_name'     => $order->get_billing_first_name(),
			'customer_last_name'      => $order->get_billing_last_name(),
			'customer_address'        => $order->get_billing_address_1(),
			'customer_address_2'      => $order->get_billing_address_2(),
			'customer_city'           => $order->get_billing_city(),
			'customer_state'          => $order->get_billing_state(),
			'customer_postal_code'    => $order->get_billing_postcode(),
			'customer_country'        => $order->get_billing_country(),
			'customer_company'        => $order->get_billing_company(),
			'customer_phone'          => $order->get_billing_phone(),
			'customer_email'          => $order->get_billing_email(),
		]));

		// Save the booking.
		$booking->save();

		foreach ( $cart->get_cart() as $cart_item_key => $cart_item ) {
			// Check valid item to insert to the AweBooking bookings.
			if ( ! $this->is_booking_product_item( $cart_item ) ) {
				continue;
			}

			// Get booking data from cart item data.
			$booking_data = $cart_item['awebooking'];
			$room_type = Factory::get_room_type( $booking_data['room-type'] ); // TODO: ...

			try {
				$request = new Request(
					new Period( $booking_data['check_in'], $booking_data['check_out'], true ),
					$booking_data
				);

				// Run check availability of request.
				$availability = Concierge::check_room_type_availability( $room_type, $request );

				if ( $availability->unavailable() ) {
					continue;
				}
			} catch ( \Exception $e ) {
				continue; // Ignore if any exceptions throws.
			}

			// Take last room in list rooms available.
			$rooms = $availability->get_rooms();
			$the_room = end( $rooms );

			$options = new Collection( $booking_data );

			$room_item_options = [
				'name'      => $room_type->get_title(),
				'room_id'   => $the_room->get_id(),
				'check_in'  => $request->get_check_in()->toDateString(),
				'check_out' => $request->get_check_out()->toDateString(),
				'adults'    => $request->get_adults(),
				'total'     => $room_type->get_buyable_price( $options )->get_amount(),
			];

			if ( awebooking( 'setting' )->get_children_bookable() ) {
				$room_item_options['children'] = $request->get_children();
			}

			if ( awebooking( 'setting' )->get_infants_bookable() ) {
				$room_item_options['infants'] = $request->get_infants();
			}

			$room_item = (new Line_Item)->fill( $room_item_options );

			$booking->add_item( $room_item );
		} // End foreach().

		// Calculate the booking total.
		$booking->calculate_totals();

		// After all, bound IDs between booking and order.
		update_post_meta( $booking->get_id(), '_woocommerce_order_id', $order->get_id() );
		update_post_meta( $order->get_id(), '_awebooking_booking_id', $booking->get_id() );
	}

	/**
	 * Display WC order reference in booking.
	 *
	 * @param  Booking $booking Booking instance.
	 * @return void
	 */
	public function display_binding_order( Booking $booking ) {
		$order_id = (int) $booking->get_meta( '_woocommerce_order_id' );
		if ( ! $order_id ) {
			return;
		}

		$order = wc_get_order( $order_id );
		if ( $order ) {
			printf( '<p>%1$s <a href="%3$s">#%2$s</a></p>',
				esc_html__( 'This booking is linked to order', 'awebooking' ),
				$order->get_id(),
				get_edit_post_link( $order->get_id() )
			);
		}
	}

	/**
	 * Display booking reference in the WC order.
	 *
	 * @param  WC_Order $order WC_Order instance.
	 * @return void
	 */
	public function display_binding_booking( $order ) {
		$booking = $this->get_booking_from_order( $order );

		if ( $booking ) {
			printf( '<p class="form-field form-field-wide">%1$s <a href="%3$s">#%2$s</a></p>',
				esc_html__( 'This order is linked to booking', 'awebooking-woocommerce' ),
				$booking->get_id(),
				$booking->get_edit_url()
			);
		}
	}

	/**
	 * Gets booking form order if have.
	 *
	 * @param  int|WC_Order $order WC order ID or object instance.
	 * @return Booking|null
	 */
	protected function get_booking_from_order( $order ) {
		$order = wc_get_order( $order );
		$order_booking_id = (int) $order->get_meta( '_awebooking_booking_id' );

		// If booking reference does not exist, then do nothing.
		if ( ! $order_booking_id ) {
			return;
		}

		// Get booking reference.
		$booking = Factory::get_booking( $order_booking_id );
		if ( ! $booking->exists() ) {
			return;
		}

		return $booking;
	}

	/**
	 * Transform order status to booking status.
	 *
	 * @param  string $order_status Wc order status.
	 * @return string
	 */
	protected function transform_booking_status( $order_status ) {
		switch ( $order_status ) {
			case 'on-hold':
			case 'processing':
				return Booking::PROCESSING;
			case 'completed':
				return Booking::COMPLETED;
			case 'failed':
				return Booking::FAILED;
			case 'refunded':
			case 'cancelled':
				return Booking::CANCELLED;
			default:
				return Booking::PENDING;
		}
	}

	/**
	 * Determines given item is a booking product.
	 *
	 * @param  array $item Array of cart item.
	 * @return boolean
	 */
	protected function is_booking_product_item( $item ) {
		return ( ! empty( $item['awebooking'] ) && $item['data'] instanceof Product_Booking_Room );
	}

	/**
	 * Determines if have any booking-rooms in WC_Cart.
	 *
	 * @return boolean
	 */
	protected function has_booking_rooms_in_cart() {
		$cart_contents = WC()->cart->get_cart();

		if ( empty( $cart_contents ) ) {
			return false;
		}

		foreach ( $cart_contents as $cart_item_key => $cart_item ) {
			if ( $this->is_booking_product_item( $cart_item ) ) {
				return true;
			}
		}

		return false;
	}
}
