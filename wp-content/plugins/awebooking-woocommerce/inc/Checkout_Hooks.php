<?php
namespace AweBooking\WooCommerce;

use AweBooking\Support\Service_Hooks;

class Checkout_Hooks extends Service_Hooks {
	/**
	 * Init service provider.
	 *
	 * @param AweBooking $awebooking AweBooking instance.
	 */
	public function init( $awebooking ) {
		remove_action( 'awebooking/checkout/customer_form', 'awebooking_template_checkout_customer_form', 10 );
		add_action( 'awebooking/checkout/customer_form', [ $this, 'display_woocommerce_checkout' ], 10 );
	}

	/**
	 * Display woocommerce checkout shortcode.
	 *
	 * @return void
	 */
	public function display_woocommerce_checkout() {
		if ( ! defined( 'WOOCOMMERCE_CHECKOUT' ) ) {
			define( 'WOOCOMMERCE_CHECKOUT', true );
		}

		if ( ! is_checkout() ) {
			wp_enqueue_script( 'wc-checkout' );
		}

		if ( is_booking_checkout_page() ) {
			wp_enqueue_script( 'wc-checkout' );
		}

		echo do_shortcode( '[woocommerce_checkout]' );
	}
}
