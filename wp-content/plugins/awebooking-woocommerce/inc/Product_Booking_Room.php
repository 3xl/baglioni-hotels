<?php
namespace AweBooking\WooCommerce;

use AweBooking\Factory;
use AweBooking\Hotel\Room_Type;

class Product_Booking_Room extends \WC_Product {
	/**
	 * Booking room-type instance.
	 *
	 * @var Room_Type
	 */
	protected $room_type;

	/**
	 * Constructor.
	 *
	 * @param mixed $room_type //.
	 */
	public function __construct( $room_type = 0 ) {
		// Please do not call parent constructor, we don't need that.
		if ( is_numeric( $room_type ) && $room_type > 0 ) {
			$this->set_id( $room_type );
		} elseif ( $room_type instanceof self || $room_type instanceof Room_Type ) {
			$this->set_id( absint( $room_type->get_id() ) );
		} elseif ( ! empty( $room_type->ID ) ) {
			$this->set_id( absint( $room_type->ID ) );
		}

		// Setup room-type instance.
		if ( $this->get_id() ) {
			$this->room_type = Factory::get_room_type( $this->get_id() );
		}
	}

	/**
	 * Get internal type.
	 *
	 * @return string
	 */
	public function get_type() {
		return 'awebooking';
	}

	/**
	 * Bookings can always be purchased regardless of price.
	 *
	 * @return true
	 */
	public function is_purchasable() {
		return true;
	}

	/**
	 * This is virtual product (has no shipping).
	 *
	 * @return true
	 */
	public function is_virtual() {
		return true;
	}

	/**
	 * We want to sell bookings one at a time (no quantities).
	 *
	 * @return boolean
	 */
	public function is_sold_individually() {
		return false;
	}

	/**
	 * Returns whether or not the product post exists.
	 *
	 * @return bool
	 */
	public function exists() {
		return true;
	}

	/**
	 * Return the stock status (for this, it alway instock).
	 *
	 * @param  string $context The context, default 'view'.
	 * @return string
	 */
	public function get_stock_status( $context = 'view' ) {
		return 'instock';
	}

	/**
	 * Returns booking room type name.
	 *
	 * @param  string $context The context, default 'view'.
	 * @return string
	 */
	public function get_name( $context = 'view' ) {
		return $this->room_type ? sprintf( esc_html__( 'Room: %s', 'awebooking-woocommerce' ), $this->room_type->get_title() ) : '';
	}

	/**
	 * Get product price.
	 *
	 * @param  string $context The context, default 'view'.
	 * @return float
	 */
	public function get_price( $context = 'view' ) {
		return floatval( $this->get_prop( 'price', $context ) );
	}

	/**
	 * //
	 *
	 * @return string
	 */
	public function get_room_type() {
		return $this->room_type;
	}
}
