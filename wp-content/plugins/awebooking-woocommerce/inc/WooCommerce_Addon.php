<?php
namespace AweBooking\WooCommerce;

use AweBooking\Support\Addon;
use AweBooking\Currency\Currency;

class WooCommerce_Addon extends Addon {
	/* Constants */
	const VERSION = '0.3.5';

	/**
	 * Requires minimum AweBooking version.
	 *
	 * @return string
	 */
	public function requires() {
		return '3.0.0';
	}

	/**
	 * Registers services on the awebooking.
	 *
	 * @return void
	 */
	public function register() {
		if ( $this->requires_woocommerce_active() ) {
			// Because we can't use two different currency at same time, and we use
			// WooCommerce for payment, so forced AweBooking currency same as WooCommerce.
			$this->woocommerce_currency();

			$awebooking = $this->awebooking;
			$awebooking->trigger( new Cart_Hooks );
			$awebooking->trigger( new Order_Hooks );
			$awebooking->trigger( new Checkout_Hooks );

			load_plugin_textdomain( 'awebooking-woocommerce', false, dirname( $this->get_basename() ) . '/languages' );
		}
	}

	/**
	 * Init the addon.
	 */
	public function init() {
		add_filter( 'awebooking/price_format', [ $this, 'woocommerce_price_format' ], 10, 2 );
		add_filter( 'awebooking/number_format', [ $this, 'woocommerce_format_number' ], 10, 3 );

		add_action( 'awebooking/register_admin_settings', [ $this, 'admin_settings_fields' ] );
	}

	/**
	 * Use WooCommerce currency.
	 *
	 * @return void
	 */
	public function woocommerce_currency() {
		$this->awebooking->extend( 'currency', function() {
			$wc_currency = get_woocommerce_currency();
			$wc_currencies = get_woocommerce_currencies();

			return new Currency( $wc_currency, [
				'name'   => isset( $wc_currencies[ $wc_currency ] ) ? $wc_currencies[ $wc_currency ] : '',
				'symbol' => get_woocommerce_currency_symbol(),
			]);
		});
	}

	/**
	 * Returns formatted price using WooCommerce format.
	 *
	 * @param  string $formatted Formatted price using AweBooking format.
	 * @param  number $number    Input price number.
	 * @param  array  $args      Optional, format args.
	 * @return string
	 */
	public function woocommerce_format_number( $formatted, $number, $args ) {
		return (float) wc_format_decimal( $number );
	}

	/**
	 * Returns formatted price using WooCommerce format.
	 *
	 * @param  string $formatted Formatted price using AweBooking format.
	 * @param  number $price     Formatted price amount.
	 * @return string
	 */
	public function woocommerce_price_format( $formatted, $price ) {
		return wc_price( $price );
	}

	/**
	 * Modify admin setting fields has been conflict.
	 *
	 * @param  Admin_Settings $admin_settings Admin settings instance.
	 * @return void
	 */
	public function admin_settings_fields( $admin_settings ) {
		$conflicts = [ 'currency', 'currency_position', 'price_thousand_separator', 'price_decimal_separator', 'price_number_decimals' ];

		foreach ( $conflicts as $key ) {
			$admin_settings->get_field( $key )
				->set_attribute( 'disabled' )
				->set_prop( 'save_field', false );
		}

		// TODO: Display message about that.
	}

	/**
	 * Requires WooCommerce to works.
	 *
	 * @return bool
	 */
	protected function requires_woocommerce_active() {
		global $woocommerce;

		if ( ! class_exists( 'WooCommerce' ) || ( $woocommerce && version_compare( $woocommerce->version, '3.0', '<' ) ) ) {
			$this->log_error( esc_html__( 'Add-on required WooCommerce v3.x installed and active in your site, please double-check.', 'awebooking-woocommerce' ) );
			return false;
		}

		return true;
	}
}
