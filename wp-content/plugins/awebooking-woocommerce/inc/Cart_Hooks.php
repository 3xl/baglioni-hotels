<?php
namespace AweBooking\WooCommerce;

use AweBooking\Factory;
use AweBooking\Concierge;
use AweBooking\AweBooking;
use AweBooking\Booking\Request;
use AweBooking\Booking\Availability;
use AweBooking\Support\Period;
use AweBooking\Support\Collection;
use AweBooking\Support\Service_Hooks;
use AweBooking\Support\Mailer;
use AweBooking\Notification\Booking_Created;
use AweBooking\Notification\Admin_Booking_Created;

class Cart_Hooks extends Service_Hooks {
	/**
	 * Init service provider.
	 *
	 * @param AweBooking $awebooking AweBooking instance.
	 */
	public function init( $awebooking ) {
		add_filter( 'woocommerce_product_class', [ $this, 'register_product_class' ], 10, 4 );

		add_action( 'awebooking/add_booking', [ $this, 'add_product_booking_to_cart' ], 10, 2 );
		add_filter( 'woocommerce_add_cart_item_data', [ $this, 'split_product_individual_cart_items' ], 10, 2 );
		add_filter( 'woocommerce_add_cart_item', [ $this, 'woocommerce_add_cart_item' ], 10, 1 );
		add_filter( 'woocommerce_get_cart_item_from_session', [ $this, 'woocommerce_get_cart_item_from_session' ], 10, 2 );

		add_action( 'awebooking/cart/update_item', [ $this, 'awebooking_update_cart_item' ], 10, 1 );

		add_action( 'woocommerce_remove_cart_item', [ $this, 'woocommerce_remove_cart_item' ], 10, 2 );
		add_action( 'awebooking/cart/remove_item', [ $this, 'awebooking_remove_cart_item' ], 10, 2 );

		add_action( 'woocommerce_checkout_order_processed', array( $this, 'order_processed' ), 20, 3 );
	}

	/**
	 * Register product booking class.
	 *
	 * @param  string $classname    Class name.
	 * @param  string $product_type The product type.
	 * @param  string $var          The variation product type product variation product.
	 * @param  int    $product_id   The product id.
	 * @return string
	 */
	public function register_product_class( $classname, $product_type, $var, $product_id ) {
		global $woocommerce;

		if ( AweBooking::ROOM_TYPE === get_post_type( $product_id ) ) {
			$classname = Product_Booking_Room::class;
		}

		return $classname;
	}

	/**
	 * When a room-type is add to booking, we add a "product booking" in to wc-cart.
	 */
	public function add_product_booking_to_cart( $cart_item ) {
		global $woocommerce;

		// Add this booking room-type in to WooCommerce cart.
		$cart_item_key = $woocommerce->cart->add_to_cart(
			$cart_item->get_id(), 1, null, [], [ 'awebooking' => $cart_item->options->all() ]
		);

		if ( $cart_item_key ) {
			$cart_item->options['woocommerce_cart_item_key'] = $cart_item_key;
			awebooking( 'cart' )->store_cart_contents();
		}
	}

	/**
	 * Split products individual cart item if item is room type.
	 *
	 * @param  array $cart_item_data cart item data.
	 * @param  int   $product_id     product id.
	 * @return array
	 */
	public function split_product_individual_cart_items( $cart_item_data, $product_id ) {
		if ( AweBooking::ROOM_TYPE === get_post_type( $product_id ) ) {
			$cart_item_data['unique_key'] = uniqid();
		}

		return $cart_item_data;
	}

	/**
	 * Adjust the price of the booking product based on booking properties.
	 *
	 * @param  array $cart_item An array WooCommerce cart item.
	 * @return array
	 */
	public function woocommerce_add_cart_item( $cart_item ) {
		if ( isset( $cart_item['awebooking'] ) ) {
			$options = Collection::make( $cart_item['awebooking'] );

			$room_type = $cart_item['data']->get_room_type();

			try {
				if ( $room_type->is_purchasable( $options ) ) {
					$cart_item['data']->set_price( $room_type->get_buyable_price( $options )->get_amount() );
				} else {
					$cart_item = []; // Remove this cart item.
				}
			} catch ( \Exception $e ) {
				$cart_item = []; // Remove this cart item.
			}
		}

		return $cart_item;
	}

	/**
	 * Before a `cart_item` loaded from session, we will re-add booking in to it.
	 *
	 * @param  array $cart_item Cart item.
	 * @param  array $values    Cart item values.
	 * @return array
	 */
	public function woocommerce_get_cart_item_from_session( $cart_item, $values ) {
		if ( ! empty( $values['awebooking'] ) ) {
			$cart_item['awebooking'] = $values['awebooking'];

			// Adjust the price of the booking item.
			$cart_item = $this->woocommerce_add_cart_item( $cart_item );
		}

		return $cart_item;
	}

	/**
	 * Do remove cart item in hotel booking cart.
	 *
	 * @param  string  $cart_item_key Cart item key.
	 * @param  WC_Cart $cart          WC cart object.
	 * @return void
	 */
	public function woocommerce_remove_cart_item( $cart_item_key, $cart ) {
		$cart_item = $cart->get_cart_item( $cart_item_key );
		if ( empty( $cart_item['awebooking'] ) ) {
			return;
		}

		$awebooking_cart = awebooking( 'cart' );

		$awebooking_cart_item = $awebooking_cart->search(function ( $cart_item ) use ( $cart_item_key ) {
			return $cart_item->options['woocommerce_cart_item_key'] === $cart_item_key;
		})->first();

		if ( $awebooking_cart_item ) {
			$awebooking_cart->remove( $awebooking_cart_item->row_id );
		}

		global $woocommerce;
		unset( $woocommerce->cart->removed_cart_contents[ $cart_item_key ] );
	}

	public function awebooking_remove_cart_item( $cart_item, $cart ) {
		if ( ! $cart_item->options['woocommerce_cart_item_key'] ) {
			return;
		}

		global $woocommerce;
		$woocommerce->cart->remove_cart_item( $cart_item->options['woocommerce_cart_item_key'] );
	}

	public function awebooking_update_cart_item( $cart_item ) {
		global $woocommerce;

		if ( ! $cart_item->options['woocommerce_cart_item_key'] ) {
			return;
		}

		$woocommerce_cart_item =& $woocommerce->cart->cart_contents[ $cart_item->options['woocommerce_cart_item_key'] ];
		if ( ! isset( $woocommerce_cart_item['awebooking'] ) ) {
			return;
		}

		$woocommerce_cart_item['awebooking'] = $cart_item->options->all();
		if ( isset( $woocommerce_cart_item['data'] ) ) {
			$woocommerce_cart_item['data']->set_price( $cart_item->get_price()->get_amount() );
		}

		$woocommerce->cart->calculate_totals();
	}

	public function order_processed( $order_id, $posted_data, $order ) {
		$order_booking_id = (int) $order->get_meta( '_awebooking_booking_id' );

		$booking = Factory::get_booking( $order_booking_id );
		if ( ! $booking->exists() ) {
			return;
		}

		awebooking( 'cart' )->destroy();

		try {
			Mailer::to( $booking->get_customer_email() )->send( new Booking_Created( $booking ) );
			Mailer::to( awebooking( 'config' )->get_admin_notify_emails() )->send( new Admin_Booking_Created( $booking ) );
		} catch ( \Exception $e ) {}
	}
}
