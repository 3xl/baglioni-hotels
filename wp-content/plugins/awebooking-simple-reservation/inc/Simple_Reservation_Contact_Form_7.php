<?php
namespace AweBooking\Simple_Reservation;

if ( ! class_exists( 'WPCF7' ) ) {
	return;
}

class Simple_Reservation_Contact_Form_7 {

	/**
	 * Constructor.
	 */
	public function __construct() {
		// Register shortcodes.
		add_action( 'wpcf7_init', [ $this, 'add_shortcodes' ] );

		// Tag generator.
		add_action( 'wpcf7_admin_init', [ $this, 'tag_generator' ], 70 );
	}

	/**
	 * Handler shortcode.
	 *
	 * @param  obj $tag tag.
	 */
	public function shortcode_handler( $tag ) {
		$tag = new \WPCF7_FormTag( $tag );

		if ( empty( $tag->name ) ) {
			return '';
		}

		return sprintf( '<input type="hidden" name="%1$s" value="%2$s"/>', esc_attr( $tag->name ), apply_filters( 'awebooking/simple_reservation/contact_form_7', get_the_title() ) );
	}

	/**
	 * Generator tag.
	 */
	public function tag_generator() {
		if ( ! class_exists( '\WPCF7_TagGenerator' ) ) {
			return;
		}

		$tag_generator = \WPCF7_TagGenerator::get_instance();
		$tag_generator->add( 'room_type', __( 'Room Type', 'contact-form-7' ), [ $this, 'tag_panel' ] );
	}

	/**
	 * Template tag panel.
	 *
	 * @param  array $contact_form contact_form.
	 * @param  array $args         args.
	 */
	public function tag_panel( $contact_form, $args = '' ) {
		$args = wp_parse_args( $args, array() );
		$type = 'room_type'; ?>

		<div class="control-box">
			<fieldset>
				<table class="form-table">
					<tbody>
						<tr>
							<td><label><code><?php esc_html_e( 'name', 'contact-form-7' ); ?></code><br />
							<input type="text" name="name" class="tg-name oneline" /></td>
						</tr>
					</tbody>
				</table>
			</fieldset>
		</div>

		<div class="insert-box">
			<input type="text" name="<?php echo esc_attr( $type ); ?>" class="tag code" readonly="readonly" onfocus="this.select()" />

			<div class="submitbox">
			<input type="button" class="button button-primary insert-tag" value="<?php echo esc_attr( __( 'Insert Tag', 'contact-form-7' ) ); ?>" />
			</div>

			<br class="clear" />

			<p class="description mail-tag">
				<label for="<?php echo esc_attr( $args['content'] . '-mailtag' ); ?>"><?php echo sprintf( esc_html( esc_html__( 'To use the value input through this field in a mail field, you need to insert the corresponding mail-tag (%s) into the field on the Mail tab.', 'contact-form-7' ) ), '<strong><span class="mail-tag"></span></strong>' ); ?>
					<input type="text" class="mail-tag code hidden" readonly="readonly" id="<?php echo esc_attr( $args['content'] . '-mailtag' ); ?>" />
				</label>
			</p>
		</div>

		<script type="text/javascript">
		jQuery(function($){
			$(document).on('change', 'select', function() {
				var $this = $(this),
					value = $this.val();

				if ( ! value )
					return;

				$('input[name="'+$this.attr('id')+'"]').val(value).trigger('change');
			});
		});
		</script>
		<?php
	}

	/**
	 * Add shortcode.
	 */
	public function add_shortcodes() {
		if ( function_exists( 'wpcf7_add_form_tag' ) ) {
			wpcf7_add_form_tag( array( 'room_type', 'room_type*' ), array( $this, 'shortcode_handler' ), true );
		}
	}
}
