<?php
namespace AweBooking\Simple_Reservation;

use AweBooking\Support\Addon;
use AweBooking\Admin\Admin_Settings;

class Simple_Reservation_Addon extends Addon {
	const VERSION = '0.2.1';

	/**
	 * Requires minimum AweBooking version.
	 *
	 * @return string
	 */
	public function requires() {
		return '3.0.0-beta8';
	}

	/**
	 * Registers services on the awebooking.
	 *
	 * @return void
	 */
	public function register() {
		new Simple_Reservation_Contact_Form_7;

		load_plugin_textdomain( 'awebooking-simple-reservation', false, dirname( $this->get_basename() ) . '/languages' );
	}

	/**
	 * Init the addon.
	 *
	 * @return void
	 */
	public function init() {
		add_action( 'awebooking/register_admin_settings', [ $this, '_admin_settings' ] );

		if ( awebooking( 'setting' )->get( 'enable_simple_reservation', true ) ) {
			remove_action( 'awebooking/single_room_type_summary', 'awebooking_template_single_form', 15 );
			add_action( 'awebooking/single_room_type_summary', [ $this, '_add_template_shortcode' ], 15 );
		}
	}

	/**
	 * Add template shortcode.
	 */
	public function _add_template_shortcode() {
		echo do_shortcode( awebooking( 'config' )->get( 'simple_reservation_shortcode', '' ) );
	}

	/**
	 * Add admin settings.
	 *
	 * @param  Admin_Settings $admin_settings Admin_Settings instance.
	 */
	public function _admin_settings( Admin_Settings $admin_settings ) {
		$admin_settings->add_field( [
			'id'       => '__simple_reservation__',
			'section'  => 'display',
			'type' => 'title',
			'name' => esc_html__( 'Simple Reservation', 'awebooking-simple-reservation' ),
			'priority' => 65,
		] );

		$admin_settings->add_field( [
			'id'       => 'enable_simple_reservation',
			'section'  => 'display',
			'name' => esc_html__( 'Enable Simple Reservation', 'awebooking-simple-reservation' ),
			'type'     => 'toggle',
			'default'  => true,
			'priority' => 66,
		] );

		$admin_settings->add_field( [
			'id'       => 'simple_reservation_shortcode',
			'section'  => 'display',
			'name' => esc_html__( 'Shortcode', 'awebooking-simple-reservation' ),
			'type'     => 'text',
			'priority' => 67,
			'deps'     => [ 'enable_simple_reservation', '==', 'true' ],
		] );
	}
}
