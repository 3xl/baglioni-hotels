<?php
/**
 * Plugin Name:     AweBooking Simple Reservation
 * Plugin URI:      http://awethemes.com/plugins/awebooking
 * Description:     Customers just simply send contact and booking request email to admin to get support without any hassle of multiple booking steps.
 * Author:          awethemes
 * Author URI:      http://awethemes.com
 * Text Domain:     awebooking-simple-reservation
 * Domain Path:     /languages
 * Version:         0.2.1
 *
 * @package         AweBooking/Simple_Reservation
 */

use AweBooking\AweBooking;
use AweBooking\Simple_Reservation\Simple_Reservation_Addon;

/**
 * Register the simple reservation addon into AweBooking.
 */
add_action( 'awebooking/init', function ( AweBooking $awebooking ) {
	require_once trailingslashit( __DIR__ ) . 'vendor/autoload.php';

	$awebooking->register_addon( new Simple_Reservation_Addon( 'awebooking-simple-reservation', __FILE__ ) );
});
